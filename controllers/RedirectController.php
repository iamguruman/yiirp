<?php

namespace app\controllers;

use yii\web\Controller;

class RedirectController extends Controller
{

    public function actionIndex($url){

        if($url[0] != 'h' && $url[1] != 't' && $url[2] != 't' && $url[3] != 'p'){

            return $this->redirect("http://".$url);

        } else {

            return $this->redirect($url);

        }

    }

}