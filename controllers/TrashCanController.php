<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class TrashCanController extends Controller
{

    public function actionIndex(){

        if(Yii::$app->user->isGuest){
            return $this->redirect("/");
        }

        $model = Yii::$app->user->identity;

        if($model->hide_markdel == 0){

            $model->hide_markdel = 1;
            $model->save();

        } else {

            $model->hide_markdel = 0;
            $model->save();

        }

    }
}