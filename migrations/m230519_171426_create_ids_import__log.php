<?php

use yii\db\Migration;

/**
 * Class m230519_171426_create_ids_import__log
 */
class m230519_171426_create_ids_import__log extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('ids_import__log', [
            'id' => 'pk',
            'import_id' => 'integer',
            'file_id' => 'integer',
            'entry_json' => 'text',
            'created_at' => 'datetime'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m230519_171426_create_ids_import__log cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230519_171426_create_ids_import__log cannot be reverted.\n";

        return false;
    }
    */
}
