<?php

use PHPHtmlParser\Options;
use yii\web\View;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$array_of_modules=array();
$modules_dir=str_replace('/config*','/modules',__DIR__.'*');

$opendir=opendir($modules_dir);
//var_dump($modules_dir);
$array_of_modules2=[];
while($file = readdir($opendir)) {
    if ( is_dir($modules_dir.'/'.$file)
        && $file != '.' && $file != '..'
        && file_exists($modules_dir.'/'.$file.'/Module.php')
        && substr($file,0,1) !== '_'
    ) {
        $array_of_modules[$file]['class'] = "app\modules\\".$file."\Module";
        $array_of_modules2[]=$file;
    }
}

$config = [
    'id' => 'basic',
    'name' => 'yiirp',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@modules'   => '@app/modules',
        '@web'   => '@app/web',
    ],

    'modules' => $array_of_modules,

    'container' => [
        'definitions' => [
          \yii\web\View::class => [
              'on beforeRender' => function(\yii\base\ViewEvent $event) {
                    if(($_GET['view'] ?? null) === 'ajax') {
                        \Yii::$app->controller->view->off(View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
                    }
              },
              'on afterRender' => function(\yii\base\ViewEvent $event) {

                  if((strpos($event->viewFile, 'layouts/main')!==false) && ($_GET['view'] ?? null) === 'ajax') {
                      $dom = new PHPHtmlParser\Dom;

                      $dom->setOptions(
                          (new Options())
                              ->setCleanupInput(false)
//                                ->setPreserveLineBreaks(true)
                      );
                      $dom->loadStr($event->output);
                      
                      $dom->find('header')[0]->delete();
                      $dom->find('footer')[0]->delete();
                      $dom->find('main > .container')[0]->setAttribute('style', 'padding: 0 0 0 0 !important');

                      /** @var PHPHtmlParser\Dom\Node\HtmlNode $a */
                      foreach($dom->find('a') as $a) {
                          $href = $a->getAttribute('href');
                          if(strpos($href, '#w')!==false) {
                              continue;
                          }
                          if(strpos($href, 'tel:')===0) {
                              continue;
                          }
                          if(strpos($href, 'mailto:')===0) {
                              continue;
                          }
                          $a->setAttribute('href', $href .'&view=ajax');
                      }

                      $event->output = (string)$dom;

                  }

              }
          ]
        ],
    ],

    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'SPjnzVYnE0oxD3amM5hKzEcaBP1tUxET',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'user' => [
            'identityClass' => 'app\modules\user\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => '/user/login/',
        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => \yii\symfonymailer\Mailer::class,
            'viewPath' => '@app/mail',
            // send all mails to a file by default.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],

    ],
    'params' => $params,
];



//if (YII_ENV_DEV) {
//$gii = true;
//if ($gii) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];
//}

return $config;
