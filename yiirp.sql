-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Июл 10 2023 г., 12:33
-- Версия сервера: 8.0.31-0ubuntu0.20.04.2
-- Версия PHP: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `yiirp`
--

-- --------------------------------------------------------

--
-- Структура таблицы `dev_messages`
--

CREATE TABLE `dev_messages` (
  `id` int NOT NULL,
  `created_at` text COLLATE utf8mb4_general_ci,
  `created_by` int DEFAULT NULL,
  `message` text COLLATE utf8mb4_general_ci,
  `type` varchar(400) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `module_id` varchar(400) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `controller_id` varchar(400) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `action_id` varchar(400) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `value` longblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids`
--

CREATE TABLE `ids` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `owner_sys_user_id` int DEFAULT NULL,
  `owner_sys_group_id` int DEFAULT NULL,
  `template_name_id` int DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ids_template_status_name_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_action_name`
--

CREATE TABLE `ids_action_name` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_flow`
--

CREATE TABLE `ids_flow` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `parent_ids_template_name_id` int NOT NULL,
  `child_ids_template_name_id` int NOT NULL,
  `child_ids_template_status_name_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_flow_field_connection`
--

CREATE TABLE `ids_flow_field_connection` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `ids_flow_id` int NOT NULL,
  `parent_ids_template_field_id` int DEFAULT NULL,
  `child_ids_template_field_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_import__field`
--

CREATE TABLE `ids_import__field` (
  `id` int NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `comment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `ids_import__name_id` int NOT NULL,
  `ids_template_field_id` int NOT NULL,
  `field_name_from_file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Field name from file',
  `is_equal` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_import__file`
--

CREATE TABLE `ids_import__file` (
  `id` int NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `comment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `ids_import__name_id` int NOT NULL,
  `md5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ext` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_import__name`
--

CREATE TABLE `ids_import__name` (
  `id` int NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_general_ci,
  `ids_template_name_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_print_template`
--

CREATE TABLE `ids_print_template` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `ids_template_name_id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `html_content` longblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_template_field`
--

CREATE TABLE `ids_template_field` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `template_name_id` int NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `_del_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_general_ci,
  `sort` int DEFAULT NULL,
  `value_field` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `show_as_column_at_template_list` tinyint(1) DEFAULT NULL,
  `source_template_name_id` int DEFAULT NULL,
  `show_in_tree` int DEFAULT NULL,
  `html_element` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `default_value` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `saving_changes` int DEFAULT NULL,
  `auto_increment` int DEFAULT NULL,
  `auto_increment_template` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `onchange` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `crudview_view` text COLLATE utf8mb4_general_ci,
  `crudview_index` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `hint` text COLLATE utf8mb4_general_ci,
  `hint_insertion_set` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `hint_insertion_insert` text COLLATE utf8mb4_general_ci,
  `this_is_telnumber` int DEFAULT NULL,
  `this_is_url` int DEFAULT NULL,
  `this_is_email` int DEFAULT NULL,
  `is_searchable` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_template_field_replace`
--

CREATE TABLE `ids_template_field_replace` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `ids_template_field_id` int NOT NULL,
  `text_search` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `text_replace` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_template_help`
--

CREATE TABLE `ids_template_help` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `ids_template_name_id` int DEFAULT NULL,
  `ids_template_status_name_id` int DEFAULT NULL,
  `subject` varchar(400) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `description` longblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_template_links`
--

CREATE TABLE `ids_template_links` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `ids_template_name_id` int DEFAULT NULL,
  `ids_template_field_id` int DEFAULT NULL,
  `text` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `link` varchar(1000) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `target` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `redirect` int NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_template_name`
--

CREATE TABLE `ids_template_name` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Template Name',
  `name_template` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `owner_user_id` int NOT NULL,
  `extra_module_action_view` varchar(1000) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `hint` text COLLATE utf8mb4_general_ci,
  `show_field_name_on_crud_index` int DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_template_status_name`
--

CREATE TABLE `ids_template_status_name` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Template Name',
  `ids_template_name_id` int NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `style` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sort` int DEFAULT NULL,
  `show_counter` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_template__user_access`
--

CREATE TABLE `ids_template__user_access` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `ids_template_name_id` int NOT NULL,
  `sys_user_id` int NOT NULL,
  `can_list` int DEFAULT NULL,
  `can_create` int DEFAULT NULL,
  `can_read_only_self_created` int DEFAULT NULL,
  `can_update_self_created` int DEFAULT NULL,
  `can_update_other_created` int DEFAULT NULL,
  `can_edit_template` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_tree`
--

CREATE TABLE `ids_tree` (
  `id` int NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `update_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `parent_ids_id` int NOT NULL,
  `child_ids_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_values_history`
--

CREATE TABLE `ids_values_history` (
  `id` int NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `ids_id` int NOT NULL,
  `ids_template_name_id` int NOT NULL,
  `ids_template_field_id` int NOT NULL,
  `value` longblob NOT NULL,
  `action_type` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `restored_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_value_date`
--

CREATE TABLE `ids_value_date` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `ids_id` int NOT NULL,
  `template_field_id` int NOT NULL,
  `value` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_value_datetime`
--

CREATE TABLE `ids_value_datetime` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `ids_id` int NOT NULL,
  `template_field_id` int NOT NULL,
  `value` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_value_decimal102`
--

CREATE TABLE `ids_value_decimal102` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `ids_id` int NOT NULL,
  `template_field_id` int NOT NULL,
  `value` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_value_int11`
--

CREATE TABLE `ids_value_int11` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `ids_id` int NOT NULL,
  `template_field_id` int NOT NULL,
  `value` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_value_longblob`
--

CREATE TABLE `ids_value_longblob` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `ids_id` int NOT NULL,
  `template_field_id` int NOT NULL,
  `value` longblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_value_text`
--

CREATE TABLE `ids_value_text` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `ids_id` int NOT NULL,
  `template_field_id` int NOT NULL,
  `value` text COLLATE utf8mb4_general_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_value_time`
--

CREATE TABLE `ids_value_time` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `ids_id` int NOT NULL,
  `template_field_id` int NOT NULL,
  `value` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_value_varchar255`
--

CREATE TABLE `ids_value_varchar255` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `ids_id` int NOT NULL,
  `template_field_id` int NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_value_varchar500`
--

CREATE TABLE `ids_value_varchar500` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `ids_id` int NOT NULL,
  `template_field_id` int NOT NULL,
  `value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ids_value_varchar1000`
--

CREATE TABLE `ids_value_varchar1000` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `ids_id` int NOT NULL,
  `template_field_id` int NOT NULL,
  `value` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `pbx_calls`
--

CREATE TABLE `pbx_calls` (
  `id` int NOT NULL,
  `pbx_list_id` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL COMMENT 'Добавлено когда',
  `created_by` int DEFAULT NULL COMMENT 'Добавлено кем',
  `updated_at` datetime DEFAULT NULL COMMENT 'Изменено когда',
  `updated_by` int DEFAULT NULL COMMENT 'Изменено кем',
  `markdel_at` datetime DEFAULT NULL COMMENT 'Удалено когда',
  `markdel_by` int DEFAULT NULL COMMENT 'Удалено кем',
  `datetime_start` datetime DEFAULT NULL,
  `datetime_end` datetime DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Наименование',
  `call_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direction` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `office_tel` bigint DEFAULT NULL,
  `external_number` bigint DEFAULT NULL,
  `internal_number` bigint DEFAULT NULL,
  `billsec` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answered` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration_waiting` int DEFAULT NULL,
  `internal_numbers` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `pbx_list`
--

CREATE TABLE `pbx_list` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `owner_sys_user_id` int DEFAULT NULL,
  `owner_sys_group_id` int DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `operator_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `public_key` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `pbx_sms`
--

CREATE TABLE `pbx_sms` (
  `id` int NOT NULL,
  `pbx_list_id` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL COMMENT 'Добавлено когда',
  `created_by` int DEFAULT NULL COMMENT 'Добавлено кем',
  `updated_at` datetime DEFAULT NULL COMMENT 'Изменено когда',
  `updated_by` int DEFAULT NULL COMMENT 'Изменено кем',
  `markdel_at` datetime DEFAULT NULL COMMENT 'Удалено когда',
  `markdel_by` int DEFAULT NULL COMMENT 'Удалено кем',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Наименование',
  `from` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `pbx_webhook`
--

CREATE TABLE `pbx_webhook` (
  `id` int NOT NULL,
  `pbx_list_id` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL COMMENT 'Добавлено когда',
  `created_by` int DEFAULT NULL COMMENT 'Добавлено кем',
  `updated_at` datetime DEFAULT NULL COMMENT 'Изменено когда',
  `updated_by` int DEFAULT NULL COMMENT 'Изменено кем',
  `markdel_at` datetime DEFAULT NULL COMMENT 'Удалено когда',
  `markdel_by` int DEFAULT NULL COMMENT 'Удалено кем',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Наименование',
  `webhook_type_id` int DEFAULT NULL,
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `call_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `internalNumber` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `pbx_webhook_type`
--

CREATE TABLE `pbx_webhook_type` (
  `id` int NOT NULL,
  `pbx_list_id` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL COMMENT 'Добавлено когда',
  `created_by` int DEFAULT NULL COMMENT 'Добавлено кем',
  `updated_at` datetime DEFAULT NULL COMMENT 'Изменено когда',
  `updated_by` int DEFAULT NULL COMMENT 'Изменено кем',
  `markdel_at` datetime DEFAULT NULL COMMENT 'Удалено когда',
  `markdel_by` int DEFAULT NULL COMMENT 'Удалено кем',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Наименование',
  `shortname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sys_group`
--

CREATE TABLE `sys_group` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sys_user`
--

CREATE TABLE `sys_user` (
  `id` int NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `markdel_by` int DEFAULT NULL,
  `markdel_at` datetime DEFAULT NULL,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password_hash` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cell_phone` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `middlename` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `blocked_by` int DEFAULT NULL,
  `blocked_at` datetime DEFAULT NULL,
  `blocked_reason` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `key` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `authKey` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `tel_internal_number` int DEFAULT NULL,
  `default_sort` varchar(10) COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'asc',
  `dialog_width_px` int DEFAULT NULL,
  `dialog_height_px` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `dev_messages`
--
ALTER TABLE `dev_messages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ids`
--
ALTER TABLE `ids`
  ADD PRIMARY KEY (`id`),
  ADD KEY `template_id` (`template_name_id`),
  ADD KEY `owner_sys_user_id` (`owner_sys_user_id`),
  ADD KEY `owner_sys_group_id` (`owner_sys_group_id`),
  ADD KEY `markdel_by` (`markdel_by`),
  ADD KEY `ids_template_status_name` (`ids_template_status_name_id`);

--
-- Индексы таблицы `ids_action_name`
--
ALTER TABLE `ids_action_name`
  ADD PRIMARY KEY (`id`),
  ADD KEY `markdel_by` (`markdel_by`);

--
-- Индексы таблицы `ids_flow`
--
ALTER TABLE `ids_flow`
  ADD PRIMARY KEY (`id`),
  ADD KEY `markdel_by` (`markdel_by`),
  ADD KEY `parent_id` (`parent_ids_template_name_id`),
  ADD KEY `child_id` (`child_ids_template_name_id`),
  ADD KEY `child_ids_template_status_name_id` (`child_ids_template_status_name_id`);

--
-- Индексы таблицы `ids_flow_field_connection`
--
ALTER TABLE `ids_flow_field_connection`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_ids_template_field_id` (`parent_ids_template_field_id`),
  ADD KEY `child_ids_template_field_id` (`child_ids_template_field_id`);

--
-- Индексы таблицы `ids_import__field`
--
ALTER TABLE `ids_import__field`
  ADD PRIMARY KEY (`id`),
  ADD KEY `xml_field` (`field_name_from_file`),
  ADD KEY `import_xml_to_ids__name_id` (`ids_import__name_id`),
  ADD KEY `is_equal` (`is_equal`);

--
-- Индексы таблицы `ids_import__file`
--
ALTER TABLE `ids_import__file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `md5` (`md5`),
  ADD KEY `ext` (`ext`);

--
-- Индексы таблицы `ids_import__name`
--
ALTER TABLE `ids_import__name`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ids_template_name_id` (`ids_template_name_id`);

--
-- Индексы таблицы `ids_print_template`
--
ALTER TABLE `ids_print_template`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ids_template_name_id` (`ids_template_name_id`),
  ADD KEY `name` (`name`),
  ADD KEY `markdel_by` (`markdel_by`);

--
-- Индексы таблицы `ids_template_field`
--
ALTER TABLE `ids_template_field`
  ADD PRIMARY KEY (`id`),
  ADD KEY `template_name_id` (`template_name_id`),
  ADD KEY `name` (`_del_name`),
  ADD KEY `sort` (`sort`),
  ADD KEY `title` (`title`),
  ADD KEY `value_field` (`value_field`),
  ADD KEY `markdel_by` (`markdel_by`),
  ADD KEY `show_table_column` (`show_as_column_at_template_list`),
  ADD KEY `source_template_id` (`source_template_name_id`),
  ADD KEY `show_in_tree` (`show_in_tree`),
  ADD KEY `html_element` (`html_element`),
  ADD KEY `value_history` (`saving_changes`),
  ADD KEY `auto_increment` (`auto_increment`),
  ADD KEY `this_is_telnumber` (`this_is_telnumber`),
  ADD KEY `this_is_url` (`this_is_url`),
  ADD KEY `this_is_email` (`this_is_email`);

--
-- Индексы таблицы `ids_template_field_replace`
--
ALTER TABLE `ids_template_field_replace`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ids_template_field_id` (`ids_template_field_id`);

--
-- Индексы таблицы `ids_template_help`
--
ALTER TABLE `ids_template_help`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ids_template_name_id` (`ids_template_name_id`),
  ADD KEY `ids_template_status_name_id` (`ids_template_status_name_id`),
  ADD KEY `subject` (`subject`);

--
-- Индексы таблицы `ids_template_links`
--
ALTER TABLE `ids_template_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `markdel_by` (`markdel_by`),
  ADD KEY `ids_template_name_id` (`ids_template_name_id`),
  ADD KEY `ids_template_field_id` (`ids_template_field_id`),
  ADD KEY `text` (`text`);

--
-- Индексы таблицы `ids_template_name`
--
ALTER TABLE `ids_template_name`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `markdel_by` (`markdel_by`),
  ADD KEY `owner_user_id` (`owner_user_id`);

--
-- Индексы таблицы `ids_template_status_name`
--
ALTER TABLE `ids_template_status_name`
  ADD PRIMARY KEY (`id`),
  ADD KEY `color` (`style`),
  ADD KEY `sort` (`sort`);

--
-- Индексы таблицы `ids_template__user_access`
--
ALTER TABLE `ids_template__user_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ids_template_name_id` (`ids_template_name_id`),
  ADD KEY `sys_user_id` (`sys_user_id`),
  ADD KEY `read_only_self_created` (`can_read_only_self_created`),
  ADD KEY `update_only_self_created` (`can_update_self_created`),
  ADD KEY `update_all` (`can_update_other_created`),
  ADD KEY `can_create` (`can_create`),
  ADD KEY `can_list` (`can_list`),
  ADD KEY `user_can_edit_template` (`can_edit_template`);

--
-- Индексы таблицы `ids_tree`
--
ALTER TABLE `ids_tree`
  ADD PRIMARY KEY (`id`),
  ADD KEY `markdel_by` (`markdel_by`),
  ADD KEY `parent_ids_id` (`parent_ids_id`),
  ADD KEY `child_ids_id` (`child_ids_id`);

--
-- Индексы таблицы `ids_values_history`
--
ALTER TABLE `ids_values_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `markdel_by` (`markdel_by`),
  ADD KEY `ids_template_name_id` (`ids_template_name_id`),
  ADD KEY `action_type` (`action_type`),
  ADD KEY `restored_id` (`restored_id`);

--
-- Индексы таблицы `ids_value_date`
--
ALTER TABLE `ids_value_date`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ids_value_datetime`
--
ALTER TABLE `ids_value_datetime`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ids_value_decimal102`
--
ALTER TABLE `ids_value_decimal102`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ids_value_int11`
--
ALTER TABLE `ids_value_int11`
  ADD PRIMARY KEY (`id`),
  ADD KEY `value` (`value`),
  ADD KEY `markdel_by` (`markdel_by`);

--
-- Индексы таблицы `ids_value_longblob`
--
ALTER TABLE `ids_value_longblob`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ids_value_text`
--
ALTER TABLE `ids_value_text`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ids_value_time`
--
ALTER TABLE `ids_value_time`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ids_value_varchar255`
--
ALTER TABLE `ids_value_varchar255`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ids_id` (`ids_id`),
  ADD KEY `tempalate_field_id` (`template_field_id`),
  ADD KEY `markdel_by` (`markdel_by`),
  ADD KEY `value` (`value`);

--
-- Индексы таблицы `ids_value_varchar500`
--
ALTER TABLE `ids_value_varchar500`
  ADD PRIMARY KEY (`id`),
  ADD KEY `value` (`value`),
  ADD KEY `markdel_by` (`markdel_by`),
  ADD KEY `ids_id` (`ids_id`),
  ADD KEY `tempalate_field_id` (`template_field_id`);

--
-- Индексы таблицы `ids_value_varchar1000`
--
ALTER TABLE `ids_value_varchar1000`
  ADD PRIMARY KEY (`id`),
  ADD KEY `markdel_by` (`markdel_by`),
  ADD KEY `ids_id` (`ids_id`),
  ADD KEY `tempalate_field_id` (`template_field_id`);

--
-- Индексы таблицы `pbx_calls`
--
ALTER TABLE `pbx_calls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pbx_list_id` (`pbx_list_id`);

--
-- Индексы таблицы `pbx_list`
--
ALTER TABLE `pbx_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `public_key` (`public_key`);

--
-- Индексы таблицы `pbx_sms`
--
ALTER TABLE `pbx_sms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pbx_list_id` (`pbx_list_id`);

--
-- Индексы таблицы `pbx_webhook`
--
ALTER TABLE `pbx_webhook`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pbx_list_id` (`pbx_list_id`);

--
-- Индексы таблицы `pbx_webhook_type`
--
ALTER TABLE `pbx_webhook_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `label` (`shortname`),
  ADD KEY `pbx_list_id` (`pbx_list_id`);

--
-- Индексы таблицы `sys_group`
--
ALTER TABLE `sys_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `markdel_by` (`markdel_by`);

--
-- Индексы таблицы `sys_user`
--
ALTER TABLE `sys_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password` (`password_hash`),
  ADD KEY `login` (`username`),
  ADD KEY `email` (`email`),
  ADD KEY `cell_phone` (`cell_phone`),
  ADD KEY `firstname` (`firstname`),
  ADD KEY `lastname` (`lastname`),
  ADD KEY `middlename` (`middlename`),
  ADD KEY `markdel_by` (`markdel_by`),
  ADD KEY `blocked_by` (`blocked_by`),
  ADD KEY `key` (`key`),
  ADD KEY `authKey` (`authKey`),
  ADD KEY `tel_internal_number` (`tel_internal_number`),
  ADD KEY `default_sort` (`default_sort`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `dev_messages`
--
ALTER TABLE `dev_messages`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids`
--
ALTER TABLE `ids`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_action_name`
--
ALTER TABLE `ids_action_name`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_flow`
--
ALTER TABLE `ids_flow`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_flow_field_connection`
--
ALTER TABLE `ids_flow_field_connection`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_import__field`
--
ALTER TABLE `ids_import__field`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_import__file`
--
ALTER TABLE `ids_import__file`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_import__name`
--
ALTER TABLE `ids_import__name`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_print_template`
--
ALTER TABLE `ids_print_template`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_template_field`
--
ALTER TABLE `ids_template_field`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_template_field_replace`
--
ALTER TABLE `ids_template_field_replace`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_template_help`
--
ALTER TABLE `ids_template_help`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_template_links`
--
ALTER TABLE `ids_template_links`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_template_name`
--
ALTER TABLE `ids_template_name`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_template_status_name`
--
ALTER TABLE `ids_template_status_name`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_template__user_access`
--
ALTER TABLE `ids_template__user_access`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_tree`
--
ALTER TABLE `ids_tree`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_values_history`
--
ALTER TABLE `ids_values_history`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_value_date`
--
ALTER TABLE `ids_value_date`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_value_datetime`
--
ALTER TABLE `ids_value_datetime`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_value_decimal102`
--
ALTER TABLE `ids_value_decimal102`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_value_int11`
--
ALTER TABLE `ids_value_int11`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_value_longblob`
--
ALTER TABLE `ids_value_longblob`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_value_text`
--
ALTER TABLE `ids_value_text`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_value_time`
--
ALTER TABLE `ids_value_time`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_value_varchar255`
--
ALTER TABLE `ids_value_varchar255`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_value_varchar500`
--
ALTER TABLE `ids_value_varchar500`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `ids_value_varchar1000`
--
ALTER TABLE `ids_value_varchar1000`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `pbx_calls`
--
ALTER TABLE `pbx_calls`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `pbx_list`
--
ALTER TABLE `pbx_list`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `pbx_sms`
--
ALTER TABLE `pbx_sms`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `pbx_webhook`
--
ALTER TABLE `pbx_webhook`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `pbx_webhook_type`
--
ALTER TABLE `pbx_webhook_type`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `sys_group`
--
ALTER TABLE `sys_group`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `sys_user`
--
ALTER TABLE `sys_user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
