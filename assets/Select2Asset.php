<?php


namespace app\assets;


use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class Select2Asset extends AssetBundle
{
    public $js = [
        '/vendor/select2/js/select2.js'
    ];
    public $css = [
        '/vendor/select2/css/select2.css'
    ];

    public $depends = [
      JqueryAsset::class
    ];
}