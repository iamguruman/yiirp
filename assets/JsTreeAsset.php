<?php


namespace app\assets;


use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class JsTreeAsset extends AssetBundle
{
    public $js = [
        '/vendor/jstree/jstree.js'
    ];
    public $css = [
        '/vendor/jstree/themes/default/style.css'
    ];

    public $depends = [
      JqueryAsset::class
    ];
}