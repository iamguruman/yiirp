<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap5\Breadcrumbs;
use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;
use yii\bootstrap5\NavBar;

AppAsset::register($this);

$this->registerJs(<<<JS
    function setMyDeletedVisibility() {
      $.post('/trash-can/index',{}, function() {
        document.location.reload();  
      } );
    }
JS
,\yii\web\View::POS_HEAD);

$this->registerCsrfMetaTags();
$this->registerMetaTag(['charset' => Yii::$app->charset], 'charset');
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no']);
$this->registerMetaTag(['name' => 'description', 'content' => $this->params['meta_description'] ?? '']);
$this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['meta_keywords'] ?? '']);
//$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/favicon.ico')]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style type="text/css">
        .btn-success, .btn-primary, .btn-warning, .btn-danger {
            margin-bottom: 5px;
        }
    </style>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header id="header">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => ['class' => 'navbar-expand-md navbar-dark bg-dark fixed-top']
    ]);
    echo Nav::widget([
        'encodeLabels' => false,
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            //['label' => 'Home', 'url' => ['/site/index']],
            //['label' => 'About', 'url' => ['/site/about']],
            //['label' => 'Contact', 'url' => ['/site/contact']],


            Yii::$app->user->isGuest
                ? ['visible' => false]
                : ['label' => 'Ids List', 'url' => ['/ids']],

            Yii::$app->user->isGuest
                ? ['visible' => false]
                : ['label' => 'Create Id', 'url' => ['/ids/create']],

            Yii::$app->user->isGuest
                ? ['visible' => false]
                : [
                    'label' => '<form id="ids_search" method="get" style="margin: 0; padding: 0; display: inline;" action="/ids"><i class="fas fa-search" onclick="document.getElementById(\'ids_search\').submit();"></i>&nbsp;<input name="search" value="'.aGet('search').'" style="width: 100px;"></form>', //search
                    'visible' => true,
                    'format' => 'raw',
            ],

            Yii::$app->user->isGuest
                ? ['visible' => false]
                : ['label' => 'Ids Tree', 'url' => ['/ids_tree']],

            Yii::$app->user->isGuest
                ? ['visible' => false]
                : ['label' => 'Fancy Tree', 'url' => ['/ids_tree_fancy']],

            Yii::$app->user->isGuest
                ? ['visible' => false]
                : ['label' => 'Template', 'url' => ['/ids_template']],

            (aUserMyId() == 4 or aUserMyId() == 8)
                ? ['label' => 'Звонки сегодня',
                    'url' => [
                        '/ids_crud_index',
                            'DynamicModel[Data_sleduyuschego_deystviya]' => date('Y-m-d'),
                            'template_id' => 53
                    ]
                ]
                : ['visible' => false],

            /*Yii::$app->user->isGuest
                ? ['visible' => false]
                : ['label' => 'Ids import', 'url' => ['/ids_import']],*/

            Yii::$app->user->isGuest
                ? ['visible' => false]
                : ['label' => 'User details', 'url' => ['/user/details']],

            !Yii::$app->user->isGuest ? [
                'label' =>
                    Yii::$app->user->identity->hide_markdel ?
                        "<img style='fill:white' src='/media/icon_trash_close.svg' height='20' title='Items marked Del is NOT showing'>"
                        :
                        "<img style='color:white' src='/media/icon_trash_open.svg' height='20' title='Item smarked Del is SHOWING'>"
                        ,
                'url' => '#',
                'linkOptions' => [
                    'onClick' => 'setMyDeletedVisibility();'
                ],
            ] : ['visible' => false],

            Yii::$app->user->isGuest
                ? ['label' => 'Login', 'url' => ['/user/login']]
                : '<li class="nav-item">'
                    . Html::beginForm(['/user/logout'])
                    . Html::submitButton(
                        'Logout (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'nav-link btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>',

            Yii::$app->user->isGuest
                ? ['label' => 'Register', 'url' => ['/user/register']]
                : ['visible' => false]
        ]
    ]);
    NavBar::end();
    ?>
</header>

<main id="main" class="flex-shrink-0" role="main">
    <div class="container">
        <?php if (!empty($this->params['breadcrumbs'])): ?>
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        <?php endif ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer id="footer" class="mt-auto py-3 bg-light">
    <div class="container">
        <div class="row text-muted">
            <div class="col-md-6 text-center text-md-start">&copy; My Company <?= date('Y') ?></div>
            <div class="col-md-6 text-center text-md-end"><?= Yii::powered() ?></div>
        </div>
    </div>
</footer>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
