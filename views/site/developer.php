<?php
/*https://symbl.cc/ru/unicode/blocks/enclosed-alphanumerics/#subblock-2460
⑥⑦⑧⑨⑩*/
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);
?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Предложение для компании застройщика</title>
    <style>
        body { text-align: center; padding: 150px; }
        h1 { font-size: 50px; }
        body { font: 20px Helvetica, sans-serif; color: #333; }
        article { display: block; text-align: left; width: 650px; margin: 0 auto; }
        a { color: #dc8100; text-decoration: none; }
        a:hover { color: #333; text-decoration: none; }
        .small-text { font-size: 18px; }
        li { margin-bottom: 10px; }
    </style>
    <!-- Pixel -->
    <script type="text/javascript">
        (function (d, w) {
            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script");
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://qoopler.ru/index.php?ref="+d.referrer+"&page=" + encodeURIComponent(w.location.href);
            n.parentNode.insertBefore(s, n);
        })(document, window);
    </script>
    <!-- /Pixel -->
</head>
<body>
<article>
    <h1>Предложение для компании-застройщика</h1>

    <p>Здравствуйте! Мы предлагаем Вам начать получать номера телефонов тех кто посетил Ваш сайт.
        Именно тех кто зашел на Ваш сайт, но по какой-то причине не оставил заказ или не
        заполнил форму обратной связи. Вероятнее всего такой человек отвлекся и закрыл вкладку,
        запутался, не понял что-то или ушел на другой сайт без возможности вернуться и получить Вашу консультацию.
        С нашей помощью Вы не упустите такого посетителя!.</p>

    <p>Мы предлагаем далее вам ознакомиться с основными моментами о нашем сервисе.</p>

    <p>Наше предложение построено следующим образом.</p>

    <p>В самом начале мы предлагаем вам ознакомиться со
        <strong>списком сайтов (①)</strong> наших действующих клиентов, которые из сферы недвижимости
        и занимаются строительством и продажей новостроек.</p>

    <p>Далее предлагаем ознакомиться с <strong>видео отзывыми (②)</strong> наших клиентов также из вашей сферы.</p>

    <p>После будут <strong>примеры звонков (③)</strong>, которые осуществляет наш собственный колл-центр на
        номера тех, кто посетил
        сайт и не оставил заявки на сайте нашего клиента. </p>

    <p>Далее в разделе ④ мы записали подробную <strong>видео-презентацию</strong> на 30 минут с
        обзором всех тонкостей и нюансов и сервиса и нашей организации в целом.</p>

    <div>
        <h2>① Список сайтов застройщиков</h2>

        <p>Ниже представлен список сайтов компаний-застройщиков и их проектов, которые применяют наш сервис и получают хорошие результаты.</p>

        <?php

            $www = [];
            $www[0]['name'] = "ГК Единство г.Рязань";
            $www[0]['url'] = "https://edinstvo62.ru/about";
            $www[0]['projects'][0]['name'] = "ЖК Мичурин";
            $www[0]['projects'][0]['url'] = "http://michurin62.ru/";
            $www[0]['projects'][1]['name'] = "ЖК Маргелов";
            $www[0]['projects'][1]['url'] = "https://margelov62.ru/";
            $www[0]['projects'][2]['name'] = "Артал квартал ВЫШЕ!";
            $www[0]['projects'][2]['url'] = "https://domavyshe.ru/";

            $www[1]['name'] = "ГК Жилой Квартал г.Уфа";
            $www[1]['url'] = "https://gkufa.ru/o-kompanii/";
            $www[1]['projects'][0]['name'] = "более 8 проектов";

            $www[2]['name'] = "ГК ССК";
            $www[2]['url'] = "https://xn------7cdavcebg8bfe2bpzefgiga5h.xn--p1ai/#zastroiwik";
            $www[2]['projects'][0]['name'] = "ЖК Сердце Краснодара г.Краснодар";
            $www[2]['projects'][0]['url'] = "https://xn------7cdavcebg8bfe2bpzefgiga5h.xn--p1ai/";

            $www[3]['name'] = "ГК Стрижи";
            $www[3]['url'] = "https://strizhi.ru/about/";
            $www[3]['projects'][0]['name'] = "ГК Стрижи - г.Новосибирск";
            $www[3]['projects'][0]['url'] = "https://dezhnev.gk-strizhi.ru/";
            $www[3]['projects'][1]['name'] = "ЖК Дежнев";
            $www[3]['projects'][1]['url'] = "https://dezhnev.gk-strizhi.ru/";

            $www[4]['name'] = "AFI Development г.Москва";
            $www[4]['url'] = "https://afi-development.com/about";
            $www[4]['projects'][0]['name'] = "ЖК Сиреневый парк";
            $www[4]['projects'][0]['url'] = "https://afi-park.ru/";

            $www[5]['name'] = "ГК Мередиан г.Тюмень";
            $www[5]['url'] = "https://meridian72.ru/company/";
            $www[5]['projects'][0]['name'] = "ЖК Первая Линия";
            $www[5]['projects'][0]['url'] = "https://meridian72.ru/complex/first-line/";

            $www[6]['name'] = "ГК Silver Stream г.Сочи";
            $www[6]['url'] = "https://silver-stream.org/index.php/about-us";
            $www[6]['projects'][0]['name'] = "Olympic Lights";
            $www[6]['projects'][0]['url'] = "https://olympiclights.ru/";

            $www[7]['name'] = "ЮгСтройИнвест г.Краснодар";
            $www[7]['url'] = "https://gk-usi.ru/o-kompanii/istoriya-kompanii";
            $www[7]['projects'][0]['name'] = "Вересаево";
            $www[7]['projects'][0]['url'] = "https://veresaevo.ru/";
            $www[7]['projects'][1]['name'] = "Левобережье";
            $www[7]['projects'][1]['url'] = "https://levoberezhe.ru/";
            $www[7]['projects'][2]['name'] = "ЖК Квартет г.Ставрополь";
            $www[7]['projects'][2]['url'] = "https://kvartet-26.ru/";

            $www[8]['name'] = "RODINA Development г.Тюмень";
            $www[8]['url'] = "https://rodina-dom.ru/sz-rodina/o-kompanii/";
            $www[8]['projects'][0]['name'] = 'ЖК "ОК!Лэнд"';
            $www[8]['projects'][0]['url'] = "https://xn----ttbkbr9d.xn--p1ai/";
            $www[8]['projects'][1]['name'] = 'ЖК "Преображенский", ЖК "Правобережный" и другие';

            $www[9]['name'] = "СК Гарантия";
            $www[9]['url'] = "https://skgarantiya.ru";
            $www[9]['projects'][0]['name'] = 'PRIME';
            $www[9]['projects'][0]['url'] = 'https://garantiya-prime.ru/';
            $www[9]['projects'][1]['name'] = 'прочие 4 проекта';
            $www[9]['projects'][1]['url'] = "https://skgarantiya.ru/complexes";

            $www[10]['name'] = "СК Новая Жизнь";
            $www[10]['url'] = "https://shabolovskiy.com/about/";
            $www[10]['projects'][0]['name'] = 'Новая Жизнь г.Яльяновск';
            $www[10]['projects'][0]['url'] = "https://newlife-ul.ru/";
            $www[10]['projects'][1]['name'] = 'Шаболовский г.Москва';
            $www[10]['projects'][1]['url'] = "https://shabolovskiy.com/";

            $www[10]['name'] = "Суварстроит г.Казань";
            $www[10]['url'] = "https://suvarstroit.ru/News";
            $www[10]['projects'][0]['name'] = 'Времена года';
            $www[10]['projects'][0]['url'] = "https://suvarstroit.ru/zhk-vremena-goda";
            $www[10]['projects'][1]['name'] = 'Столичный';
            $www[10]['projects'][1]['url'] = "https://suvarstroit.ru/stolichnyj";
            $www[10]['projects'][2]['name'] = 'Сказочный лес';
            $www[10]['projects'][2]['url'] = "https://suvarstroit.ru/skazochnyj-les";
            $www[10]['projects'][3]['name'] = 'Палитра';
            $www[10]['projects'][3]['url'] = "https://suvarstroit.ru/palitra";
            $www[10]['projects'][4]['name'] = 'и другие проекты';
            $www[10]['projects'][4]['url'] = "https://suvarstroit.ru/palitra";

        ?>

        <ul>
            <?php foreach ($www as $wkey => $wvalue): ?>
                <li>
                    <?php if(isset($wvalue['url'])): ?>
                        <a href="<?= $wvalue['url'] ?>" target="_blank"><?= $wvalue['name'] ?></a>
                    <?php else: ?>
                        <?= $wvalue['name'] ?>
                    <?php endif; ?>

                    <?php if(count($wvalue['projects']) > 0): ?>

                        <?php if(count($wvalue['projects']) == 1): ?>
                            в т.ч. проект
                        <?php elseif(count($wvalue['projects']) > 1): ?>
                            в т.ч. проекты
                        <?php endif; ?>

                        <?php $i = 1; ?>
                        <?php foreach ($wvalue['projects'] as $pkey => $pvalue): ?>
                            <?php if(isset($pvalue['url'])):?>
                                <a href="<?= $pvalue['url'] ?>" target="_blank"><?= $pvalue['name'] ?></a><?php if($i < count($wvalue['projects'])):?>, <?php endif;?>
                            <?php else: ?>
                                <?= $pvalue['name'] ?><?php if($i < count($wvalue['projects'])):?>, <?php endif;?>
                            <?php endif; ?>
                            <?php $i++; ?>
                        <?php endforeach; ?>

                    <?php endif; ?>
                </li>
            <?php endforeach; ?>
        </ul>

        <p>И многие другие проекты и компании-застройщики используют наш сервис для повышения своих продаж. </p>

        <p>Ежегодно мы принимаем участие в реализации недвижимости многих строящихся объектов.
            Совокупный оборот всей реализуемой недвижимости давно стала превышать не один десяток миллиардов рублей.</p>

        <p>По утверждениям наших клиентов процент реализуемый недвижимости с помощью нашего сервиса составляет не
            менее 20% от совокупного оборота. Уже со второго месяца доля заинтересованных
            покупателей увеличивается на 30%.</p>

        <p>Обращайтесь к нашему cпециалисту, чтобы получить консультацию по внедрению
            сервиса в ваш действующий отдел продаж.</p>

    </div>
    <div>
        <h2>② Видео отзывы о нашем сервисе</h2>
        <p>Предлагаем вам ознакомиться с видео отзывыми наших клиентов</p>
        <?php
        $video = [];
        $video[0]['video_url'] = "https://disk.yandex.ru/i/dRjuuk13OoMiQg";
        $video[0]['nisha'] = "строительство квартирных домов и продажа";
        $video[0]['www_url'] = "suvarstroit.ru";
        $video[0]['promo_text'] = "С вашим сервисом работаем с июня 22 года. нас заинтересовала и мы решили протестировать, после успешного теста взяли минимальный пакет, а потом как он закончился взяли повышенный пакет. Учитывая нашу сферу и посмотреть статистику, из 100 заитересованных 15 в продажу. Это хороший результат";
        ?>
        <?php $i = 1; ?>
        <?php foreach($video as $value): ?>
            <li>
                Ссылка на видео-отзыв: <a href="<?= $value['video_url'] ?>" target="_blank"><?= $value['video_url'] ?></a>.<br>
                Ниша: <?= $value['nisha'] ?>.<br>
                О сотрудничестве: "<?= $value['promo_text'] ?>".<br>
                Сайт: <a href="<?= $value['www_url'] ?>" target="_blank"><?= $value['www_url'] ?></a>.<br>
            </li>
            <?php $i++; ?>
        <?php endforeach; ?>

        <p>
            Больше отзывов вы сможете найти в нашем телеграмм канале <a href="https://t.me/ya_leader" target="_blank">https://t.me/ya_leader</a>, на сайте
            или по запросу у вашего менеджера.
        </p>
    </div>
    <div>
        <h2>③ Звонки по определениям</h2>
        <p>Очень важно следовать всем рекомендациям, чтобы с клиентом состоялся продуктивный диалог.</p>
        <p>Примеры нескольких важных пунктов: своевременность, соблюдение скриптов, уверенность и доброжелательность в голосе.
            Эффективность нашего продукта зависит от понимания структуры работы.</p>
        <p>Если в работе руководствоваться не только своим опытом, а еще использовать данные в работу инструменты,
            то можно значительно увеличить успех своего звонка. </p>
        <p>Несколько примеров работы нашего колл-центра по определившимся номерам телефонов:</p>
        <ul>
            <?php
            $voice = [];
            $voice [] = "https://drive.google.com/file/d/1X0akKelYl8kPr1vJ-McLySKpKb197v6k/view?usp=sharing";
            $voice [] = "https://drive.google.com/file/d/1j4mWwMHFxceadog3eP-tC2qiT9ms0jjr/view?usp=sharing";
            $voice [] = "https://drive.google.com/file/d/1Cp54I8nVmR8UsQLGqti0Hkm_lHpTJryC/view?usp=sharing";
            $voice [] = "https://drive.google.com/file/d/13YOP6x-Tmcwpd-L1h0utuDRa1mKf7AGj/view?usp=sharing";
            $voice [] = "https://drive.google.com/file/d/1e2shfhTBi1UnrVNM_2BMBHjWmDN8YOrL/view?usp=sharing";
            $voice [] = "https://drive.google.com/file/d/1vRJk5fpLMKpB7gLCdxP0wfD2PzCDWOjT/view?usp=sharing";
            $voice [] = "https://drive.google.com/file/d/1NsZeFqFIcGap8AZxI-Wqs7wrHh0zmrby/view?usp=sharing";
            $voice [] = "https://drive.google.com/file/d/1eItVGkKZLFhc7PlRrkuNgaKq1YfR6YM5/view?usp=sharing";
            $voice [] = "https://drive.google.com/file/d/1X0akKelYl8kPr1vJ-McLySKpKb197v6k/view?usp=sharing";
            $voice [] = "https://drive.google.com/file/d/1oFDlk-fJdpVRJa4Okm11zbITtOs09gcJ/view?usp=sharing";
            $voice [] = "https://drive.google.com/file/d/1Fx_TRmHy63cTBjx8kx-TUFXdMyFiuVbJ/view?usp=sharing";
            ?>
            <?php $i = 1; ?>
            <?php foreach($voice as $key => $value): ?>
                <li><a href='<?= $value ?>' target="_blank">пример работы колл-центра <?= $i ?></a></li>
                <?php $i++; ?>
            <?php endforeach; ?>
        </ul>
    </div>

    <div>
        <h2>④ Подробная видео-презентация о нашем сервисе</h2>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/F5P4lwaWGOs?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>

    <div>
        <h2>⑤ Контакты</h2>
        <p>Будем рады ответить на ваши вопросы в любое время.</p>
        <p>Работаем с 07:00 - 22:00 Мск</p>
        <p>+7(993)992-12-34 <a href="https://wa.me/79939921234">whatsapp</a> <a href="https://t.me/79939921234">telegram</a></p>
        <p>&mdash; <a href="https://ya-leader.ru">ЯЛидер!</a></p>
    </div>
</article>
</body>
</html>
