<?php


namespace app\commands;


use app\components\PropHelper;
use app\components\XlsImportHelper;
use app\modules\ids\controllers\CreateController;
use app\modules\ids\models\Ids;
use app\modules\ids_import\models\IdsImportFile;
use app\modules\ids_import\models\IdsImportName;
use yii\console\Controller;
use yii\db\Query;

class ServiceController extends Controller
{
    public function actionRunImport($importId, $fileId = 0, $confirm = 0, $linesLimit = 0)
    {
        $importNameModel = IdsImportName::findOne($importId);
        if(!$importNameModel) {
            throw new \Exception("Import Name by id not found");
        }
        if($fileId) {
            $fileModel = IdsImportFile::findOne($fileId);
            if(!$fileModel) {
                throw new \Exception("File model by id not found");
            }
            if($fileModel->ids_import__name_id != $importNameModel->id) {
                throw new \Exception("Import ".$importId. "doesn't have file ".$fileId);
            }
        }
        else {
            $fileModel = $importNameModel->fileLast;
            if(!$fileModel) {
                throw new \Exception("Last file model not found");
            }
        }

        if($confirm) {
            XlsImportHelper::runImport($fileModel, $linesLimit);
        }
        else {
            echo PHP_EOL;
            echo 'Import : '.$importNameModel->id.' : '.$importNameModel->name.PHP_EOL;
            echo 'File : '.implode(' : ', [$fileModel->md5.'.'.$fileModel->ext, $fileModel->getPath(), $fileModel->created_at, 'exists:'.(file_exists($fileModel->getPath())?1:0)]).PHP_EOL;
        }
    }

    public function actionSetNameFromTemplate()
    {
        /** @var Ids $ids */
        $ids = Ids::findOne(92);
        CreateController::setNameFromNameTemplate($ids);
//        var_dump($ids->templateName->name_template);
//        var_dump($ids->name);
    }

    public function actionDeleteItemsByTemplateId($templateId)
    {
        $ids = (new Query())->select('id')->from('ids')->where(['template_name_id' => $templateId])->column();
        foreach($ids as $id) {
            PropHelper::deleteItemsWithAllProps($id);
        }
    }
}