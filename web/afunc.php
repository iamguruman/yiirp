<?php

use yii\helpers\Html;

if(!function_exists("aHtmlButtonPostAjax")){
    function aHtmlButtonPostAjax($title, $url, $success = null, $class = 'btn btn-primary'){

        $urlTo = \yii\helpers\Url::to($url);

        return Html::a(
            $title,
            $url,
            [
                'class' => $class,
                'onclick'=> "
                     $.ajax({
                        type     :'POST',
                        cache    : false,
                        url  : '{$urlTo}',
                        success  : function(response) {
                            {$success}
                            //$('#close').html(response);
                        }
                    });return false;",
            ]);

        return Html::a("<i class='fas fa-copy'></i> Копировать",
            ['create', 'created_from' => $model->id],
            ['class' => "btn btn-primary"]
        );

    }
}


/**
 *
 * в таблицу GridView выводит колонку ID, в которой указывает ID объекта и его состояние удален\нет,
 * если удален, то отображает красную корзину
 * колонке в GridView необходимо установить форма raw: 'format' => 'raw'
 *
 * @param $model
 * @return string
 */
function aGridVIewColumnId($model, $field = 'id', $prefix = null){

    /* [ 'attrbiute' => 'id', 'value' => function($model) { return aGridVIewColumnId($model); } ], */

    $ret = [];

    $ret [] = $prefix ? $prefix.$model->$field : $model->$field;

    if($model->markdelBy){
        $ret [] = "<i class='fas fa-trash' style='color:red' title='{$model->markdel_at}'></i>";
    }

    return implode("&nbsp;", $ret);

}

if(!function_exists("aHtmlHeader")){
    function aHtmlHeader($text, $module = null, $controller = null, $action = null, $level = "h1"){
        if(Yii::$app->controller->module->id == $module
            && Yii::$app->controller->id == $controller
            && Yii::$app->controller->action->id == $action){

            return "<{$level}>".Html::encode($text)."</{$level}>";

        }
    }
}

function aIfHideMarkdel(){

    return;

    if(!Yii::$app->user->isGuest){
        if (Yii::$app->user->identity->deleted_visibility == '1') {
            //$msg_deleted = 'Удаленные отображаются сейчас';

            return false;

        } elseif (Yii::$app->user->identity->deleted_visibility == '0') {
            return true;
            //$msg_deleted = 'Удаленные сейчас скрыты от просмотра';
            //$this->andWhere(['m_merchantdata__berito.markdel_by' => null]);
        }
    }
}

if(!function_exists("aJsonPrettyPrint")) {
    function aJsonPrettyPrint($json, $return_with_pre = false)
    {
        $result = '';
        $level = 0;
        $in_quotes = false;
        $in_escape = false;
        $ends_line_level = NULL;
        $json_length = strlen($json);

        for ($i = 0; $i < $json_length; $i++) {
            $char = $json[$i];
            $new_line_level = NULL;
            $post = "";
            if ($ends_line_level !== NULL) {
                $new_line_level = $ends_line_level;
                $ends_line_level = NULL;
            }
            if ($in_escape) {
                $in_escape = false;
            } else if ($char === '"') {
                $in_quotes = !$in_quotes;
            } else if (!$in_quotes) {
                switch ($char) {
                    case '}':
                    case ']':
                        $level--;
                        $ends_line_level = NULL;
                        $new_line_level = $level;
                        break;

                    case '{':
                    case '[':
                        $level++;
                    case ',':
                        $ends_line_level = $level;
                        break;

                    case ':':
                        $post = " ";
                        break;

                    case " ":
                    case "\t":
                    case "\n":
                    case "\r":
                        $char = "";
                        $ends_line_level = $new_line_level;
                        $new_line_level = NULL;
                        break;
                }
            } else if ($char === '\\') {
                $in_escape = true;
            }
            if ($new_line_level !== NULL) {
                $result .= "\n" . str_repeat("\t", $new_line_level);
            }
            $result .= $char . $post;
        }

        if($return_with_pre){
            return "<pre>{$result}</pre>";
        } else {
            return $result;
        }
    }
}

if(!function_exists('aDateNow')) {
    function aDateNow($format = 'Y-m-d H:i:s') {
        return date($format);
    }
}

if(!function_exists('aUserMyId')) {
    function aUserMyId() {
        return Yii::$app->user->id;
    }
}

if(!function_exists("aIfModuleControllerAction")){
    function aIfModuleControllerAction($module, $controller, $action){
        if(Yii::$app->controller->module->id == $module
            && Yii::$app->controller->id == $controller
            && Yii::$app->controller->action->id == $action
        )
        {
            return true;
        } else {
            return false;
        }
    }
}

if(!function_exists("aGet")){
    function aGet($varName) {
        return Yii::$app->request->get($varName) ?? null;
    }
}

if(!function_exists("aTabRequestUri")){
    function aTabRequestUri($tab_name, $source = null) {

        $ret = [];

        if(!$source){

            $reqUri = $_SERVER['REQUEST_URI'];
            $reqUri = str_replace("&tab={$tab_name}", "", $reqUri);
            $reqUri = str_replace("tab={$tab_name}", "", $reqUri);

            $ret [] = $reqUri;

        }

        $ret [] = "tab={$tab_name}";

        return implode("&", $ret);
    }
}

if(!function_exists("aModuleId")) {
    function aModuleId() {
        return Yii::$app->controller->module->id;
    }
}

if(!function_exists("aControllerId")) {
    function aControllerId() {
        return Yii::$app->controller->id;
    }
}

if(!function_exists("aActionId")) {
    function aActionId() {
        return Yii::$app->controller->action->id;
    }
}

if(!function_exists("aReturnto")) {
    /** @var $this \yii\web\Controller */
    function aReturnto($var = null, $thisController = null)
    {
        if ($returnto = urldecode(Yii::$app->request->get('returnto'))) {
            if ($var){
                header("location: {$returnto}&{$var}");
            }
            header("location: {$returnto}");
            exit();
        }
    }
}

if(!function_exists("ddd")) {
    function ddd($var = '', $var_name = null)
    {
        dd($var, $var_name, 'ddd');
        exit();
    }
}

if(!function_exists("dd")) {
    function dd($var = '', $var_name = null)
    {

        d($var, $var_name, 'dd');

        //if(aUserMyId() == 1){
        echo "<fieldset style='margin-bottom: 10px;'>";
        if ($var_name) {
            echo "<legend>{$var_name}</legend>";
        }
        echo "<pre>";
        var_dump($var);
        echo "</pre>";
        echo "</fieldset>";
        //}
    }
}

if(!function_exists("d")) {
    function d($var = '', $var_name = null, $type = 'd')
    {
        $model = new \app\modules\dev_messages\models\DevMessages();

        if(!Yii::$app->user->isGuest){
            $model->created_by = aUserMyId();
        }

        $model->created_at = aDateNow();

        $model->name = $var_name;

        if(gettype($var) == 'boolean'){
            if($var == true){
                $model->value = "true";
            } else {
                $model->value = "false";
            }
        } else {
            $model->value = print_r($var, true);
        }

        $model->type = $type;
        $model->module_id = aModuleId();
        $model->controller_id = aControllerId();
        $model->action_id = aActionId();

        /*if(isset($_SERVER['REQUEST_URI'])){
            $model->request_uri = $_SERVER['REQUEST_URI'];
        }

        if(isset($_SERVER['REQUEST_METHOD'])){
            $model->request_method = $_SERVER['REQUEST_METHOD'];
        }*/

        if($model->save()){

        } else {
            ddd($model->errors);
        }
    }
}