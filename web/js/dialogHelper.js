$(() => {
    window.dialogHelper = {
        count: 0,
        curOpenPos: 0,
        getMyPosition: function () {
            return 'center center';
            var map = [
                // 'left center',
                // 'center center',
                // 'right center',
                'center top',
                'center bottom',
            ];
            return map[dialogHelper.curOpenPos];
        },
        updateMyPosition: function () {
            dialogHelper.curOpenPos++;
            if (dialogHelper.curOpenPos > 1) {
                dialogHelper.curOpenPos = 0;
            }
        },

        getTpl: (title, content, idNo) => {
            return `
                <div id="dialog${idNo}" title="${title}">
                    ${content}
                </div>
`;
        },
        open: (title, content, width, height) => {
            dialogHelper.count++;
            let dialogsContainerSelector = $('#dialogs-container');
            if(!dialogsContainerSelector.length) {
                dialogsContainerSelector = $('body');
            }
            // let dialogsContainerSelector = $('#dialogs-container');
            dialogsContainerSelector.append(dialogHelper.getTpl(title, content, dialogHelper.count));
            $('#dialog' + dialogHelper.count).dialog({
                open: function () {

                    $(this).closest(".ui-dialog")
                        .find(".ui-dialog-titlebar-close")
                        // .removeClass("ui-dialog-titlebar-close")
                        .html("<span class='ui-button-icon-primary ui-icon ui-icon-closethick' style='margin-top:-10px'></span>");


                    $(this).closest(".ui-dialog").find('.ui-dialog-content').css('padding', '0 0 0 0');
                },
                // width: Number(dialogsContainerSelector.css('width').split('px')[0])/1,
                width: width ? width : Math.floor(window.innerWidth * 0.5),
                // height: Number(dialogsContainerSelector.css('height').split('px')[0])/3.15,
                // height: Math.floor(Number(dialogsContainerSelector.css('height').split('px')[0])/2),
                height: height ? height : Math.floor(window.innerHeight * 0.5),
                position: {
                    // within: ('#main'),
                    // of: ('#main'),
                    // at: (atPosition ? atPosition : "left top"), //where to place
                    // my: "center" //what to place

                }
            }).dialogExtend({
                "maximizable": true,
                "minimizable": true,
                "collapsable": true,
                "dblclick": "maximize",
                "icons": {"maximize": "ui-icon-arrow-4-diag"}
            });
            $('#dialog' + dialogHelper.count).css('overflow', 'hidden');
        },
        openNode: (nodeId, width, height) => {
            var iframeHtml = '<iframe src="/ids/view?id=' + nodeId + '&view=ajax" frameborder="0" width="100%" height="100%" scrolling="yes" id="ids_iframe_' + nodeId + '"></iframe>';
            dialogHelper.open('Ids ' + nodeId, iframeHtml, width, height);
            $('#ids_iframe_' + nodeId).on('load', function () {
                var title = $(this).contents().find('h1').html();
                $(this).parent().parent().find('.ui-dialog-title').html(title);
            });
        }
    }
});