$(() => {

    window.fancyTreeHelper = function() {

        var src = null;

        var urlSearchParams = (new URLSearchParams(window.location.search));
        var nodeId = urlSearchParams.get('nodeId') ?? urlSearchParams.get('id');
        var rootId = urlSearchParams.get('rootId');
        var idsNamePart = urlSearchParams.get('idsNamePart');
        var statusId = urlSearchParams.get('statusId');

        var templateId = urlSearchParams.get('templateId');
        var limit = urlSearchParams.get('limit');
        var offset = urlSearchParams.get('offset');
        var showAll = urlSearchParams.get('showAll');

        var pageSize = 20;


        // console.log(pageSize);


        var init = () => {
            if(showAll) {
                $('#tree-load-more-link').hide();
            }
            fetchSrc((data) => {
                src = data;
                initPlugins();
            }, 0 , pageSize);
        };

        var getQueryObject = (offset, limit) => {
            return {
                nodeId: nodeId,
                rootId: rootId,
                idsNamePart : idsNamePart,
                statusId : statusId,
                templateId : templateId,
                limit : limit,
                offset : offset,
                showAll: showAll
            };
        };

        var fetchSrc = (callback, offset, limit) => {
            $.get('/ids_tree_fancy/default/ajax-node-list?' + $.param(getQueryObject(offset, limit)), (data) => {
                callback(data);
            });
        };

        var initPlugins = () => {
            //init plugins
            $("#fancytree").fancytree({
                /*
                source: [
                    {title: "Node 1", key: "1"},
                    {
                        title: "Folder 2", key: "2", folder: true, children: [
                            {title: "Node 2.1", key: "3"},
                            {title: "Node 2.2", key: "4"}
                        ]
                    }
                ],
                 */
                source: src,
                lazyLoad: function(event, data) {
                    var node = data.node;
                    // Issue an Ajax request to load child nodes
                    data.result = {
                        url: "/ids_tree_fancy/default/ajax-tree-level",
                        data: {rootId: node.key}
                    }
                },

                click: function(event, data) {
                    var node = data.node;
                    // Only for click and dblclick events:
                    // 'title' | 'prefix' | 'expander' | 'checkbox' | 'icon'
                    targetType = data.targetType;
                    if(targetType === 'title') {
                        data.node.setExpanded(true);
                    }
                    // console.log(targetType);
                    // we could return false to prevent default handling, i.e. generating
                    // activate, expand, or select events
                },
                beforeExpand: function(event, data) {

                    // console.log(one, two, three);
                },


                dblclick: function(event, data){
                    if(event.originalEvent && (event.originalEvent.type==='dblclick')) {
                        // alert(data.node.key);
                        if($('#dialogs-container').length) {
                            dialogHelper.openNode(data.node.key);
                        }
                        else if(window !== window.parent) {
                            parent.dialogHelper.openNode(data.node.key, 800, 480);
                        }
                        else {
                            dialogHelper.openNode(data.node.key, 800, 480);
                        }
                        return false;
                    }
                }
            });

            $.contextMenu({
                selector: "#fancytree span.fancytree-title",
                items: {
                    'open': {
                        name: 'Открыть',
                        callback: function(key, opt) {
                            var node = $.ui.fancytree.getNode(opt.$trigger);
                            window.open('/ids/view?id='+node.key, '_self');
                        }
                    },
                    'openInNewWindow': {
                        name: 'Открыть в новом окне',
                        callback: function(key, opt) {
                            var node = $.ui.fancytree.getNode(opt.$trigger);
                            window.open('/ids/view?id='+node.key, '_blank');
                        }
                    },
                    'update': {
                        name: 'Редактировать',
                        callback: function(key, opt) {
                            var node = $.ui.fancytree.getNode(opt.$trigger);
                            window.open('/ids/update?id='+node.key, '_blank');
                        }
                    },
                    'copy': {
                        name: 'Копировать',
                        callback: function(key, opt) {
                            var node = $.ui.fancytree.getNode(opt.$trigger);
                            window.open('/ids/copy?id='+node.key, '_blank');
                        }
                    },

                    /*
                    "cut": {name: "Cut", icon: "cut",
                        callback: function(key, opt){
                            var node = $.ui.fancytree.getNode(opt.$trigger);
                            alert("Clicked on " + key + " on " + node);
                            dialogHelper.open('1', '1');
                        }
                    },
                    "copy": {name: "Copy", icon: "copy"},
                    "paste": {name: "Paste", icon: "paste", disabled: false },
                    "sep1": "----",
                    "edit": {name: "Edit", icon: "edit", disabled: true },
                    "delete": {name: "Delete", icon: "delete", disabled: true },
                    "more": {name: "More", items: {
                            "sub1": {name: "Sub 1"},
                            "sub1": {name: "Sub 2"}
                        }}
                     */
                }
                /*
                ,
                callback: function(itemKey, opt) {
                    var node = $.ui.fancytree.getNode(opt.$trigger);
                    alert("select " + itemKey + " on " + node);
                    dialogHelper.open('2', '2');
                }
                 */
            });
            //init plugins end

            $('#tree-load-more-link').click((e) => {
               e.preventDefault();
               treeLoadPlus();
            });
        };

        var treeReload = () => {
            fetchSrc((data) => {
                src = data;
                var tree = $("#fancytree").fancytree('getTree');
                tree.reload(src);
                $('#tree-load-more-link').show();
                replaceHistoryByParams();
            }, 0, pageSize);
        };

        var treeLoadPlus = () => {
            var elementsQty = src.length;
            var offset = Math.max(elementsQty, pageSize);
            fetchSrc((data) => {
                if(!data.length) {
                    $('#tree-load-more-link').hide();
                }
                src = [...src, ...data];
                var tree = $("#fancytree").fancytree('getTree');
                tree.reload(src);
            }, offset, pageSize);
        };

        var replaceHistoryByParams = () => {
            const urlBase = window.location.href.split('?')[0];
            const urlTail = $.param(getQueryObject(offset, limit));
            history.replaceState(undefined, undefined, urlBase + '?' + urlTail);
        }

        return {

            replaceParam : (paramName, paramValue) => {
                eval(paramName+' = "'+paramValue+'"');
            },

            treeLoadPlus: treeLoadPlus,
            treeReload : treeReload,

            initPlugins: initPlugins,

            init: init,

            replaceHistoryByParams: replaceHistoryByParams,

            getSrc: () => {
                return src;
            }
        }
    }();

    fancyTreeHelper.init();


});