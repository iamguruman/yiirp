$(() => {
   window.filterHelper = function() {
       var refreshTemplateIdInput = (currentTemplateId) => {
           $('#templateId-filter').select2('destroy');
           $('#templateId-filter').remove();
           $.get('/ids_tree_fancy/default/ajax-template-list', function(data) {
              $('#templateId-filter-container').html('<select id="templateId-filter" name="templateId">'+ '<option value="">Все</option>' + data.results.map( el => `<option value="${el.id}" ${(el.id == currentTemplateId) ? 'selected' :''}>${el.text}</option>`).join('') + '</select>');
              $('#templateId-filter').select2();
           });
       };
       var refreshStatusIdInput = (templateId, currentStatusId) => {
           $('#statusId-filter').select2('destroy');
           $('#statusId-filter').remove();
           if(templateId === '') {
               return;
           }
           $.get('/ids_tree_fancy/default/ajax-status-list?templateId='+templateId, function(data) {
               $('#statusId-filter-container').html('<select id="statusId-filter" name="statusId">'+data.results.map( el => `<option value="${el.id}" ${(el.id == currentStatusId) ? 'selected' :''}>${el.text}</option>`).join('') + '</select>');
               $('#statusId-filter').select2();
           });
       };

       $(document).on('change', '#templateId-filter', () => {
           var templateId = $('#templateId-filter').val();
           refreshStatusIdInput(templateId);
           fancyTreeHelper.replaceParam('templateId', templateId);
           fancyTreeHelper.replaceParam('statusId', '');
           fancyTreeHelper.treeReload();
       });

       $(document).on('change', '#statusId-filter', () => {
           var statusId = $('#statusId-filter').val();
           fancyTreeHelper.replaceParam('statusId', statusId);
           fancyTreeHelper.treeReload();
       });

       refreshTemplateIdInput((new URLSearchParams(window.location.search)).get('templateId'));
       refreshStatusIdInput((new URLSearchParams(window.location.search)).get('templateId'), (new URLSearchParams(window.location.search)).get('statusId'));

       return {
            refreshTemplateIdInput : refreshTemplateIdInput,
            refreshStatusIdInput : refreshStatusIdInput
       }
   }();
});