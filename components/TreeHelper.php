<?php


namespace app\components;


use app\modules\ids\models\Ids;
use app\modules\ids_tree\models\IdsTree;

class TreeHelper
{
    /**
     * Начало просмотра. Используется, если известен узел.
     * @param $nodeId
     * @return array[]
     * @throws \Exception
     */
    private static function getSingleLeafAsParent($nodeId)
    {
        /** @var Ids $node */
        $node = Ids::find()->where(['id' => $nodeId])->one();
        if(!$node) {
            throw new \Exception("Node not found for id ".$nodeId);
        }
        $config = self::helper_getSingleLeafArray($node->id, $node->name, 'parent', true, $node->template_name_id);
        return [
            $config
        ];
    }

    /**
     * Фильтрованный список. Используется, если выбирается шаблон или поиск по имени.
     * Без родительских элементов.
     * @param null $nodeId
     * @param null $parentId
     * @param null $templateId
     * @param null $idsNamePart
     * @param null $statusId
     * @param null $offset
     * @param null $limit
     * @return array
     * @throws \Exception
     */
    public static function getListFiltered($nodeId = null, $parentId = null, $templateId = null, $idsNamePart = null, $statusId = null, $offset = null, $limit = null)
    {
        $query = Ids::find();

        //проверка прав
        $query->leftJoin('ids_template__user_access', 'ids.template_name_id = ids_template__user_access.ids_template_name_id');
        $query->where(['ids_template__user_access.can_list' => '1', 'ids_template__user_access.sys_user_id' => aUserMyId()]);
        //проверка прав - END

        $query->andFilterWhere(['ids.id' => $nodeId]);

        if($parentId) {
            $query->leftJoin('ids_tree', 'ids.id = ids_tree.child_ids_id');
            $query->andWhere(['ids_tree.parent_ids_id' => $parentId]);
        }

        $query->andFilterWhere(['template_name_id' => $templateId]);

        $query->andFilterWhere(['like', 'ids.name', $idsNamePart]);

        $query->andFilterWhere(['ids_template_status_name_id' => $statusId]);



        $query->offset($offset);
        $query->limit($limit);

        /** @var Ids[] $children */
        $children = $query->all();

        $ret = [];
        foreach($children as $child) {
            $ret[] = self::helper_getSingleLeafArray($child->id, $child->name, null, true, $child->template_name_id);
        }

        return $ret;
    }

    //todo level might be filtered as well
    public static function getTreeLevel($parentId)
    {
        if(is_null($parentId)) {
            throw new \Exception("parentId cannot be null");
        }

        $childrenIds = IdsTree::find()->select('child_ids_id')->distinct()->where(['parent_ids_id' => $parentId])->column();
        $parentIds = IdsTree::find()->select('parent_ids_id')->distinct()->where(['child_ids_id' => $parentId])->column();

        $ret = [];

        /** @var Ids[] $parents */
        $parents = Ids::find()->leftJoin('ids_template__user_access', 'ids.template_name_id = ids_template__user_access.ids_template_name_id')->where(['ids.id' => $parentIds, 'ids_template__user_access.can_list' => '1', 'ids_template__user_access.sys_user_id' => aUserMyId()])->all();
        foreach($parents as $parent) {
            $ret[] = self::helper_getSingleLeafArray($parent->id, $parent->name, 'parent', true, $parent->template_name_id);
        }

        /** @var Ids[] $children */
        $children = Ids::find()->leftJoin('ids_template__user_access', 'ids.template_name_id = ids_template__user_access.ids_template_name_id')->where(['ids.id' => $childrenIds, 'ids_template__user_access.can_list' => '1', 'ids_template__user_access.sys_user_id' => aUserMyId()])->all();
        foreach($children as $child) {
            $ret[] = self::helper_getSingleLeafArray($child->id, $child->name, 'child', true, $child->template_name_id);
        }

        return $ret;

    }

    private static function buildTreeArray_recursive($idsOrIdsId)
    {
        if(is_null($idsOrIdsId)) {
            throw new \Exception("idsOrIdsId cannot be null");
        }
        if($idsOrIdsId instanceof Ids) {
            $node = $idsOrIdsId;
        }
        else {
            /** @var Ids $node */
            $node = Ids::find()->where(['id' => $idsOrIdsId])->one();
            if(!$node) {
                throw new \Exception("Node not found for id ".$idsOrIdsId);
            }
        }

        $ret = self::helper_getSingleLeafArray($node->id, $node->name, null, true, $node->template_name_id);

        $childrenIds = IdsTree::find()->select('child_ids_id')->distinct()->where(['parent_ids_id' => $node->id])->column();
        /** @var Ids[] $children */
        $children = Ids::find()->where(['id' => $childrenIds])->all();

        if($children) {
            $ret['folder'] = true;
            foreach($children as $child) {
                $ret['children'][] = self::buildTreeArray_recursive($child);
            }
        }
        return $ret;

        //todo
        /*
         Use the source option to load the data via Ajax.

$("#tree").fancytree({
  source: {
    url: "/getTreeData",
    cache: false
  },
  ...

The Ajax service is expected to return valid JSON data:

[{"title": "Node 1", "key": "1"},
 {"title": "Folder 2", "key": "2", "folder": true, "children": [
    {"title": "Node 2.1", "key": "3"},
    {"title": "Node 2.2", "key": "4"}
  ]}
]
         */

        // м.б. вывод с иконками
        // контекстное меню
        //содержание карточек
    }

    private static function helper_getSingleLeafArray($nodeId, $nodeTitle, ?string $iconType, $lazy = true, $template_id = null)
    {
        if(!in_array($iconType,['parent', 'child', null], true)) {
            throw new \Exception("Wrong iconType ".$iconType);
        }
        $parentIconSrc = 'https://img.icons8.com/?size=38&id=79599&format=png';
        $childIconSrc = 'https://img.icons8.com/?size=38&id=DyyVagPJYKDI&format=png';

        if(is_null($iconType)) {
            $iconTypeDiv = '';
        }
        elseif ($iconType === 'parent') {
            $iconTypeDiv =
                <<<ICON_TYPE_DIV
<div style="display:flex">
     <div>
        <img height="15" src="{$parentIconSrc}">&nbsp;<img height="15" src="/tplicons/{$template_id}.png">&nbsp;
     </div> 
<div>
ICON_TYPE_DIV;
        }
        elseif ($iconType === 'child') {
            $iconTypeDiv =
                <<<ICON_TYPE_DIV
     <div>
        <img height="15" src="{$childIconSrc}">&nbsp;<img height="15" src="/tplicons/{$template_id}.png">&nbsp;
     </div>
ICON_TYPE_DIV;
        }


        return [
            'key' => $nodeId,
            'title' =>
                <<<TITLE_HTML
<div style="display:flex">
<div>
 {$iconTypeDiv}
 </div>
<div>
 {$nodeTitle}
 </div>
 </div>

TITLE_HTML
            ,
            'icon' => false,
//            'icon' => 'https://img.icons8.com/?size=38&id=79599&format=png',
        'lazy' => $lazy,

        ];
    }


}