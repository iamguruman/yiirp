<?php


namespace app\components;


use app\modules\ids\controllers\CreateController;
use app\modules\ids\models\Ids;
use app\modules\ids\models\IdsValuesHistory;
use app\modules\ids_template\models\TemplateField;
use app\modules\ids_template\models\TemplateName;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class PropHelper
{
    const DATA_TYPES = [
        'date',
        'datetime',
        'decimal102',
        'int11',
        'longblob',
        'text',
        'time',
        'varchar1000',
        'varchar255',
        'varchar500',
    ];

    public static function getItemProps(?array $where = null, ?array $having = null)
    {
//        $where =  $idsId ? 'ids.id = '.$idsId : 'TRUE';
        /*
        $sql = <<<SQL
SELECT
       ids.id as ids_id,
       ids.name as ids_name,
       ids_template_name.id as ids_template_name_id,
       ids_template_name.name as ids_template_name,
       ids_template_field.id as ids_template_field_id,
       ids_template_field.title as ids_template_field_title,
       ids_template_field.value_field as ids_template_field_value_field,

       IFNULL(ids_value_date.value, IFNULL(ids_value_datetime.value, IFNULL(ids_value_decimal102.value, IFNULL(ids_value_int11.value, IFNULL(ids_value_longblob.value, IFNULL(ids_value_text.value, IFNULL(ids_value_time.value, IFNULL(ids_value_varchar1000.value, IFNULL(ids_value_varchar255.value, IFNULL(ids_value_varchar500.value, NULL)))))))))) as value,
    IFNULL(ids_value_date.id, IFNULL(ids_value_datetime.id, IFNULL(ids_value_decimal102.id, IFNULL(ids_value_int11.id, IFNULL(ids_value_longblob.id, IFNULL(ids_value_text.id, IFNULL(ids_value_time.id, IFNULL(ids_value_varchar1000.id, IFNULL(ids_value_varchar255.id, IFNULL(ids_value_varchar500.id, NULL)))))))))) as value_id

FROM ids
         LEFT JOIN ids_template_name
                   ON ids.template_name_id = ids_template_name.id
         LEFT JOIN ids_template_field
                   ON ids_template_field.template_name_id = ids_template_name.id
         LEFT JOIN ids_value_date
                   ON ids_value_date.template_field_id = ids_template_field.id AND ids_value_date.ids_id = ids.id
         LEFT JOIN ids_value_datetime
                   ON ids_value_datetime.template_field_id = ids_template_field.id AND
                      ids_value_datetime.ids_id = ids.id
         LEFT JOIN ids_value_decimal102
                   ON ids_value_decimal102.template_field_id = ids_template_field.id AND
                      ids_value_decimal102.ids_id = ids.id
         LEFT JOIN ids_value_int11
                   ON ids_value_int11.template_field_id = ids_template_field.id AND ids_value_int11.ids_id = ids.id
         LEFT JOIN ids_value_longblob
                   ON ids_value_longblob.template_field_id = ids_template_field.id AND
                      ids_value_longblob.ids_id = ids.id
         LEFT JOIN ids_value_text
                   ON ids_value_text.template_field_id = ids_template_field.id AND ids_value_text.ids_id = ids.id
         LEFT JOIN ids_value_time
                   ON ids_value_time.template_field_id = ids_template_field.id AND ids_value_time.ids_id = ids.id
         LEFT JOIN ids_value_varchar1000
                   ON ids_value_varchar1000.template_field_id = ids_template_field.id AND
                      ids_value_varchar1000.ids_id = ids.id
         LEFT JOIN ids_value_varchar255
                   ON ids_value_varchar255.template_field_id = ids_template_field.id AND
                      ids_value_varchar255.ids_id = ids.id
         LEFT JOIN ids_value_varchar500
                   ON ids_value_varchar500.template_field_id = ids_template_field.id AND
                      ids_value_varchar500.ids_id = ids.id
         WHERE $where
SQL;
        */
//        $dbOut = \Yii::$app->db->createCommand($sql)->queryAll();
        $dbOut = self::getItemPropsQuery($where, $having)->all();
        return self::formatResultArr($dbOut);
    }

    public static function getItemPropsQuery(?array $where, ?array $having)
    {
        $query = new Query();
        $query->select([
            'ids_id' => 'ids.id',
            'ids_name' => 'ids.name',
            'ids_template_name_id' => 'ids_template_name.id',
            'ids_template_name' => 'ids_template_name.name',
            'ids_template_field_id' => 'ids_template_field.id',
            'ids_template_field_title' => 'ids_template_field.title',
            'ids_template_field_value_field' => 'ids_template_field.value_field',
            (new Expression('IFNULL(ids_value_date.value, IFNULL(ids_value_datetime.value, IFNULL(ids_value_decimal102.value, IFNULL(ids_value_int11.value, IFNULL(ids_value_longblob.value, IFNULL(ids_value_text.value, IFNULL(ids_value_time.value, IFNULL(ids_value_varchar1000.value, IFNULL(ids_value_varchar255.value, IFNULL(ids_value_varchar500.value, NULL)))))))))) as value')),
            (new Expression('IFNULL(ids_value_date.id, IFNULL(ids_value_datetime.id, IFNULL(ids_value_decimal102.id, IFNULL(ids_value_int11.id, IFNULL(ids_value_longblob.id, IFNULL(ids_value_text.id, IFNULL(ids_value_time.id, IFNULL(ids_value_varchar1000.id, IFNULL(ids_value_varchar255.id, IFNULL(ids_value_varchar500.id, NULL)))))))))) as value_id')),
        ]);
        $query->from('ids');
        $query->leftJoin('ids_template_name', 'ids.template_name_id = ids_template_name.id');
        $query->leftJoin('ids_template_field', 'ids_template_field.template_name_id = ids_template_name.id');
        $query->leftJoin('ids_value_date', 'ids_value_date.template_field_id = ids_template_field.id AND ids_value_date.ids_id = ids.id');
        $query->leftJoin('ids_value_datetime', 'ids_value_datetime.template_field_id = ids_template_field.id AND
                      ids_value_datetime.ids_id = ids.id');
        $query->leftJoin('ids_value_decimal102', 'ids_value_decimal102.template_field_id = ids_template_field.id AND
                      ids_value_decimal102.ids_id = ids.id');
        $query->leftJoin('ids_value_int11', 'ids_value_int11.template_field_id = ids_template_field.id AND ids_value_int11.ids_id = ids.id');
        $query->leftJoin('ids_value_longblob', 'ids_value_longblob.template_field_id = ids_template_field.id AND
                      ids_value_longblob.ids_id = ids.id');
        $query->leftJoin('ids_value_text', 'ids_value_text.template_field_id = ids_template_field.id AND ids_value_text.ids_id = ids.id');
        $query->leftJoin('ids_value_time', 'ids_value_time.template_field_id = ids_template_field.id AND ids_value_time.ids_id = ids.id');
        $query->leftJoin('ids_value_varchar1000', 'ids_value_varchar1000.template_field_id = ids_template_field.id AND
                      ids_value_varchar1000.ids_id = ids.id');
        $query->leftJoin('ids_value_varchar255', 'ids_value_varchar255.template_field_id = ids_template_field.id AND
                      ids_value_varchar255.ids_id = ids.id');
        $query->leftJoin('ids_value_varchar500', 'ids_value_varchar500.template_field_id = ids_template_field.id AND
                      ids_value_varchar500.ids_id = ids.id');

        $query->where($where);
        $query->having($having);
        return $query;
    }

    /**
     * @param \yii\db\DataReader $dbOut
     * @return array
     */
    public static function formatResultArr(array $dbOut): array
    {
        $ret = [];
        foreach ($dbOut as $line) {
            if (!isset($ret[$line['ids_id']])) {
                $ret[$line['ids_id']] = [
                    'ids_id' => $line['ids_id'],
                    'ids_name' => $line['ids_name'],
                    'props' => []
                ];
            }

            $ret[$line['ids_id']]['props'][$line['ids_template_field_id']] = [
                'ids_template_field_id' => $line['ids_template_field_id'],
                'ids_template_field_title' => $line['ids_template_field_title'],
                'ids_template_field_value_field' => $line['ids_template_field_value_field'],
                'value' => $line['value'],
                'value_id' => $line['value_id'],
            ];

        }
        return $ret;
    }

//    public static function searchItemsWithPropsViaAndMap()
//    {
//
//    }

    public static function searchItemsWithProps($templateNameId, array &$templateFieldIdToValueMap, array &$templateFieldIdToDataTableName, $useAndMode = true, $searchModeUseTitles = false)
    {
        if(!$templateFieldIdToValueMap) {
            throw new \Exception("TemplateFieldIdToValueMap cannot be empty");
        }
//        $pairConditions = ['or'];
//        if($templateFieldIdToValueMap) {
//            print_r($templateFieldIdToValueMap);
//            print_r($pairConditions);
//            exit();
//        }
        foreach($templateFieldIdToValueMap as $tplFieldId => $tplFieldValue) {
//            $pairConditions[] = [
//                'and',
//                ['ids_template_field.' . ($searchModeUseTitles ? 'title' : 'id') => $tplFieldId],
//                [
//                    'or',
//                    ['ids_value_date.value' => $tplFieldValue],
//                    ['ids_value_datetime.value' => $tplFieldValue],
//                    ['ids_value_decimal102.value' => $tplFieldValue],
//                    ['ids_value_int11.value' => $tplFieldValue],
//                    ['ids_value_longblob.value' => $tplFieldValue],
//                    ['ids_value_text.value' => $tplFieldValue],
//                    ['ids_value_time.value' => $tplFieldValue],
//                    ['ids_value_varchar1000.value' => $tplFieldValue],
//                    ['ids_value_varchar255.value' => $tplFieldValue],
//                    ['ids_value_varchar500.value' => $tplFieldValue],
//                ]
//            ];
            $pairConditions[] = [
                'ids_template_field.' . ($searchModeUseTitles ? 'title' : 'id') => $tplFieldId,
                $templateFieldIdToDataTableName[$tplFieldId].'.value' => $tplFieldValue
            ];
        }
        $where = [
            'and',
            [
                'ids_template_name.'.($searchModeUseTitles ? 'name' : 'id') => $templateNameId
            ],
            array_merge(['or'], $pairConditions)

        ];
//        $where = array_merge($where, $pairConditions);

        $query = self::getItemPropsQuery($where, null);
//        file_put_contents(__DIR__.'/../runtime/where_debug.txt', print_r($query->createCommand()->rawSql, true));
//        exit();
//        echo '<pre>';

        //todo debug pair conditions

//        echo '</pre>';
//        exit();
        $result = $query->all();
        if($useAndMode) {
            $newResult = [];
            $resultGroupedByIdsId = ArrayHelper::index($result, null, 'ids_id');
            foreach($resultGroupedByIdsId as $idsId => $resultGroup) {
                $templateFieldIdsFound = array_unique(ArrayHelper::getColumn($resultGroup, 'ids_template_field_id'));

                //todo this might be replaced by array_intersect to be more precise
                if(sizeof($templateFieldIdsFound) === sizeof(array_keys($templateFieldIdToValueMap))) {
                    $newResult = array_merge($newResult, $resultGroup);
                }
            }
            return $newResult;
        }
        else {
            return $result;
        }
    }

    public static function saveNewItem($templateNameId, array &$templateFieldIdToValueMap, array &$templateFieldIdToDataTableName)
    {
        /** @var TemplateName $templateNameModel */
        $templateNameModel = TemplateName::findOne($templateNameId);
        if(!$templateNameModel->name_template) {
            throw new \Exception("No name_template given");
        }

        $ids = new Ids();
        $ids->template_name_id = $templateNameId;
        $ids->save(false);

        foreach($templateFieldIdToValueMap as $templateFieldId => $value) {
            /** @var TemplateField $templateFieldModel */
            $templateFieldModel = TemplateField::findOne($templateFieldId);
            $templateFieldModel->value_field;
            \Yii::$app->db->createCommand()->insert('ids_value_'.$templateFieldModel->value_field, [
                'ids_id' => $ids->id,
                'template_field_id' => $templateFieldId,
                'value' => $value
            ])->execute();
        }

        $ids->refresh();
        CreateController::setNameFromNameTemplate($ids);
//        dd($templateFieldIdToValueMap);
//        exit();
    }

    public static function deleteItemsWithAllProps($idsId)
    {
        /** @var Ids $ids */
        $ids = Ids::findOne($idsId);
        foreach(self::DATA_TYPES as $dataType) {
            \Yii::$app->db->createCommand()->delete('ids_value_'.$dataType, ['ids_id' => $ids->id])->execute();
        }

        IdsValuesHistory::deleteAll(['ids_id' => $ids->id]);
        $ids->delete();

    }
}