<?php

namespace app\components;

use app\modules\ids_import\models\IdsImportField;
use app\modules\ids_import\models\IdsImportFile;
use app\modules\ids_template\models\TemplateField;
use Aspera\Spreadsheet\XLSX\Reader;
use yii\helpers\ArrayHelper;


class XlsImportHelper
{
    public static function runImport(IdsImportFile $idsImportFile, $linesLimit = 0)
    {
        //1 - init
        $templateId = $idsImportFile->idsImportName->ids_template_name_id;
        $fileModel = $idsImportFile;
        $idsImportNameModel = $fileModel->idsImportName;


        //2 - first maps
        /** @var IdsImportField[] $idsImportFields */
        $idsImportFields = IdsImportField::find()->where(['ids_import__name_id' => $idsImportNameModel->id])->all();
        $queryTemplateFieldNames = ArrayHelper::getColumn(array_filter($idsImportFields, function(IdsImportField $idsImportField) { return $idsImportField->is_equal; }), 'field_name_from_file'); //поля, которые используются для поиска, только названия полей
        $queryTemplateFieldIds = ArrayHelper::getColumn(array_filter($idsImportFields, function(IdsImportField $idsImportField) { return $idsImportField->is_equal; }), 'ids_template_field_id'); //поля, кторые используются для поиска, только id полей
        $templateFieldIdToFieldName = ArrayHelper::map($idsImportFields, 'ids_template_field_id', 'field_name_from_file'); //id шаблона к названию поля
        $templateFieldIdToValueTableName = array_map(function($value) { return 'ids_value_'.$value; }, TemplateField::find()->where(['id' => array_keys($templateFieldIdToFieldName)])->indexBy('id')->select('value_field')->column());

//        var_dump($templateFieldIdToValueTableName); exit();
        //


//        dd($idsImportFields);
//        dd($queryFieldNames);
//        dd($templateFieldIdToFieldName);
//        exit();

        //3 - get first row with titles
        $reader = new Reader();
        $reader->open($fileModel->getPath());

        $xlsFieldMap = null;
        foreach ($reader as $row) {
            if(!$xlsFieldMap) {
                $xlsFieldMap = $row;
                break;
            }
        }



//        var_dump($xlsFieldMap);
//        var_dump($templateFieldIdToFieldName);
//        exit();

        //4 - get xlsColumnNo to templateFieldIds maps
        $xlsColumnNoToTemplateFieldId = [];

        foreach($xlsFieldMap as $xlsFieldIndex =>  $xlsFieldTitle) {
            if(($templateFieldId = array_search($xlsFieldTitle, $templateFieldIdToFieldName))!==false) {
//                echo implode(' : ', [$xlsFieldTitle, $xlsFieldIndex, $templateFieldId]).'<br>';
                $xlsColumnNoToTemplateFieldId[$xlsFieldIndex] = $templateFieldId;
            }
        }

//        dd($queryTemplateFieldIds); exit();

        $xlsColumnNoToTemplateFieldIdSearchOnly = array_filter($xlsColumnNoToTemplateFieldId, function($templateFieldId) use($queryTemplateFieldIds) {
            return in_array($templateFieldId, $queryTemplateFieldIds);
        });


        $rowToSearchMap = function($row) use($xlsColumnNoToTemplateFieldIdSearchOnly) {
            $ret = [];
            foreach($row as $columnNo => $value) {
                if(isset($xlsColumnNoToTemplateFieldIdSearchOnly[$columnNo])) {
                    $ret[$xlsColumnNoToTemplateFieldIdSearchOnly[$columnNo]] = $value;
                }
            }
            return $ret;
        };

        $rowToTplMap = function($row) use($xlsColumnNoToTemplateFieldId) {
            $ret = [];
            foreach($row as $columnNo => $value) {
                if(isset($xlsColumnNoToTemplateFieldId[$columnNo])) {
                    $ret[$xlsColumnNoToTemplateFieldId[$columnNo]] = $value;
                }
            }
            return $ret;
        };

        //5 - iterate over rows

        $no = 0;
        foreach ($reader as $row) {
            if(!$no++) {
                continue;
            }
            if($linesLimit) {
                if ($no > ($linesLimit + 1)) {
                    break;
                }
            }

            //todo row to search map debug - returns empty array
            $templateFieldIdToXlsValueMapIsEqualOnly = $rowToSearchMap($row);
            $templateFieldIdToXlsValueMap = $rowToTplMap($row);
//            var_dump($templateFieldIdToXlsValueMap);
//            if(!$templateFieldIdToXlsValueMapIsEqualOnly) {
//                var_dump('no row search map, continue');
//                continue;
//            }
//            var_dump($templateFieldIdToXlsValueMapIsEqualOnly); exit();
            if(!$templateFieldIdToXlsValueMapIsEqualOnly || !PropHelper::searchItemsWithProps($templateId, $templateFieldIdToXlsValueMapIsEqualOnly, $templateFieldIdToValueTableName)) {
                //todo log, why saving was performed
                PropHelper::saveNewItem($templateId, $templateFieldIdToXlsValueMap, $templateFieldIdToValueTableName);
                var_dump('saving');
            }
            else {
                var_dump('not saving');
            }

            if(\Yii::$app instanceof \yii\console\Application && !($no % 50)) {
                echo PHP_EOL.$no;
            }
        }


//        dd($xlsColumnNoToTemplateFieldIdSearchOnly);
//        dd($xlsColumnNoToTemplateFieldId);
//        dd(memory_get_peak_usage());
//        exit();


        $reader->close();
//        PropHelper::searchItemsWithProps($idsImportFile->idsImportName->ids_template_name_id, );
    }
}