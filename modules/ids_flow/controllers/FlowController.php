<?php

namespace app\modules\ids_flow\controllers;

use app\modules\ids_flow\models\IdsFlow;
use app\modules\ids_flow\models\IdsFlowFieldConnectionSearch;
use app\modules\ids_flow\models\IdsFlowSearch;
use app\modules\ids_template\models\TemplateName;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FlowController implements the CRUD actions for IdsFlow model.
 */
class FlowController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all IdsFlow models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new IdsFlowSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single IdsFlow model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $model = $this->findModel($id);

        $fieldConnectionSearchModel = new IdsFlowFieldConnectionSearch();
        $fieldConnectionDataProvider = $fieldConnectionSearchModel->search($this->request->queryParams, [
            'ids_flow_id' => $model->id,
        ]);

        return $this->render('view', [
            'fieldConnectionSearchModel' => $fieldConnectionSearchModel,
            'fieldConnectionDataProvider' => $fieldConnectionDataProvider,

            'model' => $model,
        ]);
    }

    /**
     * Creates a new IdsFlow model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new IdsFlow();

        if($parent = TemplateName::findOne(aGet('parent_id'))){
            $model->parent_ids_template_name_id = $parent->id;
        }

        if($child = TemplateName::findOne(aGet('child_id'))){
            $model->child_ids_template_name_id = $child->id;
        }

        if ($this->request->isPost) {
            if ($model->load($this->request->post())){
                if($model->save()) {
                    aReturnto();
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    ddd($model->errors);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing IdsFlow model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing IdsFlow model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the IdsFlow model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return IdsFlow the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IdsFlow::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
