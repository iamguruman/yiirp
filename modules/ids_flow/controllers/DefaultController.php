<?php

namespace app\modules\ids_flow\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{

    public function actionIndex(){

        return $this->redirect("/ids_template/name");

    }

}