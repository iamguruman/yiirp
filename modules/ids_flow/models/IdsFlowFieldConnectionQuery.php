<?php

namespace app\modules\ids_flow\models;

/**
 * This is the ActiveQuery class for [[IdsFlowFieldConnection]].
 *
 * @see IdsFlowFieldConnection
 */
class IdsFlowFieldConnectionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return IdsFlowFieldConnection[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return IdsFlowFieldConnection|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
