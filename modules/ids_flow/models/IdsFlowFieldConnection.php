<?php

namespace app\modules\ids_flow\models;

use app\modules\ids_template\models\TemplateField;
use Yii;

/**
 * This is the model class for table "ids_flow_field_connection".
 *
 * @property int $id
 * @property int|null $created_by
 * @property string|null $created_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @property int|null $markdel_by
 * @property string|null $markdel_at
 *
 * @property int $ids_flow_id
 * @property-read IdsFlow $idsFlow
 *
 * @property int|null $parent_ids_template_field_id
 * @property-read TemplateField $parentIdsTemplateField
 *
 * @property int|null $child_ids_template_field_id
 * @property-read TemplateField $childIdsTemplateField
 *
 */
class IdsFlowFieldConnection extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ids_flow_field_connection';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ids_flow_id'], 'required'],
            [['id', 'created_by', 'updated_by', 'markdel_by', 'ids_flow_id',
                'parent_ids_template_field_id', 'child_ids_template_field_id'], 'integer'],
            [['created_at', 'updated_at', 'markdel_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'markdel_by' => Yii::t('app', 'Markdel By'),
            'markdel_at' => Yii::t('app', 'Markdel At'),
            'ids_flow_id' => Yii::t('app', 'Ids Flow ID'),
            'parent_ids_template_field_id' => Yii::t('app', 'Parent Ids Template Field ID'),
            'child_ids_template_field_id' => Yii::t('app', 'Child Ids Template Field ID'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return IdsFlowFieldConnectionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IdsFlowFieldConnectionQuery(get_called_class());
    }

    public function getIdsFlow(){
        return $this->hasOne(IdsFlow::class, ['id' => 'ids_flow_id']);
    }

    public function getParentIdsTemplateField(){
        return $this->hasOne(TemplateField::class, ['id' => 'parent_ids_template_field_id']);
    }

    public function getChildIdsTemplateField(){
        return $this->hasOne(TemplateField::class, ['id' => 'child_ids_template_field_id']);
    }
}
