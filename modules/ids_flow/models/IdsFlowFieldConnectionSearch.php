<?php

namespace app\modules\ids_flow\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\ids_flow\models\IdsFlowFieldConnection;

/**
 * IdsFlowFieldConnectionSearch represents the model behind the search form of `app\modules\ids_flow\models\IdsFlowFieldConnection`.
 */
class IdsFlowFieldConnectionSearch extends IdsFlowFieldConnection
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by', 'markdel_by', 'ids_flow_id',
                'parent_ids_template_field_id', 'child_ids_template_field_id'], 'integer'],
            [['created_at', 'updated_at', 'markdel_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $params2 = [])
    {
        $query = IdsFlowFieldConnection::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'markdel_by' => $this->markdel_by,
            'markdel_at' => $this->markdel_at,
            'ids_flow_id' => $this->ids_flow_id,
            'parent_ids_template_field_id' => $this->parent_ids_template_field_id,
            'child_ids_template_field_id' => $this->child_ids_template_field_id,
        ]);

        if(!empty($params2['ids_flow_id'])){
            $query->andWhere(['ids_flow_id' => $params2['ids_flow_id']]);
        }

        return $dataProvider;
    }
}
