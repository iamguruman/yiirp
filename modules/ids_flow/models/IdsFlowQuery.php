<?php

namespace app\modules\ids_flow\models;

/**
 * This is the ActiveQuery class for [[IdsFlow]].
 *
 * @see IdsFlow
 */
class IdsFlowQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return IdsFlow[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return IdsFlow|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
