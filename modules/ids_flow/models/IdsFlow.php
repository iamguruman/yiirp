<?php

namespace app\modules\ids_flow\models;

use app\modules\ids_template\models\IdsTemplateStatusName;
use app\modules\ids_template\models\TemplateName;
use Yii;

/**
 * This is the model class for table "ids_flow".
 *
 * @property int $id
 * @property int $created_by
 * @property string $created_at
 * @property int $updated_by
 * @property string $updated_at
 * @property int $markdel_by
 * @property string $markdel_at
 *
 * @property int $parent_ids_template_name_id
 * @property-read TemplateName $parentIdsTemplateName
 *
 * @property int $child_ids_template_name_id
 * @property-read TemplateName $childIdsTemplateName
 *
 * @property-read IdsTemplateStatusName $childIdsTemplateStatusName
 * @property integer $child_ids_template_status_name_id
 */
class IdsFlow extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ids_flow';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['child_ids_template_status_name_id'], 'integer'],
            [['parent_ids_template_name_id', 'child_ids_template_name_id'], 'required'],
            [['created_by', 'updated_by', 'markdel_by', 'parent_ids_template_name_id', 'child_ids_template_name_id'], 'integer'],
            [['created_at', 'updated_at', 'markdel_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'markdel_by' => Yii::t('app', 'Markdel By'),
            'markdel_at' => Yii::t('app', 'Markdel At'),
            'parent_ids_template_name_id' => Yii::t('app', 'Parent Ids Template Name ID'),
            'child_ids_template_name_id' => Yii::t('app', 'Child Ids Template Name ID'),
        ];
    }

    public function getChildIdsTemplateName(){
        return $this->hasOne(TemplateName::className(), ['id' => 'child_ids_template_name_id']);
    }

    public function getParentIdsTemplateName(){
        return $this->hasOne(TemplateName::className(), ['id' => 'parent_ids_template_name_id']);
    }

    /**
     * {@inheritdoc}
     * @return IdsFlowQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IdsFlowQuery(get_called_class());
    }

    public function getChildIdsTemplateStatusName(){
        return $this->hasOne(IdsTemplateStatusName::className(), ['id' => 'child_ids_template_status_name_id']);
    }
}
