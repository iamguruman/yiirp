<?php

use yii\bootstrap5\Tabs;
use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\modules\ids_flow\models\IdsFlow $model */

/** @var \app\modules\ids_flow\models\IdsFlowFieldConnectionSearch $fieldConnectionSearchModel */
/** @var \yii\data\ActiveDataProvider $fieldConnectionDataProvider */

$this->title = $model->id;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Template Names'),
    'url' => ['/ids_template']];

if($model->parentIdsTemplateName){
    $this->params['breadcrumbs'][] = ['label' => "Parent template: ".$model->parentIdsTemplateName->name,
        'url' => ['/ids_template/name/view', 'id' => $model->parent_ids_template_name_id,
            'tab' => 'flow']
    ];
}

if($model->childIdsTemplateName){
    $this->params['breadcrumbs'][] = ['label' => "Child template: ".$model->childIdsTemplateName->name,
        'url' => ['/ids_template/name/view', 'id' => $model->child_ids_template_name_id,
            'tab' => 'flow']
    ];
}

$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ids-flow-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'parentIdsTemplateName.name',
            'childIdsTemplateName.name',
            'childIdsTemplateStatusName.name',
        ],
    ]) ?>

    <?= Tabs::widget(['items' => [
        [
            'label' => "Field Connection ({$fieldConnectionDataProvider->totalCount})",
            'content' => "<br>".$this->render("@app/modules/ids_flow/views/field-connection/index.php", [
                    'searchModel' => $fieldConnectionSearchModel,
                    'dataProvider' => $fieldConnectionDataProvider,
                ])
        ],
    ]]) ?>

</div>
