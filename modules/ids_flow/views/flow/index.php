<?php

use app\modules\ids_flow\models\IdsFlow;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\modules\ids_flow\models\IdsFlowSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

if(aIfModuleControllerAction("ids_temaplte", "flow", "index")){
    $this->title = Yii::t('app', 'Ids Flows');
    $this->params['breadcrumbs'][] = $this->title;
}
?>
<div class="ids-flow-index">

    <?php if(Yii::$app->controller->action->id == 'index'): ?>
        <h1><?= Html::encode($this->title) ?></h1>
    <?php endif; ?>

    <p>
        <?php if(aIfModuleControllerAction("ids_template", "name", "view")): ?>
            <?= Html::a(Yii::t('app', 'Create  as Parent'),
                ['/ids_flow/flow/create',
                    'parent_id' => aGet("id") ],
                ['class' => 'btn btn-success']) ?>

            <?= Html::a(Yii::t('app', 'Create as Child'),
                ['/ids_flow/flow/create',
                    'child_id' => aGet("id") ],
                ['class' => 'btn btn-success']) ?>
        <?php endif; ?>

        <?php if(aIfModuleControllerAction("ids_template", "flow", "index")): ?>
            <?= Html::a(Yii::t('app', 'Create Ids Flow'),
                ['/ids_template/flow/create',
                    'template_id' => aControllerId() == "name" ? aGet("id") : null],
                ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',

            [
                'attribute' => 'parentIdsTemplateName.name',
                'format' => 'raw',
                'label' => 'Parent Template',
                'value' => function(IdsFlow $model){

                    $ret = [];

                    $ret [] =Html::a($model->parentIdsTemplateName->name,
                        ['/ids/create',
                            'template_id' => $model->parent_ids_template_name_id,
                            'child_id' => aGet('id')
                        ], ['class' => 'btn btn-primary']);

                    if(aIfModuleControllerAction("ids_template", "name", "view")){
                        $ret [] = Html::a("template options",
                            ['/ids_template/name/view', 'id' => $model->parent_ids_template_name_id],
                            ['class' => 'btn btn-primary']);
                    }

                    return implode("&nbsp;", $ret);
                }
            ],

            [
                'attribute' => 'childIdsTemplateName.name',
                'format' => 'raw',
                'label' => 'Child Template',
                'value' => function(IdsFlow $model){

                    $ret = [];

                    $ret [] = Html::a($model->childIdsTemplateName->name,
                        ['/ids/create',
                            'template_id' => $model->child_ids_template_name_id,
                            'parent_id' => aGet('id')
                        ], ['class' => 'btn btn-primary']);

                    if(aIfModuleControllerAction("ids_template", "name", "view")){
                        $ret [] = Html::a("template options",
                            ['/ids_template/name/view', 'id' => $model->child_ids_template_name_id],
                            ['class' => 'btn btn-primary']);
                    }

                    return implode("&nbsp;", $ret);
                }
            ],

            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, IdsFlow $model, $key, $index, $column) {
                    return Url::toRoute(["/ids_flow/flow/".$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
