<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\ids_flow\models\IdsFlow $model */

$this->title = Yii::t('app', 'Update Ids Flow: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ids Flows'), 'url' => ['index']];

if($model->parentIdsTemplateName){
    $this->params['breadcrumbs'][] = ['label' => "Parent template: ".$model->parentIdsTemplateName->name,
        'url' => ['/ids_template/name/view', 'id' => $model->parent_ids_template_name_id,
            'tab' => 'flow']
    ];
}

if($model->childIdsTemplateName){
    $this->params['breadcrumbs'][] = ['label' => "Child template: ".$model->childIdsTemplateName->name,
        'url' => ['/ids_template/name/view', 'id' => $model->child_ids_template_name_id,
            'tab' => 'flow']
    ];
}

$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];

$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

?>
<div class="ids-flow-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
