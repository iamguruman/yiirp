<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\ids_flow\models\IdsFlow $model */

$this->title = Yii::t('app', 'Create Ids Flow');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Template Names'),
    'url' => ['/ids_template']];

if($model->parentIdsTemplateName){
    $this->params['breadcrumbs'][] = ['label' => "Parent template: ".$model->parentIdsTemplateName->name,
        'url' => ['/ids_template/name/view', 'id' => $model->parent_ids_template_name_id,
            'tab' => 'flow']
    ];
}

if($model->childIdsTemplateName){
    $this->params['breadcrumbs'][] = ['label' => "Child template: ".$model->childIdsTemplateName->name,
        'url' => ['/ids_template/name/view', 'id' => $model->child_ids_template_name_id,
            'tab' => 'flow']
    ];
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ids-flow-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
