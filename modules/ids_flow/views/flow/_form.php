<?php

use app\modules\ids_template\models\IdsTemplateStatusName;
use app\modules\ids_template\models\IdsTemplateUserAccess;
use app\modules\ids_template\models\TemplateName;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\ids_flow\models\IdsFlow $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ids-flow-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent_ids_template_name_id')->widget(Select2::className(), [
        'data' => TemplateName::find()
            ->select(['name', TemplateName::tableName().'.id'])
            ->indexBy(TemplateName::tableName().'.id')
            ->joinWith('usersAccess')
            ->andWhere([IdsTemplateUserAccess::tableName().'.sys_user_id' => aUserMyId()])
            ->column(),
        'options' => [
            'placeholder' => 'Select a template ...',
            'onchange' => "changeTemplate();"
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <br>

    <?= $form->field($model, 'child_ids_template_name_id')->widget(Select2::className(), [
        'data' => TemplateName::find()
            ->select(['name', TemplateName::tableName().'.id'])
            ->indexBy(TemplateName::tableName().'.id')
            ->joinWith('usersAccess')
            ->andWhere([IdsTemplateUserAccess::tableName().'.sys_user_id' => aUserMyId()])
            ->column(),
        'options' => [
            'placeholder' => 'Select a template ...',
            'onchange' => "changeTemplate();"
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <br>

    <?php if($model->child_ids_template_name_id): ?>

        <?= $form->field($model, 'child_ids_template_status_name_id')->widget(Select2::className(), [
            'data' => IdsTemplateStatusName::find()
                ->select(['name', 'id'])
                ->andWhere(['ids_template_name_id' => $model->child_ids_template_name_id])
                ->indexBy('id')
                ->orderBy(['sort' => SORT_ASC])
                ->column(),
            'options' => ['placeholder' => 'Select a template ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>

    <?php else: ?>

        <p>После сохранения можно выбрать статус у нового объекта</p>

    <?php endif; ?>

    <br>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
