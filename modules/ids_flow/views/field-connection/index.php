<?php

use app\modules\ids_flow\models\IdsFlowFieldConnection;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this */
/** @var app\modules\ids_flow\models\IdsFlowFieldConnectionSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

if(aIfModuleControllerAction("ids_flow","field-connection","index")){
    $this->title = Yii::t('app', 'Ids Flow Field Connections');
    $this->params['breadcrumbs'][] = $this->title;
}
?>
<div class="ids-flow-field-connection-index">

    <?= aHtmlHeader($this->title, "ids_flow","field-connection","index") ?>

    <p>
        <?php if(aIfModuleControllerAction("ids_flow","field-connection","index")): ?>
            <?= Html::a(Yii::t('app', 'Create Ids Flow Field Connection'),
                ['create'],
                ['class' => 'btn btn-success']) ?>
        <?php endif; ?>


        <?php if(aIfModuleControllerAction("ids_flow","flow","view")): ?>
            <?= Html::a(Yii::t('app', 'Create Ids Flow Field Connection'),
                ['/ids_flow/field-connection/create', 'flow_id' => aGet('id')],
                ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ids_flow_id',
            'parentIdsTemplateField.title',
            'childIdsTemplateField.title',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, IdsFlowFieldConnection $model, $key, $index, $column) {
                    return Url::toRoute(["/ids_flow/field-connection/".$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
