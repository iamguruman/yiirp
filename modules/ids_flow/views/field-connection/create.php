<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\ids_flow\models\IdsFlowFieldConnection $model */

$this->title = Yii::t('app', 'Create Ids Flow Field Connection');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ids Flow Field Connections'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ids-flow-field-connection-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
