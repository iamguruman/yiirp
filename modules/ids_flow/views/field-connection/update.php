<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\ids_flow\models\IdsFlowFieldConnection $model */

$this->title = Yii::t('app', 'Update Ids Flow Field Connection: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ids Flow Field Connections'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ids-flow-field-connection-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
