<?php

use app\modules\ids_template\models\TemplateField;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\ids_flow\models\IdsFlowFieldConnection $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ids-flow-field-connection-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ids_flow_id')->textInput() ?>

    <br>

    <?php if($model->idsFlow && $model->idsFlow->parent_ids_template_name_id): ?>
        <div class="form-group field-templatefield-title">
            <label class="control-label" for="templatefield-title">Parent Template</label>
            <div><?= $model->idsFlow->parentIdsTemplateName->name ?></div>
            <div class="help-block"></div>
        </div>

        <br>
    <?php endif; ?>

    <?= $form->field($model, 'parent_ids_template_field_id')->widget(Select2::className(), [
        'data' => TemplateField::find()
            ->select([TemplateField::tableName().'.title', TemplateField::tableName().'.id'])
            ->indexBy(TemplateField::tableName().'.id')
            ->andWhere([TemplateField::tableName().'.template_name_id' => $model->idsFlow->parent_ids_template_name_id])
            ->orderBy(['sort' => SORT_ASC])
            ->column(),
        'options' => [
            'placeholder' => "Select Field from parent template",
            //'onchange' => "changeTemplate();"
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <br>

    <?php if($model->idsFlow && $model->idsFlow->childIdsTemplateName): ?>
        <div class="form-group field-templatefield-title">
            <label class="control-label" for="templatefield-title">Child Template</label>
            <div><?= $model->idsFlow->childIdsTemplateName->name ?></div>
            <div class="help-block"></div>
        </div>

        <br>
    <?php endif; ?>

    <?= $form->field($model, 'child_ids_template_field_id')->widget(Select2::className(), [
        'data' => TemplateField::find()
            ->select([TemplateField::tableName().'.title', TemplateField::tableName().'.id'])
            ->indexBy(TemplateField::tableName().'.id')
            ->andWhere([TemplateField::tableName().'.template_name_id' => $model->idsFlow->child_ids_template_name_id])
            ->orderBy(['sort' => SORT_ASC])
                ->column(),
        'options' => [
            'placeholder' => "Select Field from parent template",
            //'onchange' => "changeTemplate();"
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <br>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
