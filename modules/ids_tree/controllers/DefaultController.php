<?php

namespace app\modules\ids_tree\controllers;

use app\modules\ids\models\Ids;
use app\modules\ids_template\models\IdsTemplateUserAccess;
use app\modules\ids_tree\models\IdsTree;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class DefaultController extends Controller
{

    public function actionIndex(){
        return $this->render("index");
    }

    //VasiliiBaukin:
    public function actionAjaxIndex()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        //todo
        $idsTreeItems = IdsTree::find()->asArray()->all();
        $idsItems = Ids::find()->asArray()->indexBy('id')->all();
        $ret = [];
        foreach($idsTreeItems as $idsTreeItem) {
            $ret[] = [
                'id' => $idsTreeItem['child_ids_id'],
                'parent' => $idsTreeItem['parent_ids_id'],
                'text' => $idsItems[$idsTreeItem['child_ids_id']]['name'],
            ];
        }
        //todo как-то правильно получить корень
        //проверить, все ли parent объекты присутствуют в выдаче

        foreach (Ids::find()
                    ->joinWith('templateName.usersAccess')
                    ->andWhere([IdsTemplateUserAccess::tableName().'.sys_user_id' => aUserMyId()])
                    ->asArray()
                    ->all() as $parent){

            $ret[] = [
                'id' => "{$parent['id']}",
                'parent' => '#',
                'text' => "{$parent['name']}",
            ];

        }


        return $ret;
    }

}