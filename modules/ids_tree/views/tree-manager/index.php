<?php

use app\modules\ids\models\Ids;
use app\modules\ids_flow\models\IdsFlow;
use app\modules\ids_tree\models\IdsTree;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this */
/** @var app\modules\ids_tree\models\IdsTreeSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

if(aIfModuleControllerAction("ids_tree", "tree-manager", "index")){
    $this->title = Yii::t('app', 'Tree manager');
    $this->params['breadcrumbs'][] = ['label' => 'Ids', 'url' => '/ids'];

    $this->params['breadcrumbs'][] = $this->title;
}

if(empty($id_hide)){ $id_hide = null; }
if(empty($parent_hide)){ $parent_hide = null; }
if(empty($child_hide)){ $child_hide = null; }

?>
<div class="ids-tree-index">

    <?php if(aIfModuleControllerAction("ids_tree", "tree-manager", "index")): ?>
        <h1><?= Html::encode($this->title) ?></h1>
    <?php endif; ?>

    <p>

        <?php if(aIfModuleControllerAction("ids", "tree-manager", "index")): ?>
            <?= Html::a(Yii::t('app', 'Create Ids Tree'),
                ['/ids_tree/tree-manager/create'],
                ['class' => 'btn btn-success']) ?>
        <?php endif; ?>

        <?php if(aIfModuleControllerAction("ids", "view", "index")): ?>
            <?php if($parent_hide): ?>

                <?= Html::a(Yii::t('app', 'Create Ids Tree'),
                    ['/ids_tree/tree-manager/create', 'parent_id' =>aGet('id')],
                    ['class' => 'btn btn-success']) ?>
                <img src="" >
                <?php if($idModel = Ids::findOne(aGet('id'))): ?>
                    <?php foreach (IdsFlow::findAll(['parent_ids_template_name_id' => $idModel->template_name_id]) as $idsFlow): ?>
                        <?php
                        $xTitle = [];
                        if(file_exists(Yii::getAlias("@app")."/web/tplicons/{$idsFlow->child_ids_template_name_id}.png")){
                            $xTitle [] = "<img src='/tplicons/{$idsFlow->child_ids_template_name_id}.png' style='max-height: 20px;'>";
                        }
                        $xTitle [] = $idsFlow->childIdsTemplateName->name;
                        ?>
                        <?= Html::a(implode(' ', $xTitle),
                            ['/ids/create',
                                'template_id' => $idsFlow->childIdsTemplateName->id,
                                'parent_id' => aGet('id'),
                                'flow_id' => $idsFlow->id
                            ],
                            ['class' => 'btn btn-success']) ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>

        <?php if(aIfModuleControllerAction("ids", "view", "index")): ?>
            <?php if($child_hide): ?>

                <?= Html::a(Yii::t('app', 'Create Ids Tree'),
                    ['/ids_tree/tree-manager/create', 'child_id' =>aGet('id')],
                    ['class' => 'btn btn-success']) ?>


            <?php endif; ?>
        <?php endif; ?>

    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'created_at',

            !$id_hide ? [
                'attribute' => 'id',
            ] : ['visible' => false],

            !$parent_hide ? [
                'attribute' => 'parent_ids_id',
            ] : ['visible' => false],

            !$parent_hide ? [
                'label' => 'Icon',
                'format' => 'raw',
                'value' => function(IdsTree $model){
                    $icon = "";

                    if($model->parentId && $model->parentId->template_name_id){

                        $icon = Yii::getAlias("@app/web/tplicons/{$model->parentId->template_name_id}.png");
                        if(file_exists($icon)){
                            $icon = Html::a("<img src='/tplicons/{$model->parentId->template_name_id}.png' style='max-height: 30px;'>",
                                ['/ids/view', 'id' =>$model->parentId->id],
                                ['data-pjax' => 0]);
                        }

                    }

                    return $icon;
                }
            ] : ['visible' => false],

            !$parent_hide ? [
                'format' => 'raw',
                'attribute' => 'parentIds.name',
                'value' => function(IdsTree $model){
                    if($model->parentId)
                        return $model->parentId->name;
                }
            ] : ['visible' => false],

            !$child_hide ? [
                'attribute' => 'child_ids_id',
            ] : ['visible' => false],

            !$child_hide ? [
                'label' => 'Icon',
                'format' => 'raw',
                'value' => function(IdsTree $model){

                    $icon = "";

                    if($model->childId && $model->childId->template_name_id){

                        $icon = Yii::getAlias("@app/web/tplicons/{$model->childId->template_name_id}.png");
                        if(file_exists($icon)){
                            $icon = Html::a("<img src='/tplicons/{$model->childId->template_name_id}.png' style='max-height: 30px;'>",
                                ['/ids/view', 'id' =>$model->childId->id],
                                ['data-pjax' => 0]);
                        }

                    }

                    return $icon;
                }
            ] : ['visible' => false],

            !$child_hide ? [
                'format' => 'raw',
                'attribute' => 'childIds.name',
                'value' => function(IdsTree $model){
                    if($model->childId)
                        return $model->childId->name;
                }
            ] : ['visible' => false],

            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, IdsTree $model, $key, $index, $column) {
                    return Url::toRoute(['/ids_tree/tree-manager/'.$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
