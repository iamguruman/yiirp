<?php

use app\modules\ids\models\Ids;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\ids_tree\models\IdsTree $model */
/** @var Ids $parentId */
/** @var Ids $childId */

$this->title = Yii::t('app', 'Create Ids Tree');

$this->params['breadcrumbs'][] = ['label' => 'Ids', 'url' => '/ids'];

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tree manager'), 'url' => ['index']];

if($parentId){
    $this->params['breadcrumbs'][] = ['label' => $parentId->name, 'url' => ['/ids/view', 'id' => $parentId->id]];
}

if($childId){
    $this->params['breadcrumbs'][] = ['label' => $childId->name, 'url' => ['/ids/view', 'id' => $childId->id]];
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ids-tree-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
