<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\modules\ids_tree\models\IdsTree $model */

$this->title = "Link #{$model->id}";
$this->params['breadcrumbs'][] = ['label' => 'Ids', 'url' => '/ids'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tree manager'), 'url' => ['index']];

if($model->parentId){
    $this->params['breadcrumbs'][] = ['label' => "ID{$model->parentId->id}: {$model->parentId->name}",
        'url' => ['/ids/view', 'id' => $model->parentId->id]];
}

if($model->childId){
    $this->params['breadcrumbs'][] = ['label' => "ID{$model->childId->id}: {$model->childId->name}",
        'url' => ['/ids/view', 'id' => $model->childId->id]];
}

$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ids-tree-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'parent_ids_id',
            'parentId.name',
            'child_ids_id',
            'childId.name',
        ],
    ]) ?>

    <?= \yii\bootstrap5\Tabs::widget(['items' => [
        [
            'label' => 'ID',
            'active' => false,
            'content' => "<br>".DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'created_by',
                        'created_at',
                        'updated_by',
                        'updated_at',
                        'markdel_by',
                        'markdel_at',
                    ],
                ])
        ],

        /*$model->parentId ? [
            'label' => 'Parent',
            'active' => false,
            'content' => "<br>".$this->render("@app/modules/ids/views/view/view.php", ['model' => $model->parentId]),
        ] : ['visible' => false],

        $model->childId ? [
            'label' => 'Child',
            'active' => false,
            'content' => "<br>".$this->render("@app/modules/ids/views/view/view.php", ['model' => $model->childId]),
        ] : ['visible' => false],*/

    ]]) ?>

</div>
