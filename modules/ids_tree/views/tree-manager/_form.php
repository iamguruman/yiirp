<?php

use app\modules\ids\models\Ids;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\ids_tree\models\IdsTree $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ids-tree-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php /*= $form->field($model, 'parent_ids_id')->widget(Select2::className(), [
        'data' => Ids::find()->select(['name', 'id'])->indexBy('id')->column(),
        'options' => ['placeholder' => 'Select a template ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) */?>

    <?= $form->field($model, 'parent_ids_id')->textInput() ?>

    <br>

    <?php /*= $form->field($model, 'child_ids_id')->widget(Select2::className(), [
        'data' => Ids::find()->select(['name', 'id'])->indexBy('id')->column(),
        'options' => ['placeholder' => 'Select a template ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) */?>

    <?= $form->field($model, 'child_ids_id')->textInput() ?>

    <br>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
