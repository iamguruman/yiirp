<?php

use app\modules\ids\models\Ids;
use app\modules\ids_flow\models\IdsFlow;
use app\modules\ids_tree\models\IdsTree;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this */
/** @var app\modules\ids_tree\models\IdsTreeSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

if(empty($type)){
    $type = "";
}

if(aIfModuleControllerAction("ids_tree", "tree-manager", "index")){
    $this->title = Yii::t('app', 'Tree manager');
    $this->params['breadcrumbs'][] = ['label' => 'Ids', 'url' => '/ids'];

    $this->params['breadcrumbs'][] = $this->title;
}

if(empty($id_hide)){ $id_hide = null; }
if(empty($parent_hide)){ $parent_hide = null; }
if(empty($child_hide)){ $child_hide = null; }

?>
<div class="ids-tree-index">

    <?php if(aIfModuleControllerAction("ids_tree", "tree-manager", "index")): ?>
        <h1><?= Html::encode($this->title) ?></h1>
    <?php endif; ?>

    <p>

        <?php if(aIfModuleControllerAction("ids", "tree-manager", "index")): ?>
            <?= Html::a(Yii::t('app', 'Create Ids Tree'),
                ['/ids_tree/tree-manager/create'],
                ['class' => 'btn btn-success']) ?>
        <?php endif; ?>

        <?php if(aIfModuleControllerAction("ids", "view", "index")): ?>
            <?php if($parent_hide): ?>

                <?= Html::a(Yii::t('app', 'Create Ids Tree'),
                    ['/ids_tree/tree-manager/create', 'parent_id' =>aGet('id')],
                    ['class' => 'btn btn-success']) ?>
                <img src="" >
                <?php if($idModel = Ids::findOne(aGet('id'))): ?>
                    <?php
                        $idsFlows = IdsFlow::find()
                            ->andWhere(['parent_ids_template_name_id' => $idModel->template_name_id])
                            ->all();
                    ?>
                    <?php foreach ($idsFlows as $idsFlow): ?>
                        <?php
                        $xTitle = [];
                        if(file_exists(Yii::getAlias("@app")."/web/tplicons/{$idsFlow->child_ids_template_name_id}.png")){
                            $xTitle [] = "<img src='/tplicons/{$idsFlow->child_ids_template_name_id}.png' style='max-height: 20px;'>";
                        }
                        $xTitle [] = $idsFlow->childIdsTemplateName->name;
                        ?>
                        <?= Html::a(implode(' ', $xTitle),
                            ['/ids/create',
                                'template_id' => $idsFlow->childIdsTemplateName->id,
                                'parent_id' => aGet('id'),
                                'flow_id' => $idsFlow->id
                            ],
                            ['class' => 'btn btn-success']) ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>

        <?php if(aIfModuleControllerAction("ids", "view", "index")): ?>
            <?php if($child_hide): ?>

                <?= Html::a(Yii::t('app', 'Create Ids Tree'),
                    ['/ids_tree/tree-manager/create', 'child_id' =>aGet('id')],
                    ['class' => 'btn btn-success']) ?>


            <?php endif; ?>
        <?php endif; ?>

    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'created_at',

            !$id_hide ? [
                'label' => 'Link ID',
                'attribute' => 'id',
            ] : ['visible' => false],

            [
                'format' => 'raw',
                'label' => "ID",
                'value' => function(IdsTree $model) use ($parent_hide, $child_hide){

                    if(!$parent_hide && $model->parentId ){
                        return $model->parentId->id;
                    }

                    if(!$child_hide && $model->childId){
                        return $model->childId->id;
                    }

                }
            ],


            [
                'format' => 'raw',
                'label' => "Icon",
                'value' => function(IdsTree $model) use ($parent_hide, $child_hide){

                    $icon = "";

                    if(!$parent_hide && $model->parentId ){
                        $icon = Yii::getAlias("@app/web/tplicons/{$model->parentId->template_name_id}.png");
                        if(file_exists($icon)){
                            $icon = Html::a("<img src='/tplicons/{$model->parentId->template_name_id}.png' style='max-height: 30px;'>",
                                ['/ids/view', 'id' =>$model->parentId->id],
                                ['data-pjax' => 0]);
                        }
                    }

                    if(!$child_hide && $model->childId && $model->childId->template_name_id){

                        $icon = Yii::getAlias("@app/web/tplicons/{$model->childId->template_name_id}.png");
                        if(file_exists($icon)){
                            $icon = Html::a("<img src='/tplicons/{$model->childId->template_name_id}.png' style='max-height: 30px;'>",
                                ['/ids/view', 'id' =>$model->childId->id],
                                ['data-pjax' => 0]);
                        }

                    }

                    return $icon;

                }
            ],

            [
                'format' => 'raw',
                'label' => "Name",
                'value' => function(IdsTree $model) use ($parent_hide, $child_hide){

                    if(!$parent_hide && $model->parentId ){
                        return $model->parentId->name;
                    }

                    if(!$child_hide && $model->childId){
                        return $model->childId->name;
                    }

                }
            ],

            [
                'format' => 'raw',
                'label' => "Status",
                'value' => function(IdsTree $model) use ($parent_hide, $child_hide){

                    $ret = [];

                    if(!$parent_hide && $model->parentId && $model->parentId->idsTemplateStatusName){

                        $ret [] = $model->parentId->idsTemplateStatusName->name;

                        if($model->parentId->idsTemplateStatusName->style){
                            $ret [] = "<div title='{$model->parentId->idsTemplateStatusName->name}' style='display: inline-block; height: 25px; width: 25px; border: 1px solid black;{$model->parentId->idsTemplateStatusName->style}'></div>";
                        }

                    }

                    if(!$child_hide && $model->childId && $model->childId->idsTemplateStatusName){

                        $ret [] = $model->childId->idsTemplateStatusName->name;

                        if($model->childId->idsTemplateStatusName->style){
                            $ret [] = "<div title='{$model->childId->idsTemplateStatusName->name}' style='display: inline-block; height: 25px; width: 25px; border: 1px solid black;{$model->childId->idsTemplateStatusName->style}'></div>";
                        }

                    }

                    return implode("&nbsp;", $ret);

                }
            ],

            [
                'format' => 'raw',
                'label' => "",
                'value' => function(IdsTree $model) use ($parent_hide, $child_hide){
                    $ret = [];

                    if(!$parent_hide){
                        $ret [] = Html::a("<img src='/media/icon_eye.png' height='25'>", ['/ids/view', 'id' => $model->parent_ids_id], ['data-pjax' => 0]);
                    }

                    if(!$child_hide){
                        $ret [] = Html::a("<img src='/media/icon_eye.png' height='25'>", ['/ids/view', 'id' => $model->child_ids_id], ['data-pjax' => 0]);
                    }

                    $ret [] = Html::a("<img src='/media/icon_cog.png' height='25'>", ['/ids_tree/tree-manager/view', 'id' => $model->id], ['data-pjax' => 0]);

                    return implode(" ", $ret);
                }
            ],

            aIfModuleControllerAction("ids_tree", "tree-manager", "index") ? [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, IdsTree $model, $key, $index, $column) {
                    return Url::toRoute(['/ids_tree/tree-manager/'.$action, 'id' => $model->id]);
                }
            ] : ['visible' => false],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
