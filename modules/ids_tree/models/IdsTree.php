<?php

namespace app\modules\ids_tree\models;

use app\modules\ids\models\Ids;
use Yii;

/**
 * This is the model class for table "ids_tree".
 *
 * @property int $id
 * @property string $created_at
 * @property int $created_by
 * @property int $update_by
 * @property string $updated_at
 * @property int $markdel_by
 * @property string $markdel_at
 *
 * @property int $parent_ids_id
 * @property-read Ids $parentId
 *
 * @property int $child_ids_id
 * @property-read Ids $childId
 */
class IdsTree extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ids_tree';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_ids_id', 'child_ids_id'], 'required'],
            [['created_at', 'updated_at', 'markdel_at'], 'safe'],
            [['created_by', 'update_by', 'markdel_by', 'parent_ids_id', 'child_ids_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'update_by' => Yii::t('app', 'Update By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'markdel_by' => Yii::t('app', 'Markdel By'),
            'markdel_at' => Yii::t('app', 'Markdel At'),
            'parent_ids_id' => Yii::t('app', 'Parent Ids ID'),
            'child_ids_id' => Yii::t('app', 'Child Ids ID'),
        ];
    }

    public function getParentId(){
        return $this->hasOne(Ids::className(), ['id' => 'parent_ids_id']);
    }

    public function getChildId(){
        return $this->hasOne(Ids::className(), ['id' => 'child_ids_id']);
    }

    /**
     * {@inheritdoc}
     * @return IdsTreeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IdsTreeQuery(get_called_class());
    }
}
