<?php

namespace app\modules\ids_tree\models;

/**
 * This is the ActiveQuery class for [[IdsTree]].
 *
 * @see IdsTree
 */
class IdsTreeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return IdsTree[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return IdsTree|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
