<?php

namespace app\modules\ids_tree\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\ids_tree\models\IdsTree;

/**
 * IdsTreeSearch represents the model behind the search form of `app\modules\ids_tree\models\IdsTree`.
 */
class IdsTreeSearch extends IdsTree
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'update_by', 'markdel_by', 'parent_ids_id', 'child_ids_id'], 'integer'],
            [['created_at', 'updated_at', 'markdel_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $params2 = [])
    {
        $query = IdsTree::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'update_by' => $this->update_by,
            'updated_at' => $this->updated_at,
            'markdel_by' => $this->markdel_by,
            'markdel_at' => $this->markdel_at,
            'parent_ids_id' => $this->parent_ids_id,
            'child_ids_id' => $this->child_ids_id,
        ]);

        if(!empty($params2['parent_ids_id'])){
            $query->andWhere(['parent_ids_id' => $params2['parent_ids_id']]);
        }

        if(!empty($params2['child_ids_id'])){
            $query->andWhere(['child_ids_id' => $params2['child_ids_id']]);
        }

        return $dataProvider;
    }
}
