<?php
/** @var $this \yii\web\View */

use app\assets\Select2Asset;

$this->registerJsFile('/vendor/fancytree/jquery.fancytree-all-deps.js', ['depends' => \yii\web\JqueryAsset::class]);
$this->registerJsFile('//cdn.jsdelivr.net/npm/jquery-contextmenu@2.9.0/dist/jquery.contextMenu.min.js', ['depends' => \yii\web\JqueryAsset::class]);
$this->registerJsFile('https://code.jquery.com/ui/1.13.2/jquery-ui.js', ['depends' => \yii\web\JqueryAsset::class]);
$this->registerJsFile('https://romb.github.io/jquery-dialogextend/build/jquery.dialogextend.js', ['depends' => \yii\web\JqueryAsset::class]);

$this->registerCssFile('/vendor/fancytree/skin-xp/ui.fancytree.css', ['depends' => \yii\web\JqueryAsset::class]);
$this->registerCssFile('//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css', ['depends' => \yii\web\JqueryAsset::class]);
$this->registerCssFile('//cdn.jsdelivr.net/npm/jquery-contextmenu@2.9.0/dist/jquery.contextMenu.min.css', ['depends' => \yii\web\JqueryAsset::class]);
$this->registerJsFile('/js/dialogHelper.js', ['depends' => \yii\web\JqueryAsset::class, 'defer' => true]);
$this->registerJsFile('/js/fancytree.js', ['depends' => \yii\web\JqueryAsset::class, 'defer' => true]);
$this->registerJsFile('/js/ids_tree_fancy.js', ['depends' => Select2Asset::class, 'defer' => true]);

//exit();
Select2Asset::register($this);


//
//заполнить дерево
//выводить карточки в ряд - 3 карты 100% высота

?>
<style>
    span.fancytree-title { white-space: normal; }
</style>

<div>

    <input id="search" name="search" onchange="changeName()" style="width: 100%" value="<?= aGet('idsNamePart') ?>" placeholder="Наименование Ids">

    <script>
        //todo move to filter helper
        function changeName(){
            let search = document.getElementById("search").value;
            fancyTreeHelper.replaceParam('idsNamePart', search);
            fancyTreeHelper.treeReload();
        }
    </script>

</div>

<div class="filters-container">
    <div id="templateId-filter-container"></div>
    <div id="statusId-filter-container"></div>
</div>

<div class="fancytree-container" style="display: flex">

    <div>
    <div id="fancytree" style="min-height:300px">
    </div>
        <a id="tree-load-more-link" href="#">Догрузить узлы</a>
    </div>



</div>