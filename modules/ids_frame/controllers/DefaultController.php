<?php

namespace app\modules\ids_frame\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{

    public function actionIndex(){

        return $this->renderPartial("index");

    }

}