<?php

/** @var yii\web\View $this */
/** @var TemplateName $model */

use app\modules\ids_template\models\TemplateName;
use yii\helpers\Html;



$this->registerJsFile('/js/dialogHelper.js', ['depends' => \yii\web\JqueryAsset::class, 'defer' => true]);


$this->registerCssFile('//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css', ['depends' => \yii\web\JqueryAsset::class]);
$this->registerJsFile('https://code.jquery.com/ui/1.13.2/jquery-ui.js', ['depends' => \yii\web\JqueryAsset::class]);

$this->registerJsFile('https://romb.github.io/jquery-dialogextend/build/jquery.dialogextend.js', ['depends' => \yii\web\JqueryAsset::class]);


$width = aGet('width') ?? 400;

$count = count($model->statusNames);

?>
<style>
    body, form {
        margin: 0;
        padding: 0;
    }

    table {
        height: 100vh;
        width: 100%;
        min-width: <?= $width * $count?>px;
    }

    td {
        min-width: <?= $width ?>px;
    }

    .table_name {
        font-size: 25px;
        font-weight: bold;
    }

    .td_name {
        font-size: 18px;
        font-weight: bold;
        text-align: center;
    }

    .search_input {
        width: 100%;
    }
</style>
<div class="ids-kanban">

   <table border="1" cellpadding="0" cellspacing="0">
       <!-- TEMPLATE HEADER -->
       <tr style="height: 40px;">
           <td class="table_name" width="<?= ($width * $count) ?>" colspan="<?= $count ?>">
               <div><?= $model->name ?>

                   <?php if(aGet('view') != 'tree' and !empty(aGet('view'))): ?>
                       <?= Html::a("<img src='/media/icon_tree.png' height='20'>",
                           ['/ids_kanban/',
                               'template_id' => aGet('template_id')]
                       ) ?>
                   <?php endif; ?>

                   <?php if(aGet('view') != 'table'): ?>
                       <?= Html::a("<img src='/media/icon_greed.png' height='20'>",
                           ['/ids_kanban/',
                               'template_id' => aGet('template_id'),
                               'view' => 'table']
                       ) ?>
                   <?php endif; ?>

                   <?= $model->userCanEdit ?
                       Html::a("<img src='/media/icon_cog.png' height='20'>", ['/ids_template/name/view', 'id' => $model->id])
                       : null ?>

               </div>
           </td>
       </tr>

       <!-- STATUS HEADER -->
       <tr style="height: 40px;">
           <?php foreach ($model->statusNames as $statusName): ?>
               <td class="td_name" style="width: text-align: center; <?= $statusName->style ?>" width="<?= $width ?>">
                   <?= $statusName->name ?> (<?= $statusName->id ?>)
               </td>
           <?php endforeach; ?>
       </tr>

       <!-- SEARCH FORMS -->
       <tr style="height: 18px;">
           <?php foreach ($model->statusNames as $statusName): ?>
               <td>
                   <form method="get"
                         action="/ids_kanban/iframe"
                         target="status_<?= $statusName->id ?>"
                   >
                       <input type="hidden" name="statusId" value="<?= $statusName->id ?>">
                       <input type="hidden" name="view" value="<?= aGet('view') ?>">
                       <input class="search_input" name="search">
                       <input type="submit" style="display: none">
                   </form>
               </td>
           <?php endforeach; ?>
       </tr>

       <!-- IFRAMES -->
       <tr>
           <?php foreach ($model->statusNames as $statusName): ?>
               <td style="width: <?= $width?>px;">
                   <iframe name="status_<?= $statusName->id?>"
                           width="100%"
                           height="100%"
                           src="/ids_kanban/iframe?limit=null&statusId=<?= $statusName->id ?>&view=<?= aGet('view')?>"
                   >
                   </iframe>
               </td>
           <?php endforeach; ?>
       </tr>

   </table>

</div>