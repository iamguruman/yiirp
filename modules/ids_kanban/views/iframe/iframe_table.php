<?php

use app\modules\ids\models\Ids;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/** @var $dataProvider \yii\data\ActiveDataProvider */
/** @var $searchModel \app\modules\ids\models\IdsSearch */

?>
<style>
    #yii-debug-toolbar{
        display: none !important;
    }
</style>

<?php Pjax::begin(); ?>
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [

        [
            'label' => '',
            'format' => 'raw',
            'value' => function(Ids $model){

                $ret = [];

                $ret [] = "N{$model->id}";

                $ret [] = Html::a($model->name,
                    ['/ids/view', 'id' => $model->id],
                    ['data-pjax' => 0, 'target' => '_blank',
                        'style' => $model->markdel_by ? "text-decoration:line-through;" : null]);

                return implode(" ", $ret);
            }
        ],

    ]
]); ?>

<?php Pjax::end(); ?>