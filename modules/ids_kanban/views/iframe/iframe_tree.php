<?php

use app\modules\ids\models\Ids;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var $dataProvider \yii\data\ActiveDataProvider */
/** @var $searchModel \app\modules\ids\models\IdsSearch */

$this->registerJsFile('/vendor/fancytree/jquery.fancytree-all-deps.js', ['depends' => \yii\web\JqueryAsset::class]);
$this->registerJsFile('//cdn.jsdelivr.net/npm/jquery-contextmenu@2.9.0/dist/jquery.contextMenu.min.js', ['depends' => \yii\web\JqueryAsset::class]);
$this->registerJsFile('https://code.jquery.com/ui/1.13.2/jquery-ui.js', ['depends' => \yii\web\JqueryAsset::class]);
$this->registerJsFile('https://romb.github.io/jquery-dialogextend/build/jquery.dialogextend.js', ['depends' => \yii\web\JqueryAsset::class]);

$this->registerCssFile('/vendor/fancytree/skin-xp/ui.fancytree.css', ['depends' => \yii\web\JqueryAsset::class]);
$this->registerCssFile('//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css', ['depends' => \yii\web\JqueryAsset::class]);
$this->registerCssFile('//cdn.jsdelivr.net/npm/jquery-contextmenu@2.9.0/dist/jquery.contextMenu.min.css', ['depends' => \yii\web\JqueryAsset::class]);
$this->registerJsFile('/js/dialogHelper.js', ['depends' => \yii\web\JqueryAsset::class, 'defer' => true]);
$this->registerJsFile('/js/fancytree.js', ['depends' => \yii\web\JqueryAsset::class, 'defer' => true]);


?>

<style>
    span.fancytree-title { white-space: normal; }
</style>

<div class="fancytree-container" style="display: flex">

    <div id="fancytree" style="min-height:300px">
    </div>

</div>

