<?php

namespace app\modules\ids_kanban\controllers;

use app\modules\ids\models\Ids;
use app\modules\ids_template\models\TemplateField;
use app\modules\ids_template\models\TemplateName;
use Yii;
use yii\base\DynamicModel;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\View;

class DefaultController extends Controller
{
    public function actionIndex($template_id){

        $model = TemplateName::findOne($template_id);
        $this->view->off(View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
        return $this->renderAjax("index", ['model' => $model]);

    }

}