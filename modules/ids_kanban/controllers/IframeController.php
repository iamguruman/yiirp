<?php

namespace app\modules\ids_kanban\controllers;

use app\modules\ids\models\IdsSearch;
use app\modules\ids_template\models\IdsTemplateStatusName;
use yii\web\Controller;
use yii\web\View;

class IframeController extends Controller
{

    public function actionIndex($statusId, $search = null, $limit = null){

        $model = IdsTemplateStatusName::findOne($statusId);

        $searchModel = new IdsSearch();
        $dataProvider = $searchModel->search($this->request->queryParams, [
            'user_can_list' => true,
            'ids_template_status_name_id' => $model->id,
            'search' => $search
        ]);

        if($limit == "null"){
            $dataProvider->setPagination(false);
        }

        $this->view->off(View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);

        return $this->renderAjax(aGet('view') == 'table' ? "iframe_table" : "iframe_tree", [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

}