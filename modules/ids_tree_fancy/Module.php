<?php

namespace app\modules\ids_tree_fancy;

/**
 * accesses module definition class
 *
 * используется https://github.com/ROMB/jquery-dialogextend
 *
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\ids_tree_fancy\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
