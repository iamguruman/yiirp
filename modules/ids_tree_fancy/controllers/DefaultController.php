<?php

namespace app\modules\ids_tree_fancy\controllers;

use app\components\TreeHelper;
use app\modules\ids_template\models\IdsTemplateStatusName;
use app\modules\ids_template\models\IdsTemplateUserAccess;
use app\modules\ids_template\models\TemplateName;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Options;
use yii\web\Controller;
use yii\web\Response;
use yii\web\View;

class DefaultController extends Controller
{

    public function actionIndex()
    {
        $this->view->off(View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
        return $this->renderAjax('fancy-tree');
    }


    //todo rename actions

    public function actionAjaxNodeList()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        //nodeId - сам узел
        //rootId - дочерние элементы узла

        $nodeId = $_GET['nodeId'] ?? null;
        $rootId = $_GET['rootId'] ?? null;
        $idsNamePart = $_GET['idsNamePart'] ?? null;
        $statusId = $_GET['statusId'] ?? null;
        $templateId = $_GET['templateId'] ?? null;
        $limit = $_GET['limit'] ?? 20;
        if($limit === 'null') {
            $limit = null;
        }
        $offset = $_GET['offset'] ?? 0;

        $ret = TreeHelper::getListFiltered($nodeId, $rootId, $templateId, $idsNamePart, $statusId, $offset, $limit);

        return $ret;
    }

    public function actionAjaxTreeLevel()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $nodeId = $_GET['rootId'] ?? null;
        $ret = TreeHelper::getTreeLevel($nodeId);
        return $ret;
    }

    public function actionLayoutTopOnly()
    {
        $this->view->off(View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
        $output =  $this->renderContent('');

        $dom = new Dom();

        $dom->setOptions(
            (new Options())
                ->setCleanupInput(false)
//                                ->setPreserveLineBreaks(true)
        );
        $dom->loadStr($output);

        $dom->find('footer')[0]->delete();
        $dom->find('#main')[0]->delete();
//        $dom->find('.fa-search')[0]->delete();
        $dom->find('form')[0]->delete();

        /** @var Dom\Node\HtmlNode $a */
        foreach($dom->find('a') as $a) {
            $a->setAttribute('target', '_parent');
        }


        /** @var Dom\Node\HtmlNode $form */
        /*
        foreach($dom->find('form') as $form) {
            $form->setAttribute('target', '_parent');
        }
        */

        return (string)$dom;
    }

    public function actionAjaxStatusList()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $templateId = aGet('templateId');
        if(!$templateId) {
            throw new \Exception("templateId is required");
        }

        $ret = IdsTemplateStatusName::find()
            ->select(['name', 'id'])
            ->andWhere(['ids_template_name_id' => $templateId])
            ->all();
        return [
                'results' => array_map(function($value) { return ['id' => $value['id'], 'text' => $value['name']]; }, $ret)
            ];
    }

    public function actionAjaxTemplateList()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $ret = TemplateName::find()
            ->select(['ids_template_name.name', 'ids_template_name.id'])
            ->joinWith('usersAccess')
            ->andWhere([IdsTemplateUserAccess::tableName().'.sys_user_id' => aUserMyId()])
                ->asArray()
            ->all();
        return [
            'results' => array_map(function($value) { return ['id' => $value['id'], 'text' => $value['name']]; }, $ret)
        ];
    }

}