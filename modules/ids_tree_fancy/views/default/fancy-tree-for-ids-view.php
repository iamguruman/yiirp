<?php
//todo refactor - mb include fancy tree and apply mods ?

/** @var $this \yii\web\View */

use app\modules\ids_template\models\IdsTemplateUserAccess;
use app\modules\ids_template\models\TemplateName;
use kartik\select2\Select2;

$this->registerJsFile('/vendor/fancytree/jquery.fancytree-all-deps.js', ['depends' => \yii\web\JqueryAsset::class]);
$this->registerJsFile('//cdn.jsdelivr.net/npm/jquery-contextmenu@2.9.0/dist/jquery.contextMenu.min.js', ['depends' => \yii\web\JqueryAsset::class]);
$this->registerJsFile('https://code.jquery.com/ui/1.13.2/jquery-ui.js', ['depends' => \yii\web\JqueryAsset::class]);
$this->registerJsFile('https://romb.github.io/jquery-dialogextend/build/jquery.dialogextend.js', ['depends' => \yii\web\JqueryAsset::class]);

$this->registerCssFile('/vendor/fancytree/skin-xp/ui.fancytree.css', ['depends' => \yii\web\JqueryAsset::class]);
$this->registerCssFile('//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css', ['depends' => \yii\web\JqueryAsset::class]);
$this->registerCssFile('//cdn.jsdelivr.net/npm/jquery-contextmenu@2.9.0/dist/jquery.contextMenu.min.css', ['depends' => \yii\web\JqueryAsset::class]);
$this->registerJsFile('/js/fancytree.js', ['depends' => \yii\web\JqueryAsset::class, 'defer' => true]);
$this->registerJsFile('/js/dialogHelper.js', ['depends' => \yii\web\JqueryAsset::class, 'defer' => true]);

//
//заполнить дерево
//выводить карточки в ряд - 3 карты 100% высота

?>

<style>
    span.fancytree-title { white-space: normal; }
</style>

<div class="container" style="display: flex">

    <div id="fancytree" style="min-height:300px">

    </div>
    <div style="display: none">
        <form>
            <input id="ids-id">
            <button id="ids-id-button" type="submit">Go</button>
        </form>
    </div>

</div>