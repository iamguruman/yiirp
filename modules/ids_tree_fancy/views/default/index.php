<?php

use app\modules\ids_template\models\IdsTemplateUserAccess;
use app\modules\ids_template\models\TemplateName;

\app\assets\JsTreeAsset::register($this);
$this->registerJsFile('/js/ids_default.js', ['depends' => [\app\assets\JsTreeAsset::class], 'defer' => true]);

?>
<p id="tree">

</p>
