<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\fileserver\components\FileServerGetLink;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\vatssms\models\PbxSmsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$module = app\modules\vats\Module::moduleId;
$controller = "sms";
$action = "index";

if(aIfModuleControllerAction("vats", "sms", "inbox")){
    $this->title = "Входящие СМС сообщения";
}

if(aIfModuleControllerAction("vats", "sms", "send")){
    $this->title = "Отправленные СМС сообщения";
}

$this->params['breadcrumbs'][] = \app\modules\vats\Module::getBreadcrumbs();
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="mvatssms-index">

    <?= aHtmlHeader($this->title, $module, $controller, 'inbox') ?>
    <?= aHtmlHeader($this->title, $module, $controller, 'send') ?>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute' => 'id',
                'headerOptions' => ['style' => 'width:100px;'],
                'format' => 'raw', 'value' => function($model) { return aGridVIewColumnId($model); }],
            
            'datetime',

            aIfModuleControllerAction("vats", "sms", "inbox") ? ['visible' => false] : [
                'attribute' => 'to'
            ],

            aIfModuleControllerAction("vats", "sms", "send") ? ['visible' => false] : [
                'attribute' => 'from'
            ],

            'message',

            ['class' => 'yii\grid\ActionColumn', 'template' => '', 'buttons' => [
                /*'view' => function($url, \app\modules\vatssms\models\PbxSms $model, $key){
                    return aGridViewActionColumnViewButton($model, $model->getUrlView());
                },*/


            ]],

        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
