<?php

use app\modules\pbx\models\PbxCalls;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\pbx\models\PbxCallsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$module = app\modules\vats\Module::moduleId;
$controller = "voicemail";
$action = "index";

if(aIfModuleControllerAction($module, $controller, $action)){

    $this->title = "Голосовая почта";

    $this->params['breadcrumbs'][] = \app\modules\vats\Module::getBreadcrumbs();
    $this->params['breadcrumbs'][] = $this->title;
}

?>
<div class="mpbx-index">

    <?= aHtmlHeader($this->title, $module, $controller, $action) ?>

    <p>Раздел в разработке</p>
</div>
