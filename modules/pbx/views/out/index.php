<?php

use app\modules\pbx\models\PbxCalls;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\pbx\models\PbxCallsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$module = app\modules\vats\Module::moduleId;
$controller = "out";
$action = "index";

if(aIfModuleControllerAction($module, $controller, $action)){

    $this->title = "Исходящие звонки отвеченные";

    $this->params['breadcrumbs'][] = \app\modules\vats\Module::getBreadcrumbs();
    $this->params['breadcrumbs'][] = $this->title;
}

?>
<div class="mpbx-index">

    <?= aHtmlHeader($this->title, $module, $controller, $action) ?>

    <p>
        <?/*= aIfModuleControllerAction($module, $controller, $action) ?
            Html::a('Добавить', ["/{$module}/{$controller}/create"], ['class' => 'btn btn-success'])
            : null  */?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute' => 'id',
                'headerOptions' => ['style' => 'width:100px;'],
                'format' => 'raw', 'value' => function($model) { return aGridVIewColumnId($model); }],

            //'created_at',
            //'markdel_at',
            //'markdel_by',

            [
                'attribute' => 'datetime_start',
                'headerOptions' => ['style' => 'width:200px;'],
                'label' => 'Дата',
                'value' => function(PbxCalls $model){

                    $ret = null;

                    if($model->datetime_start){
                        $ret = date('d-m-Y', strtotime($model->datetime_start));
                    }

                    if($ret == date('d-m-Y')){
                        $ret = "сегодня";
                    }

                    if($ret == date('d-m-Y', strtotime("-1 day"))){
                        $ret = "вчера";
                    }

                    return $ret;
                }
            ],

            [
                'attribute' => 'datetime_start',
                'label' => 'Время',
                'value' => function(PbxCalls $model){
                    if($model->datetime_start){
                        return date('H:i:s', strtotime($model->datetime_start));
                    }
                }
            ],
            //'call_id',
            //'direction',
            [
                'label' => '',
                'header' => "<i class='fas fa-user-clock' title='Ожидание ответа'></i>",
                'value' => function(PbxCalls $model){

                    if($model->duration_waiting > 0 && $model->duration_waiting <= 60){
                        return $model->duration_waiting . "c";
                    }
                    if($model->duration_waiting > 60){
                        return round($model->duration_waiting / 60, 1) . "м";
                    }

                    return '';
                },
                'format' => 'raw',
            ],

            [
                'label' => '',
                'header' => "<i class='fas fa-clock' title='Продолжительность разговора'></i>",
                'value' => function(PbxCalls $model){

                    if($model->duration > 0 && $model->duration <= 60){
                        return $model->duration . "c";
                    }
                    if($model->duration > 60){
                        return round($model->duration / 60, 0) . "м";
                    }

                    return '';

                },
                'format' => 'raw',
            ],

            //'answered',
            //'billsec',
            //'duration',

            [
                'label' => 'Откуда звонок',
                'attribute' => 'office_tel',
                'format' => 'raw',
                'value' => function(PbxCalls $model){
                    return $model->office_tel;
                }
            ],

            [
                'label' => 'Внутренние номера',
                'attribute' => 'internalNumberSearch',
                'value' => function(PbxCalls $model){
                    return $model->internalNumberHistory;
                }
            ],

            [
                'label' => 'Куда звонили',
                'attribute' => 'external_number',
                'format' => 'raw',
                'value' => function(PbxCalls $model){
                    return $model->external_number;
                }
            ],

            //'file',

            //'internal_number',

            [
                'label' => '',
                'format' => 'raw',
                'value' => function(PbxCalls $model){

                    $ret = [];

                    $ret [] = aHtmlButtonPostAjax("Позвонить",
                        ['/calls/vats-call-to', 'callto' => $model->external_number],
                    );

                    return implode("", $ret);
                }
            ],

            [
                'label' => '',
                'format' => 'raw',
                'value' => function(PbxCalls $model){
                    return "<audio controls src='https://integration.mcn.ru/api/public/api/external/6V8r6TS6RECRKbCIZ9IwU?call_id={$model->call_id}'></audio>";
                }
            ],

            //'datetime_end',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {files}', 'buttons' => [
                /*'view' => function($url, PbxCalls $model, $key){
                    return aGridViewActionColumnViewButton($model, $model->getUrlView());
                },*/

                'files' => function($url, PbxCalls $model, $key){

                    $count = count($model->uploads);

                    if($count == 1){

                        $upload = $model->uploads[0];
                        return Html::a("<i class='fas fa-paperclip'>{$count}</i>",
                            FileServerGetLink::http($upload->md5, $upload->ext),
                            ['data-pjax' => 0, 'target' => '_blank']
                        );

                    } elseif($count > 1){

                        return Html::a("<i class='fas fa-paperclip'>{$count}</i>",
                            ["/pbx/default/view", 'id' => $model->id, 'tab' => 'files'],
                            ['data-pjax' => 0]);

                    }

                }
            ]],        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
