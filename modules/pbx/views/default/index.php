<?php

use app\modules\pbx\models\PbxCalls;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\pbx\models\PbxCallsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$module = app\modules\pbx\Module::moduleId;
$controller = "default";
$action = "index";

if(aIfModuleControllerAction($module, $controller, $action)){

    $this->title = "Все звонки АТС";

    $this->params['breadcrumbs'][] = \app\modules\pbx\Module::getBreadcrumbs();
    $this->params['breadcrumbs'][] = $this->title;
}

?>
<div class="mpbx-index">

    <?= aHtmlHeader($this->title, $module, $controller, $action) ?>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute' => 'id',
                'headerOptions' => ['style' => 'width:100px;'],
                'format' => 'raw', 'value' => function($model) { return aGridVIewColumnId($model); }],

            //'created_at',
            //'markdel_at',
            //'markdel_by',


            //'call_id',
            //'direction',

            [
                'attribute' => 'datetime_start',
                'label' => 'Дата',
                'value' => function(PbxCalls $model){

                    $ret = null;

                    if($model->datetime_start){
                        $ret = date('d-m-Y', strtotime($model->datetime_start));
                    }

                    if($ret == date('d-m-Y')){
                        $ret = "сегодня";
                    }

                    if($ret == date('d-m-Y', strtotime("-1 day"))){
                        $ret = "вчера";
                    }

                    return $ret;
                }
            ],

            [
                'attribute' => 'datetime_start',
                'label' => 'Время',
                'value' => function(PbxCalls $model){
                    if($model->datetime_start){
                        return date('H:i:s', strtotime($model->datetime_start));
                    }
                }
            ],



            [
                'attribute' => 'direction',
                'label' => 'Напр',
                'value' => function(PbxCalls $model){
                    if($model->direction == 'in'){
                        return "вх";
                    } elseif ($model->direction == 'out'){
                        return "исх";
                    } else {
                        return "вн";
                    }
                }
            ],

            [
                'label' => '',
                'header' => "<i class='fas fa-user-clock' title='Ожидание ответа'></i>",
                'value' => function(PbxCalls $model){

                    if($model->duration_waiting > 0 && $model->duration_waiting <= 60){
                        return $model->duration_waiting . "c";
                    }
                    if($model->duration_waiting > 60){
                        return round($model->duration_waiting / 60, 1) . "м";
                    }

                    return '';
                },
                'format' => 'raw',
            ],

            [
                'label' => '',
                'header' => "<i class='fas fa-clock' title='Продолжительность разговора'></i>",
                'value' => function(PbxCalls $model){

                    if($model->duration > 0 && $model->duration <= 60){
                        return $model->duration . "c";
                    }
                    if($model->duration > 60){
                        return round($model->duration / 60, 0) . "м";
                    }

                    return '';

                },
                'format' => 'raw',
            ],

            //'answered',
            //'billsec',
            //'duration',
            [
                'label' => 'Номер АТС',
                'attribute' => 'office_tel',
                'format' => 'raw',
                'value' => function(PbxCalls $model){
                    return $model->office_tel;
                }
            ],

            [
                'label' => 'Внутренние номера',
                'attribute' => 'internalNumberSearch',
                'value' => function(PbxCalls $model){
                    return $model->internalNumberHistory;
                }
            ],

            //'file',

            [
                'label' => 'Внешний номер',
                'attribute' => 'external_number',
                'format' => 'raw',
                'value' => function(PbxCalls $model){
                    return $model->external_number;
                }
            ],

            [
                'label' => '',
                'format' => 'raw',
                'value' => function(PbxCalls $model){

                    $ret = [];

                    $ret [] = aHtmlButtonPostAjax("Позвонить",
                        ['/calls/vats-call-to', 'callto' => $model->external_number],
                    );

                    return implode("", $ret);
                }
            ],

            [
                'label' => '',
                'format' => 'raw',
                'value' => function(PbxCalls $model){
                    return "<audio controls src='https://integration.mcn.ru/api/public/api/external/6V8r6TS6RECRKbCIZ9IwU?call_id={$model->call_id}'></audio>";
                }
            ],

            //'internal_number',
            //'datetime_end',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}', 'buttons' => [
                /*'view' => function($url, \app\modules\pbx\models\PbxCalls $model, $key){
                    return aGridViewActionColumnViewButton($model, $model->getUrlView());
                },*/

                /*'files' => function($url, \app\modules\pbx\models\PbxCalls $model, $key){

                    $count = count($model->uploads);

                    if($count == 1){

                        $upload = $model->uploads[0];
                        return Html::a("<i class='fas fa-paperclip'>{$count}</i>",
                            FileServerGetLink::http($upload->md5, $upload->ext),
                            ['data-pjax' => 0, 'target' => '_blank']
                        );

                    } elseif($count > 1){

                        return Html::a("<i class='fas fa-paperclip'>{$count}</i>",
                            ["/pbx/default/view", 'id' => $model->id, 'tab' => 'files'],
                            ['data-pjax' => 0]);

                    }

                }*/
            ]],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
