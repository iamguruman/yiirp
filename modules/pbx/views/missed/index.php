<?php

use app\modules\pbx\models\PbxCalls;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\pbx\models\PbxCallsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$module = app\modules\vats\Module::moduleId;
$controller = "missed";
$action = "index";

if(aIfModuleControllerAction($module, $controller, $action)){

    $this->title = "Входящие звонки без ответа";

    $this->params['breadcrumbs'][] = \app\modules\vats\Module::getBreadcrumbs();
    $this->params['breadcrumbs'][] = $this->title;
}

?>
<div class="mpbx-index">

    <?= aHtmlHeader($this->title, $module, $controller, $action) ?>

    <p>
        <?/*= aIfModuleControllerAction($module, $controller, $action) ?
            Html::a('Добавить', ["/{$module}/{$controller}/create"], ['class' => 'btn btn-success'])
            : null  */?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute' => 'id',
                'headerOptions' => ['style' => 'width:100px;'],
                'format' => 'raw', 'value' => function($model) { return aGridVIewColumnId($model); }],

            //'created_at',
            //'markdel_at',
            //'markdel_by',

            [
                'attribute' => 'datetime_start',
                'label' => 'Дата',
                'value' => function(PbxCalls $model){

                    $ret = null;

                    if($model->datetime_start){
                        $ret = date('d-m-Y', strtotime($model->datetime_start));
                    }

                    if($ret == date('d-m-Y')){
                        $ret = "сегодня";
                    }

                    if($ret == date('d-m-Y', strtotime("-1 day"))){
                        $ret = "вчера";
                    }

                    return $ret;
                }
            ],

            [
                'attribute' => 'datetime_start',
                'label' => 'Время',
                'value' => function(PbxCalls $model){
                    if($model->datetime_start){
                        return date('H:i:s', strtotime($model->datetime_start));
                    }
                }
            ],

            'datetime_end:raw:Завершен',

            [
                'label' => 'Продолжительность',
                'value' => function(PbxCalls $model){
                    if($model->datetime_end && $model->datetime_start){
                        return round(
                                (strtotime($model->datetime_end) - strtotime($model->datetime_start))
                                / 60, 2)." мин";
                    }
                }
            ],

            //'call_id',
            //'direction',
            //'duration_waiting',
            //'answered',
            //'billsec',
            //'duration',

            [
                'label' => 'Куда звонил',
                'attribute' => 'office_tel',
                'format' => 'raw',
                'value' => function(PbxCalls $model){
                    return $model->office_tel;
                }
            ],

            //'file',

            /*[
                'label' => 'Внутренние номера',
                'attribute' => 'internalNumberSearch',
                'value' => function(PbxCalls $model){
                    return $model->internalNumberHistory;
                }
            ],*/

            [
                'label' => 'Кто звонил',
                'attribute' => 'external_number',
                'format' => 'raw',
                'value' => function(PbxCalls $model){
                    return $model->external_number;
                }
            ],

            [
                'label' => '',
                'format' => 'raw',
                'value' => function(PbxCalls $model){

                    $ret = [];

                    $ret [] = aHtmlButtonPostAjax("Позвонить",
                        ['/calls/vats-call-to', 'callto' => $model->external_number],
                    );

                    return implode("", $ret);
                }
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {files}', 'buttons' => [
                /*'view' => function($url, \app\modules\pbx\models\PbxCalls $model, $key){
                    return aGridViewActionColumnViewButton($model, $model->getUrlView());
                },*/

                'files' => function($url, \app\modules\pbx\models\PbxCalls $model, $key){

                    $count = count($model->uploads);

                    if($count == 1){

                        $upload = $model->uploads[0];
                        return Html::a("<i class='fas fa-paperclip'>{$count}</i>",
                            FileServerGetLink::http($upload->md5, $upload->ext),
                            ['data-pjax' => 0, 'target' => '_blank']
                        );

                    } elseif($count > 1){

                        return Html::a("<i class='fas fa-paperclip'>{$count}</i>",
                            ["/pbx/default/view", 'id' => $model->id, 'tab' => 'files'],
                            ['data-pjax' => 0]);

                    }

                }
            ]],        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
