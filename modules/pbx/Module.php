<?php

namespace app\modules\pbx;

use app\components\ModuleAccess;
use yii\helpers\ArrayHelper;

/**
 * pbx module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\pbx\controllers';

    const moduleId = "pbx";

    const moduleUrl = "/".self::moduleId;

    const moduleTitle = "Виртуальная АТС";

    // удялет из меню со списком всех модулей
    //const removeFromModuleMenu = true;

    // удляет из индивидуального меню для модуля
    //const removeFromThisModuleMenu = true;

    public static function getBreadcrumbs($url = true){
        return [
            'label' => self::moduleTitle,
            'url' => $url ? [self::moduleUrl.'/default/index'] : null,
        ];
    }

    public static function moduleMenu(){
        $array = [
            'label' => self::moduleTitle,
            'items' => [

                [
                    'label' => "Все звонки",
                    'url' => self::moduleUrl."/default",
                ],

                [
                    'label' => "Входящие звонки без ответа",
                    'url' => self::moduleUrl."/missed",
                ],

                [
                    'label' => "Входящие звонки отвеченные",
                    'url' => self::moduleUrl."/in",
                ],

                [
                    'label' => "Исходящие звонки без ответа",
                    'url' => self::moduleUrl."/out-no-answer",
                ],

                [
                    'label' => "Исходящие звонки отвеченные",
                    'url' => self::moduleUrl."/out",
                ],

                [
                    'label' => "Голосовая почта",
                    'url' => self::moduleUrl."/voicemail",
                ],
            ]
        ];

        ArrayHelper::multisort($array, 'label');

        return $array;
    }

    /**
     * {@inheritdoc}
     */
    public function init()
    {

        //ModuleAccess::check($this->id);

        parent::init();

        // custom initialization code goes here
    }
}
