<?php

namespace app\modules\vatssms\models;

use app\modules\vatssms\models\PbxSms;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PbxSmsSearch represents the model behind the search form of `app\modules\vatssms\models\PbxSms`.
 */
class PbxSmsSearch extends PbxSms
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by', 'markdel_by'], 'integer'],
            [['name'], 'string'],
            [['datetime'], 'string'],
            [['from'], 'string'],
            [['to'], 'string'],
            [['created_at', 'updated_at', 'markdel_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $params2 = [])
    {
        $query = PbxSms::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'markdel_at' => $this->markdel_at,
            'markdel_by' => $this->markdel_by,
        ]);
        
        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'to', $this->to]);
        $query->andFilterWhere(['like', 'from', $this->from]);
        $query->andFilterWhere(['like', 'datetime', $this->datetime]);
        //    ->andFilterWhere(['like', 'date', $this->date])
        //;
        

        if(!empty($params2['from_like'])){
            $query->andWhere(['like', 'from', $params2['from_like']]);
        }

        if(!empty($params2['to_like'])){
            $query->andWhere(['like', 'to', $params2['to_like']]);
        }


        return $dataProvider;
    }
}
