<?php

namespace app\modules\pbx\models;

use app\modules\mcn_webhook\models\PbxWebhook;
use Yii;
use app\modules\users\models\User;
use yii\helpers\Html;

/**
 * This is the model class for table "m_mcncalls".
 *
 * @property int $id
 * @property string $created_at Добавлено когда
 *
 * @property string $name Наименование
 *
 * @property string $mcn_callId
 * @property string $internal_number
 * @property string $m_mcncalls_id
 *
 */
class MMcnCallsUniqueInternalNumber extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_mcncalls__unique_internal_number';
    }

    public static function createNew($call_id, $internalNumber)
    {

        if(!$internalNumber){
            return;
        }

        if(self::find()
            ->andWhere(['mcn_callId' => $call_id])
            ->andWhere(['internal_number' => $internalNumber])
            ->count() == 0
        ) {

            $model = new MMcnCallsUniqueInternalNumber();
            $model->created_at = aDateNow();
            $model->internal_number = $internalNumber;
            $model->mcn_callId = $call_id;

            if($m_mcncalls_id = PbxCalls::findOne(['call_id' => $call_id])){
                $model->m_mcncalls_id = $m_mcncalls_id->id;
            }

            $model->save();

        }

    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mcn_callId'], 'string'],

            [['internal_number'], 'integer'],

            [['created_at'], 'safe'],

            [['m_mcncalls_id'], 'integer'],
            [['m_mcncalls_id'], 'exist', 'skipOnError' => true, 'targetClass' => PbxCalls::className(), 'targetAttribute' => ['m_mcncalls_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcnCall()
    {
        return $this->hasOne(PbxCalls::className(), ['id' => 'm_mcncalls_id']);
    }

    /**
     * {@inheritdoc}
     * @return MMcnCallsUniqueInternalNumberQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MMcnCallsUniqueInternalNumberQuery(get_called_class());
    }

}
