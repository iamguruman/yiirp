<?php

namespace app\modules\pbx\models;

/**
 * This is the ActiveQuery class for [[PbxCalls]].
 *
 * @see PbxCalls
 */
class PbxCallsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    public function init()
    {
        parent::init();

        if(aIfHideMarkdel()){
            $this->andWhere(['m_mcncalls.markdel_by' => null]);
        }
    }

    /**
     * {@inheritdoc}
     * @return PbxCalls[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PbxCalls|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
