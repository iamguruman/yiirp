<?php

namespace app\modules\pbx\models;

/**
 * This is the ActiveQuery class for [[PbxCalls]].
 *
 * @see PbxCalls
 */
class MMcnCallsUniqueInternalNumberQuery extends \yii\db\ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return PbxCalls[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PbxCalls|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
