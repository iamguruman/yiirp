<?php

namespace app\modules\pbx\models;

use app\modules\pbx_webhook\models\PbxWebhook;
use app\modules\user\models\User;
use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "m_mcncalls".
 *
 * @property int $id
 * @property string $created_at Добавлено когда
 *
 * @property int $created_by Добавлено кем
 * @property User $createdBy
 *
 * @property string $updated_at Изменено когда
 *
 * @property int $updated_by Изменено кем
 * @property User $updatedBy
 *
 * @property string $markdel_at Удалено когда
 *
 * @property int $markdel_by Удалено кем
 * @property User $markdelBy
 *
 * @property string $name Наименование
 *
 * @property-read MMcnCallsUpload[] $uploads - вложения, см метод getUploads
 *
 * @property-read string $urlIconToFiles - ссылка-иконка на вложения
 *
 * @property-read PbxWebhook[] $webhooks
 * @property-read PbxWebhook[] $webhooksWithInternalNumber
 *
 * @property string $datetime_start
 * @property string $datetime_end
 *
 * @property string $call_id
 * @property string $direction
 * @property string $office_tel
 * @property string $internal_number
 * @property string $external_number
 * @property string $billsec
 * @property string $duration
 * @property string $answered
 * @property string $file
 * @property string $duration_waiting
 *
 * @property-read string $internalNumberHistory
 *
 * @property-read array $allUniqueInternalNumberFromCall - список уникальный внутренних номеров,
 *                                                          которые участвуют в телефонном звонке
 *
 * @property-read MMcnCallsUniqueInternalNumber[] $uniqueInternalNumbers - список всех внутренних номеров
 *                                                              в которые попал данный звонок
 *
 */
class PbxCalls extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pbx_calls';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            /*[['datetime_start'], 'string'],
            [['datetime_end'], 'string'],
            [['direction'], 'string', 'max' => 3],

            [['office_tel'], 'integer'],
            [['external_number'], 'integer'],
            [['internal_number'], 'integer'],

            [['billsec'], 'string', 'max' => 255],
            [['duration'], 'string', 'max' => 255],
            [['answered'], 'string', 'max' => 1],
            [['file'], 'string', 'max' => 500],
            [['duration_waiting'], 'integer'],

            [['call_id'], 'string', 'max' => 30],*/

            [['name'], 'string', 'max' => 255],

            [['created_at', 'updated_at', 'markdel_at'], 'safe'],

            [['created_by', 'updated_by', 'markdel_by'], 'integer'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['markdel_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['markdel_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',

            'urlTo' => 'Звонок через АТС',
            'urlToBlank' => 'Звонок через АТС',

            'created_at' => 'Добавлено когда',

            'created_by' => 'Добавлено кем',
            'createdBy.lastNameWithInitials' => 'Добавлено кем',

            'updated_at' => 'Изменено когда',

            'updated_by' => 'Изменено кем',
            'updatedBy.lastNameWithInitials' => 'Изменено кем',

            'markdel_at' => 'Удалено когда',

            'markdel_by' => 'Удалено кем',
            'markdelBy.lastNameWithInitials' => 'Удалено кем',

            'name' => 'Наименование',
        ];
    }

    /**
     * ссылка на просмотр объекта
     * @return array
     */
    public function getUrlView($return = 'string'){
        
        $arr = [];
        
        $arr [] = ['/pbx/default/view', 'id' => $this->id];
        
        if($return == 'array'){
            return $arr;
        } elseif ($return == 'string'){
            return "/pbx/default/view?id={$this->id}";
        } else {
            return $arr;
        }
    }

    /**
     * ссылка к списку объектов
     * @return array
     */
    public function getUrlIndex(){
        return ['/pbx/default/index'];
    }

    public function getUrlTo($target = null){
        return Html::a(str_replace("&nbsp;", " ", $this->getTitle()),
            $this->getUrlView(),
            ['target' => $target, 'data-pjax' => 0]);
    }

    /**
     * получить заголовок объекта
     * @return string
     */
    public function getTitle(){

        $ret = [];
        
        if($this->name)
            $ret [] = $this->name;
        
        if(count($ret) == 0){
            $ret [] = "Без названия";
        }
        
        $ret [] = $this->getUrlIconToFiles();
        
        return implode(' ', $ret);
    
    }
    
    public function getUrlIconToFiles(){

        return;
        /*$count = count($this->uploads);

        $ret = [];

        if($count == 0){
            return;
        } elseif($count == 1){
            $ret [] = Html::a("<i class='fas fa-paperclip'>{$count}</i>",
                FileServerGetLink::http($this->uploads[0]->md5, $this->uploads[0]->ext), ['data-pjax' => 0]);
        } elseif ($count > 1){
            $ret [] = Html::a("<i class='fas fa-paperclip'>{$count}</i>",
                [$this->getUrlView('string'), 'tab' => 'files'], ['data-pjax' => 0]);
        } 

        return implode($ret);*/
    }

    public function getUrlToBlank(){
        return $this->getUrlTo('_blank');
    }

    public function getBreadcrumbs(){
        return [
            'label' => $this->getTitle(),
            'url' => $this->getUrlView()
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarkdelBy()
    {
        return $this->hasOne(User::className(), ['id' => 'markdel_by']);
    }

    /**
     * получаем файлы вложения
     * @return \yii\db\ActiveQuery
     */
/*    public function getUploads()
    {
        return $this->hasMany(MMcnCallsUpload::className(), ['object_id' => 'id']);
    }*/

    /**
     * {@inheritdoc}
     * @return PbxCallsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PbxCallsQuery(get_called_class());
    }

    public function getWebhooks(){
        return $this->hasMany(PbxWebhook::className(), ['call_id' => 'call_id']);
    }

    public function getWebhooksWithInternalNumber(){
        return $this->hasMany(PbxWebhook::className(), ['call_id' => 'call_id'])
            ->andWhere(['is not', PbxWebhook::tableName().'.internalNumber', null]);
    }

    public function getInternalNumberHistory(): string
    {

        $ret = [];

        foreach ($this->webhooksWithInternalNumber as $key => $value){

            if($value->internalNumber){
                if($key == 0){

                    $ret [] = $value->internalNumber;

                } elseif ($key > 0){

                    if($this->webhooksWithInternalNumber[$key-1]->internalNumber != $value->internalNumber){
                        $ret [] = $value->internalNumber;
                    }

                }
            }

        }

        return implode(" > ", $ret);

    }

    public function getAllUniqueInternalNumberFromCall(): array
    {

        $ret = [];

        foreach ($this->webhooksWithInternalNumber as $key => $value){

            if($value->internalNumber){
                if($key == 0){

                    $ret [] = $value->internalNumber;

                } elseif ($key > 0){

                    if($this->webhooksWithInternalNumber[$key-1]->internalNumber != $value->internalNumber){
                        $ret [] = $value->internalNumber;
                    }

                }
            }

        }

        array_unique($ret);

        return $ret;

    }

    public function getUniqueInternalNumbers(){
        return $this->hasMany(MMcnCallsUniqueInternalNumber::className(), ['m_mcncalls_id' => 'id']);
    }
}
