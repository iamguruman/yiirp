<?php

namespace app\modules\pbx\models;

use app\modules\pbx\models\PbxCalls;
use app\modules\mcn_webhook\models\PbxWebhook;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PbxCallsSearch represents the model behind the search form of `app\modules\pbx\models\PbxCalls`.
 */
class PbxCallsSearch extends PbxCalls
{

    public $internalNumberSearch;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by', 'markdel_by'], 'integer'],
            [['name'], 'string'],
            [['created_at', 'updated_at', 'markdel_at'], 'safe'],
            [['datetime_end'], 'string'],
            [['datetime_start'], 'string'],
            [['office_tel'], 'string'],
            [['external_number'], 'string'],
            [['internalNumberSearch'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $params2 = [])
    {
        $query = PbxCalls::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'markdel_at' => $this->markdel_at,
            'markdel_by' => $this->markdel_by,
        ]);
        
        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'office_tel', $this->office_tel]);
        $query->andFilterWhere(['like', 'external_number', $this->external_number]);
        $query->andFilterWhere(['like', 'datetime_start', $this->datetime_start]);
        $query->andFilterWhere(['like', 'datetime_end', $this->datetime_end]);
        //    ->andFilterWhere(['like', 'date', $this->date])
        //;
        

        if(!empty($params2['direction'])){
            $query->andWhere(['direction' => $params2['direction']]);
        }

        if(!empty($params2['not_answered']) && $params2['not_answered'] == true){
            $query->andWhere(['or', ['answered' => null], ['answered' => 0]]);
        }

        if(!empty($params2['is_answered']) && $params2['is_answered'] == true){
            $query->andWhere(['answered' => 1]);
        }

        if(!empty($params2['internal_number'])){
            $query->joinWith('uniqueInternalNumbers')
                ->andWhere([MMcnCallsUniqueInternalNumber::tableName().'.internal_number' => $params2['internal_number']]);
        }

        if($this->internalNumberSearch){
            $query->joinWith('webhooks')
                ->andWhere(['like', PbxWebhook::tableName().'.internalNumber',  $this->internalNumberSearch]);
        }


        return $dataProvider;
    }
}
