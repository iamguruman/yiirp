<?php

namespace app\modules\vatssms\models;

/**
 * This is the ActiveQuery class for [[PbxSms]].
 *
 * @see PbxSms
 */
class PbxSmsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    public function init()
    {
        parent::init();

        if(aIfHideMarkdel()){
            $this->andWhere(['m_vatssms.markdel_by' => null]);
        }
    }

    /**
     * {@inheritdoc}
     * @return PbxSms[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PbxSms|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
