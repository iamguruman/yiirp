<?php

namespace app\modules\pbx\controllers;

use app\modules\pbx\models\PbxCallsSearch;
use Yii;

class OutNoAnswerController extends \yii\web\Controller
{

    public function actionIndex(){

        $searchModel = new PbxCallsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [
            'direction' => 'out',
            'not_answered' => true,
            'internal_number' => Yii::$app->user->identity->tel_internal_number,
        ]);
        $dataProvider->setSort(['defaultOrder' => ['datetime_start' => SORT_DESC]]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

}