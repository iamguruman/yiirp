<?php

namespace app\modules\pbx\controllers;

use app\modules\vatssms\models\MVatsSmsUpload;
use app\modules\vatssms\models\MVatsSmsUploadSearch;
use app\modules\vatssms\models\PbxSms;
use app\modules\vatssms\models\PbxSmsSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;

/**
 * DefaultController implements the CRUD actions for PbxSms model.
 */
class SmsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }


    /**
     * Lists all PbxSms models.
     * @return mixed
     */
    public function actionInbox()
    {
        $searchModel = new PbxSmsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [
            'to_like' => Yii::$app->user->identity->tel_mobile_num
        ]);
        $dataProvider->setSort(['defaultOrder' => ['id' => SORT_DESC]]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all PbxSms models.
     * @return mixed
     */
    public function actionSend()
    {
        $searchModel = new PbxSmsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [
            'from_like' => Yii::$app->user->identity->tel_mobile_num
        ]);
        $dataProvider->setSort(['defaultOrder' => ['id' => SORT_DESC]]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the PbxSms model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PbxSms the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PbxSms::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
