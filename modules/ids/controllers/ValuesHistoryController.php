<?php

namespace app\modules\ids\controllers;

use app\modules\ids\models\IdsValuesHistory;
use app\modules\ids\models\IdsValuesHistorySearch;
use app\modules\ids_template\models\TemplateField;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ValuesHistoryController implements the CRUD actions for IdsValuesHistory model.
 */
class ValuesHistoryController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    public function actionRestoreValue($id){

        $oldValueModel = $this->findModel($id);

        $fieldModel = TemplateField::findOne($oldValueModel->ids_template_field_id);

        $ids_id = $oldValueModel->ids_id;

        $sql = [];

        $sql [] = "update `ids_value_{$fieldModel->value_field}` ";
        $sql [] = "set `value`='{$oldValueModel->value}' ";
        $sql [] = "where ";
        $sql [] = "`ids_id` = {$ids_id} ";
        $sql [] = "and `template_field_id` = {$oldValueModel->ids_template_field_id} ";

        $sql = implode($sql);

        $result = Yii::$app->db->createCommand($sql)->query();

        $historyModel = new IdsValuesHistory();
        $historyModel->created_at = aDateNow();
        $historyModel->ids_template_name_id = $oldValueModel->ids_template_name_id;
        $historyModel->ids_template_field_id = $oldValueModel->ids_template_field_id;
        $historyModel->ids_id = $ids_id;
        $historyModel->value = $oldValueModel->value;
        $historyModel->action_type = "restore";
        $historyModel->restored_id = $oldValueModel->id;
        if($historyModel->save()){
        } else {
            ddd($historyModel->errors);
        }

        aReturnto();
        return $this->redirect(["/ids/view", 'id' => $ids_id]);

    }

    /**
     * Lists all IdsValuesHistory models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new IdsValuesHistorySearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single IdsValuesHistory model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new IdsValuesHistory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new IdsValuesHistory();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing IdsValuesHistory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing IdsValuesHistory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the IdsValuesHistory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return IdsValuesHistory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IdsValuesHistory::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
