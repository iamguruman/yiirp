<?php

namespace app\modules\ids\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class ClearController extends Controller
{

    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@']
                        ],
                    ],
                ],
            ]


        );
    }
    public function actionIndex(){

        Yii::$app->db->createCommand("TRUNCATE `yiirp`.`ids`")->execute();
        Yii::$app->db->createCommand("TRUNCATE `yiirp`.`ids_tree`")->execute();
        Yii::$app->db->createCommand("TRUNCATE `yiirp`.`ids_values_history`")->execute();
        Yii::$app->db->createCommand("TRUNCATE `yiirp`.`ids_value_date`")->execute();
        Yii::$app->db->createCommand("TRUNCATE `yiirp`.`ids_value_datetime`")->execute();
        Yii::$app->db->createCommand("TRUNCATE `yiirp`.`ids_value_decimal102`")->execute();
        Yii::$app->db->createCommand("TRUNCATE `yiirp`.`ids_value_int11`")->execute();
        Yii::$app->db->createCommand("TRUNCATE `yiirp`.`ids_value_longblob`")->execute();
        Yii::$app->db->createCommand("TRUNCATE `yiirp`.`ids_value_text`")->execute();
        Yii::$app->db->createCommand("TRUNCATE `yiirp`.`ids_value_time`")->execute();
        Yii::$app->db->createCommand("TRUNCATE `yiirp`.`ids_value_varchar255`")->execute();
        Yii::$app->db->createCommand("TRUNCATE `yiirp`.`ids_value_varchar500`")->execute();
        Yii::$app->db->createCommand("TRUNCATE `yiirp`.`ids_value_varchar1000`")->execute();

        return $this->render("index");
    }

}