<?php

namespace app\modules\ids\controllers;

use app\modules\ids\models\Ids;
use app\modules\ids\models\IdsSearch;
use app\modules\ids_template\models\IdsTemplateUserAccess;
use app\modules\ids_template\models\TemplateField;
use app\modules\ids_template\models\TemplateName;
use app\modules\ids_tree\models\IdsTree;
use Yii;
use yii\filters\AccessControl;
use yii\grid\ActionColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * DefaultController implements the CRUD actions for Ids model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@']
                        ],
                    ],
                ],
            ]


        );
    }

    /**
     * Lists all Ids models.
     *
     * @return string
     */
    public function actionIndex($template_id = null)
    {

        $templateName = TemplateName::findOne($template_id);

        /*foreach (TemplateName::find()->all() as $template){
            dd($template->name);
            if(IdsTemplateUserAccess::find()
                ->andWhere(['ids_template_name_id' => $template->id])
                ->andWhere(['sys_user_id' => aUserMyId()])
                ->count() == 0
            ) {
                dd(0);
                $acc = new IdsTemplateUserAccess();
                $acc->created_by = aUserMyId();
                $acc->created_at = aDateNow();
                $acc->ids_template_name_id = $template->id;
                $acc->sys_user_id = aUserMyId();
                $acc->can_list = 1;
                $acc->can_create = 1;
                $acc->can_update_other_created = 1;
                $acc->can_update_self_created = 1;
                $acc->can_read_only_self_created = 0;
                $acc->save();
            } else {

            }
        }*/

        if (aGet('search') && aGet('search')[0] == '='){
            return $this->redirect(['/ids/view', 'id' => str_replace("=", "", aGet('search'))]);
        }

        $searchModel = new IdsSearch();
        $dataProvider = $searchModel->search($this->request->queryParams, [
            'template_id' => Yii::$app->request->get('template_id'),
            'search' => aGet('search'),
            'user_can_list' => true,
            'ids_template_status_name_id' => aGet('status'),
            'hide_markdel' => Yii::$app->user->identity->hide_markdel
        ]);

        if(Yii::$app->user->identity->default_sort == 'desc'){
            $dataProvider->setSort(['defaultOrder' => ['id' => SORT_DESC]]);
        }


        $columns = [];
        $columns [] = ['class' => 'yii\grid\SerialColumn'];

        !$template_id ? $columns [] = [
            'label' => 'Icon',
            'format' => 'raw',
            'value' => function(Ids $model){
                $icon = Yii::getAlias("@app/web/tplicons/{$model->template_name_id}.png");
                if(file_exists($icon)){

                    $ret = [];

                    $title = '';
                    if($model->templateName && $model->templateName->name){
                        $title = $model->templateName->name;
                    }

                    $ret [] = Html::a(
                        "<img src='/tplicons/{$model->template_name_id}.png' title='{$title}' style='max-height: 30px;'>",
                        ['/ids', 'template_id' =>$model->template_name_id],
                        ['data-pjax' => 0]);

                    if($model->markdelBy){
                        $ret [] = "<span style='color: red;' title='Deleted by {$model->markdelBy->username} at {$model->markdelAtFormatted}'>d</span>";
                    }

                    return implode("&nbsp;", $ret);
                }
            }
        ] : ['visible' => false];



        $columns [] = ['attribute' => 'id',
            'format' => 'raw',
            'value' => function(Ids $model){
                $ret = [];

                if($model->createdBy){
                    $title = [];

                    $title [] = "Created by {$model->createdBy->username} at {$model->created_at}";

                    if($model->updatedBy){
                        $title [] = ", updated by {$model->updatedBy->username} at {$model->updated_at}";
                    }

                    if($model->markdelBy){
                        $title [] = ", deleted by {$model->markdelBy->username} at {$model->markdel_at}";
                    }

                    $title = implode(" ", $title);

                    $ret [] = "<img src='/usericons/nopic.png' height='15' title='{$title}'>";
                }

                $ret [] = $model->id;

                if($model->markdelBy){
                    $title = "Mark deleted by {$model->markdelBy->lastname} at {$model->markdel_at}";
                    $ret [] = "<img src='/media/icon_trash_red.png' height='25' title='$title'>";
                }

                return implode("&nbsp;",$ret);
            }
        ];

        if($templateName){
            if($templateName->show_field_name_on_crud_index){
                $columns [] = ['attribute' => 'name'];
            }
        } else {
            $columns [] = ['attribute' => 'name'];
        }

        //global $templateFieldTable, $templateFieldId;
        if($templateName){
            /** @var TemplateField $templateField */
            foreach ($templateName->templateFields as $templateField){
                if($templateField->show_as_column_at_template_list){

                    $templateFieldId = $templateField->id;
                    $templateFieldTable = $templateField->value_field;

                    $columns [] = [
                        'label' => "{$templateField->title} ($templateField->id)",
                        'format' => 'raw',
                        'value' => function(Ids $model) use ($templateFieldId){
                        //ddd($grid);

                            return $model->getFieldValue($templateFieldId);

                            /*return TemplateField::getFieldValueStatic(
                                $model->id,
                                $GLOBALS['templateFieldId'],
                                $GLOBALS['templateFieldTable']
                            );*/
                        }
                    ];
                }
            }
        }

        $columns [] = [
            'label' => '',
            'format' => 'raw',
            'value' => function(Ids $model){

                $ret = [];

                if($model->templateName && $model->templateName->extra_module_action_view){
                    $ret [] = Html::a("vM",
                        strtr($model->templateName->extra_module_action_view, ['{id}' => $model->id]),
                        ['data-pjax' => 0]);
                }

                $ret [] = Html::a("<img src='/media/icon_eye.png' height='20'>", ['/ids/view', 'id' => $model->id], ['data-pjax' => 0]);

                $ret [] = Html::a("<img src='/media/icon_yandex_disk.png' height='20'>",
                    "https://disk.yandex.ru/client/disk/yiirp/{$model->id}",
                    ['data-pjax' => 0, 'target' => '_blank']
                );

                return implode("&nbsp;", $ret);
            }
        ];

        return $this->render('index', [
            'columns' => $columns,

            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }



    /**
     * Finds the Ids model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Ids the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ids::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
