<?php

namespace app\modules\ids\controllers;

use app\modules\ids\models\Ids;
use app\modules\ids\models\IdsSearch;
use app\modules\ids\models\IdsValuesHistory;
use app\modules\ids_flow\models\IdsFlow;
use app\modules\ids_template\models\TemplateField;
use app\modules\ids_template\models\TemplateName;
use app\modules\ids_tree\models\IdsTree;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for Ids model.
 */
class CreateController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@']
                        ],
                    ],
                ],
            ]


        );
    }

    /**
     * Creates a new Ids model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionIndex($template_id = null, $copy_id = null)
    {

        if(!aUserMyId()){
            return $this->goHome();
        }

        $model = new Ids();

        if($templateName = TemplateName::findOne($template_id)){
            $model->template_name_id = $templateName->id;
        }

        if($parent = Ids::findOne(aGet('parent_id'))){
            $model->parent_id = $parent->id;
        }

        if($child = Ids::findOne(aGet('child_id'))){
            $model->child_id = $child->id;
        }

        if($flow = IdsFlow::findOne(aGet('flow_id'))){
            $model->ids_template_status_name_id = $flow->child_ids_template_status_name_id;
        }

        $model->created_at = aDateNow();
        $model->created_by = aUserMyId();

        if($copyFromModel = Ids::findOne($copy_id)){
            /** @var TemplateField $templateField */
            foreach ($model->templateName->templateFields as $templateField){
                $model->fieldValue[$templateField->id] = $copyFromModel->getFieldValue($templateField->id);
            }
        }

        if ($this->request->isPost) {

            if ($model->load($this->request->post())) {

                if ($model->save()) {

                    self::updateValues($model);

                    self::setNameFromNameTemplate($model);

                    self::createParentChildTree($model);

                    aReturnto();
                    return $this->redirect(['/ids/view', 'id' => $model->id]);

                } else {
                    ddd($model->errors);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public static function updateValues(Ids $model){


        foreach ($model->fieldValue as $field_id => $value){

            $act = null;

            if($fieldModel = TemplateField::findOne($field_id)){

                // в случае если в поле стоит счетчик
                if($fieldModel->auto_increment){

                    $sql = "
                        select value
                        from `ids_value_{$fieldModel->value_field}` 
                        inner join `ids_template_field` 
                                on `ids_template_field`.`id` 
                                       = `ids_value_{$fieldModel->value_field}`.`template_field_id` 
                        where `ids_template_field`.`template_name_id` = {$fieldModel->template_name_id}
                        order by `value` DESC
                        limit 1
                    ";

                    $lastAi = Yii::$app->db->createCommand($sql)->queryScalar();

                    $removeTextFromAiValue = str_replace("{AI}", "", $value);

                    $lastAi = str_replace($removeTextFromAiValue, "", $lastAi);

                    $lastAi++;

                    $value = str_replace("{AI}", $lastAi, $value);

                }

                if($value){

                    $sql = "
                                select count(*) as `count` 
                                from `ids_value_{$fieldModel->value_field}` 
                                where 
                                    `ids_id` = {$model->id}
                                and `template_field_id` = {$fieldModel->id}
                            ";

                    $count = Yii::$app->db->createCommand($sql)->queryScalar();

                    foreach($fieldModel->idsTemplateFieldReplaces as $replace){
                        $value = str_replace($replace->text_search, $replace->text_replace, $value);
                    }

                    if($count == 0){

                        $sql = "
                                    insert into `ids_value_{$fieldModel->value_field}` 
                                        (`created_at`, `ids_id`, `template_field_id`, `value`)  
                                    values 
                                        (NOW(), '{$model->id}', '{$fieldModel->id}', '{$value}')
                                ";

                        $act = "insert";

                    } else {

                        $sql = "
                                select `value`
                                from `ids_value_{$fieldModel->value_field}` 
                                where 
                                    `ids_id` = {$model->id}
                                and `template_field_id` = {$fieldModel->id}
                            ";
                        $valueFromDb = Yii::$app->db->createCommand($sql)->queryScalar();

                        if($value != $valueFromDb){
                            $sql = "
                                    update `ids_value_{$fieldModel->value_field}` 
                                    set `value` = '{$value}', `updated_at` = NOW()
                                    where 
                                        `ids_id` = '{$model->id}'
                                    and `template_field_id` = '{$fieldModel->id}'
                                ";

                            $act = "update";
                        }

                    }

                    $count = Yii::$app->db->createCommand($sql)->queryScalar();

                    if($fieldModel->saving_changes & ($act == 'insert' or $act == 'update')){

                        $valuesHistoryModel = new IdsValuesHistory();
                        $valuesHistoryModel->created_at = aDateNow();
                        $valuesHistoryModel->ids_template_name_id = $fieldModel->template_name_id;
                        $valuesHistoryModel->ids_template_field_id = $fieldModel->id;
                        $valuesHistoryModel->ids_id = $model->id;
                        $valuesHistoryModel->value = $value;
                        $valuesHistoryModel->action_type = $act;

                        if($valuesHistoryModel->save()){
                        } else {
                            ddd($valuesHistoryModel->errors);
                        }

                    }

                }

            }

        }
    }

    public static function setNameFromNameTemplate(Ids &$model)
    {
        if($model->templateName){
            $templateName = $model->templateName;

            if($templateName->name_template){

                $model->name = $templateName->name_template;

                preg_match_all('#{.*(.*)}#U', $templateName->name_template, $matches);
                foreach($matches[1] as $match){

                    $fieldModel = TemplateField::findOne($match);

                    $sql = "
                        select value 
                        from `ids_value_{$fieldModel->value_field}`
                        where 
                            `ids_id` = {$model->id}
                        and `template_field_id` = {$match}
                    ";

                    $replace = Yii::$app->db->createCommand($sql)->queryScalar();

                    if($fieldModel->sourceTemplateName){
                        $replace = Ids::findOne($replace)->name;
                    }

                    $model->name = str_replace('{'.$match.'}', $replace, $model->name);

                    $model->save();
                }

            }

        }
    }

    private static function createParentChildTree(Ids $model)
    {

        if($model->parent_id or $model->child_id){

            $treeModel = new IdsTree();
            $treeModel->created_at = aDateNow();

            if($model->child_id){
                $treeModel->child_ids_id = $model->child_id;
                $treeModel->parent_ids_id = $model->id;
            }

            if($model->parent_id){
                $treeModel->parent_ids_id = $model->parent_id;
                $treeModel->child_ids_id = $model->id;
            }

            if($treeModel->save()){

            } else {
                ddd($treeModel->errors);
            }

        }

        if($model->extra_parents_id){
            foreach ($model->extra_parents_id as $epKey => $epValue){
                if($epValue){
                    $treeModel = new IdsTree();
                    $treeModel->created_at = aDateNow();
                    $treeModel->child_ids_id = $model->id;
                    $treeModel->parent_ids_id = $epKey;
                    if($treeModel->save()){
                    } else {
                        ddd($treeModel->errors);
                    }
                }
            }
        }

    }

    /**
     * Finds the Ids model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Ids the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ids::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
