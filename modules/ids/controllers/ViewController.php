<?php

namespace app\modules\ids\controllers;

use app\modules\ids\models\Ids;
use app\modules\ids\models\IdsSearch;
use app\modules\ids\models\IdsValuesHistory;
use app\modules\ids\models\IdsValuesHistorySearch;
use app\modules\ids_flow\models\IdsFlowSearch;
use app\modules\ids_template\models\IdsPrintTemplate;
use app\modules\ids_template\models\IdsPrintTemplateSearch;
use app\modules\ids_template\models\IdsTemplateStatusName;
use app\modules\ids_template\models\TemplateField;
use app\modules\ids_tree\models\IdsTree;
use app\modules\ids_tree\models\IdsTreeSearch;
use Yii;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for Ids model.
 */
class ViewController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Displays a single Ids model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionIndex($id)
    {

        $model = $this->findModel($id);

        $attributes = [];
        $attributes [] = "id";

        if(IdsTemplateStatusName::find()
                ->andWhere(['ids_template_name_id' => $model->template_name_id])
                ->count() > 0)
        {
            $attributes [] = [
                'label' => 'Status',
                'value' => function(Ids $model){
                    if($model->idsTemplateStatusName){
                        return $model->idsTemplateStatusName->name;
                    }
                }
            ];
        }

        /*if(aUserMyId() == 8) {
            $attributes [] = [
                'label' => 'Status2',
                'format' => 'raw',
                'value' => function (Ids $model) {

                    $html = [];

                    $html [] = "<select size='3' style='width: 100%;'>";

                    $html [] = "<option value=''>Выбрать значение статуса</option>";

                    if($model->templateName){
                        foreach ($model->templateName->statusNames as $statusName){

                            $selected = "";
                            if($model->ids_template_status_name_id == $statusName->id){
                                $selected = " selected";
                            }

                            $onchange = "alert(this.value)";

                            $html [] = "<option style='{$statusName->style}' onchange=\"{$onchange}\" value='{$statusName->id}'{$selected}>{$statusName->name}</option>";
                        }
                    }

                    $html [] = "</select>";

                    return implode("", $html);
                }
            ];
        }*/

        //$attributes [] = "name";

        if($templateName = $model->templateName){
            /** @var TemplateField $templateField */
            foreach ($templateName->templateFields as $templateField){

                $attributes [] = [

                    'label' => "{$templateField->title} ($templateField->id)",

                    'value' => function($model) use ($templateField) {
                        return $templateField->getFieldValue($model->id, $templateField->id);
                    },

                    'format' =>
                        $templateField->html_element == 'textarea' ? "ntext"
                            : 'raw',

                ];
            }
        }

        $flowSearchModel = new IdsFlowSearch();
        $flowDataProvider = $flowSearchModel->search(Yii::$app->request->queryParams, [
            'template_id' => $model->template_name_id
        ]);

        $parentSearchModel = new IdsTreeSearch();
        $parentDataProvider = $parentSearchModel->search(Yii::$app->request->queryParams, [
            'parent_ids_id' => $model->id
        ]);
        $parentDataProvider->setSort(['defaultOrder' =>['created_at' => SORT_DESC]]);

        $childSearchModel = new IdsTreeSearch();
        $childDataProvider = $childSearchModel->search(Yii::$app->request->queryParams, [
            'child_ids_id' => $model->id
        ]);
        $childDataProvider->setSort(['defaultOrder' =>['created_at' => SORT_DESC]]);

        $historySearchModel = new IdsValuesHistorySearch();
        $historyDataProvider = $historySearchModel->search(Yii::$app->request->queryParams, [
            'ids_id' => $model->id
        ]);
        $historyDataProvider->setSort(['defaultOrder' =>['created_at' => SORT_DESC]]);

        $printSearchModel = new IdsPrintTemplateSearch();
        $printDataProvider = $printSearchModel->search(Yii::$app->request->queryParams, [
            'ids_template_name_id' => $model->template_name_id
        ]);
        $historyDataProvider->setSort(['defaultOrder' =>['created_at' => SORT_DESC]]);

        return $this->render('view', [

            'printSearchModel' => $printSearchModel,
            'printDataProvider' => $printDataProvider,

            'historySearchModel' => $historySearchModel,
            'historyDataProvider' => $historyDataProvider,

            'parentSearchModel' => $parentSearchModel,
            'parentDataProvider' => $parentDataProvider,

            'childSearchModel' => $childSearchModel,
            'childDataProvider' => $childDataProvider,

            'flowSearchModel' => $flowSearchModel,
            'flowDataProvider' => $flowDataProvider,

            'attributes' => $attributes,

            'model' => $model,
        ]);
    }

    /**
     * Finds the Ids model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Ids the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ids::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
