<?php

namespace app\modules\ids\controllers;

use app\modules\ids\models\Ids;
use app\modules\ids_template\models\IdsPrintTemplate;
use app\modules\ids_template\models\TemplateField;
use app\modules\ids_tree\models\IdsTree;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class PrintController extends Controller
{

    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@']
                        ],
                    ],
                ],
            ]


        );
    }

    public function actionIndex($id, $print_template){


        $idsModel = Ids::findOne($id);
        $template_name_id = $idsModel->template_name_id;

        $printModel = IdsPrintTemplate::findOne($print_template);

        $html = $printModel->html_content;

        $html = str_replace("{ID}", $idsModel->id, $html);
        $html = str_replace("{id}", $idsModel->id, $html);

        // CHILD TEMPLATES ------------
        preg_match_all('/<TPLCHILD=\d{0,}>(.*?)<\/TPLCHILD=\d{0,}>/s', $html, $matches);
        $tplIds = $matches[0];
        $tplHtmls = $matches[1];
        $i = 1;
        foreach ($tplIds as $key => $value){
            preg_match('/<TPLCHILD=(.*?)>/s', $value, $tag);
            $tplId = $tag[1];

            //$html = str_replace('{i}', $i, $html);
            $rpl = str_replace("{i}", $i, $tplHtmls[$key]);
            $html = str_replace($tplHtmls[$key], self::replaceHtml($rpl, $idsModel, $tplId), $html);
            $i++;
        }

        // THIS IDs -------------
        preg_match_all('#{.*(.*)}#U', $html, $matches);

        foreach($matches[1] as $match){

            if(str_contains($match, '[') and str_contains($match, ']')){

                //dd($match);
                preg_match_all('#\[.*(.*)\]#U', $match, $connectedFieldHtmlTpl);
                $connectedField_id = str_replace($connectedFieldHtmlTpl[0], "", $match);

                $connectedIds_id = Ids::staticGetValueField($idsModel->id, $connectedField_id);
                $connectedIdsModel = Ids::findOne($connectedIds_id);

                $replace = $connectedFieldHtmlTpl[1][0];

                preg_match_all('#<.*(.*)>#U', $replace, $replaces);
                //dd($replaces);
                foreach ($replaces[0] as $repKey => $repTpl){
                    $replace = str_replace($repTpl, $connectedIdsModel->getFieldValue($replaces[1][$repKey]), $replace);
                    //dd($connectedIdsModel->getFieldValue($replaces[1][$repKey]), $replaces[1][$repKey]);
                }

                $html = str_replace('{'.$match.'}', $replace, $html);

            } else {

                $matchModel = TemplateField::findOne($match);

                $sql = "
                        select value 
                        from `ids_value_{$matchModel->value_field}`
                        where 
                            `ids_id` = {$idsModel->id}
                        and `template_field_id` = {$match}
                    ";

                $replace = Yii::$app->db->createCommand($sql)->queryScalar();

                $html = str_replace('{'.$match.'}', $replace, $html);

            }

        }

        //eval($html);

        return $this->renderPartial("print", [
            'html' => $html,
            'template_name_id' => $template_name_id,
        ]);

    }

    public static function replaceHtml($html, Ids &$model, $template_id)
    {

        $ret = [];

        $xs = Ids::find()
            ->andWhere(['template_name_id' => $template_id])
            ->joinWith('childsId')
            ->andWhere([IdsTree::tableName().'.parent_ids_id' => $model->id])
            ->all();

        foreach ($xs as $x){

            $replaced = $html;

            preg_match_all('#{.*(.*)}#U', $replaced, $matches);
            foreach($matches[1] as $match){

                $fieldModel = TemplateField::findOne($match);

                $sql = "
                        select value 
                        from `ids_value_{$fieldModel->value_field}`
                        where 
                            `ids_id` = {$x->id}
                        and `template_field_id` = {$match}
                    ";


                $value = Yii::$app->db->createCommand($sql)->queryScalar();
                if($fieldModel->sourceTemplateName){
                    $value = Ids::findOne($value)->name;
                }

                $replaced = str_replace('{'.$match.'}',
                    $value,
                    $replaced);

            }

            $ret [] = $replaced;

        }

        $ret = implode($ret);

        return $ret;

    }

}