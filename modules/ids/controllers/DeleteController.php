<?php

namespace app\modules\ids\controllers;

use app\modules\ids\models\Ids;
use app\modules\ids\models\IdsSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for Ids model.
 */
class DeleteController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@']
                        ],
                    ],
                ],
            ]


        );
    }

    /**
     * Deletes an existing Ids model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionIndex($id)
    {

        $model = $this->findModel($id);

        if($model->markdel_by){

            $model->markdel_by = null;
            $model->markdel_at = null;

        } else {

            //$model->markdel_by = Yii::$app->user->identity->id;
            $model->markdel_by = 1;
            $model->markdel_at = date('Y-m-d H:i:s');

        }

        $model->save();

        if(Yii::$app->request->get('returnto')){
            return $this->redirect(Yii::$app->request->get('returnto'));
        }

        return $this->redirect(['/ids', 'template_id' => $model->template_name_id]);

    }

    /**
     * Finds the Ids model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Ids the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ids::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
