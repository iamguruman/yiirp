<?php

namespace app\modules\ids\controllers;

use app\modules\ids\models\Ids;
use app\modules\ids\models\IdsSearch;
use app\modules\ids_template\models\TemplateField;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for Ids model.
 */
class UpdateController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Updates an existing Ids model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionIndex($id)
    {
        $model = $this->findModel($id);

        $model->updated_at = aDateNow();

        if ($this->request->isPost && $model->load($this->request->post())) {

            $model->updated_at = aDateNow();
            $model->updated_by = aUserMyId();

            if ($model->save()) {

                CreateController::updateValues($model);
                CreateController::setNameFromNameTemplate($model);

                return $this->redirect(['/ids/view', 'id' => $model->id]);
            }
        }

        if($templateName = $model->templateName){
            /** @var TemplateField $templateField */
            foreach ($templateName->templateFields as $templateField){
                $fields [] = [
                    'label' => "{$templateField->title} ($templateField->id)",
                    'value' => $templateField->getFieldValue($model->id, $templateField->id)
                ];
            }
        }

        return $this->render('update', [
            'fields' => $fields,
            'model' => $model,
        ]);
    }

    /**
     * Finds the Ids model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Ids the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ids::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
