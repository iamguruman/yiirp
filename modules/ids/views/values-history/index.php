<?php

use app\modules\ids\models\IdsValuesHistory;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this */
/** @var app\modules\ids\models\IdsValuesHistorySearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

if(aIfModuleControllerAction("ids", "values-history", "index")){
    $this->title = Yii::t('app', 'Ids Values Histories');
    $this->params['breadcrumbs'][] = $this->title;
}
?>
<div class="ids-values-history-index">

    <?php if(aIfModuleControllerAction("ids", "values-history", "index")): ?>
        <h1><?= Html::encode($this->title) ?></h1>
    <?php endif; ?>

    <p>
        <?php /*= Html::a(Yii::t('app', 'Create Ids Values History'), ['create'], ['class' => 'btn btn-success']) */?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'created_at',
            'created_by',

            aIfModuleControllerAction("ids", "view", "index") ? ['visible' => false] : [
                'attribute' => 'idsId.name'
            ],

            aIfModuleControllerAction("ids", "view", "index") ? ['visible' => false] : [
                'attribute' => 'idsTemplateName.name'
            ],

            'idsTemplateField.title:raw:Template Field',
            'value',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, IdsValuesHistory $model, $key, $index, $column) {
                    return Url::toRoute(['/ids/values-history/'.$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
