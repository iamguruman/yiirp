<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\ids\models\IdsValuesHistory $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ids-values-history-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <?= $form->field($model, 'markdel_at')->textInput() ?>

    <?= $form->field($model, 'markdel_by')->textInput() ?>

    <?= $form->field($model, 'ids_id')->textInput() ?>

    <?= $form->field($model, 'ids_template_name_id')->textInput() ?>

    <?= $form->field($model, 'ids_template_field_id')->textInput() ?>

    <?= $form->field($model, 'value')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
