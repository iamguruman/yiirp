<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\modules\ids\models\IdsValuesHistory $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ids Values Histories'), 'url' => ['index']];

if($model->idsId){
    $this->params['breadcrumbs'][] = ['label' => $model->idsId->name,
        'url' => ['/ids/view', 'id' => $model->idsId->id]];
}

$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
<div class="ids-values-history-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'),
            ['update', 'id' => $model->id],
            ['class' => 'btn btn-primary']) ?>


        <?= Html::a(Yii::t('app', 'Restore value'),
            ['restore-value', 'id' => $model->id],
            ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ids_id',
            'idsId.name',
            'idsTemplateName.name',
            'idsTemplateField.title',
            'value',
            'action_type',
        ],
    ]) ?>

</div>
