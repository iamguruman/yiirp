<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\ids\models\IdsValuesHistory $model */

$this->title = Yii::t('app', 'Create Ids Values History');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ids Values Histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ids-values-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
