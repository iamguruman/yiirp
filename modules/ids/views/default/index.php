<?php

use app\modules\ids\models\Ids;
use app\modules\ids_template\models\IdsTemplateUserAccess;
use app\modules\ids_template\models\TemplateName;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this */
/** @var app\modules\ids\models\IdsSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

/*$columns = [
    'class' => ActionColumn::className(),
    'urlCreator' => function ($action, Ids $model, $key, $index, $column) {
        return Url::toRoute(['/ids/'.$action, 'id' => $model->id]);
    }
];*/

\app\assets\JsTreeAsset::register($this);

$template = TemplateName::findOne(Yii::$app->request->get('template_id'));
if($template){

    $this->params['breadcrumbs'][] = ['label' => 'Ids', 'url' => '/ids'];

    $this->title = Yii::t('app', $template->name);
} else {
    $this->title = Yii::t('app', 'Ids');
}

$this->params['breadcrumbs'][] = $this->title;

$icon = "";
if($template){
    $xIcon = Yii::getAlias("@app/web/tplicons/{$template->id}.png");
    if(file_exists($xIcon)){
        $icon = Html::a("<img src='/tplicons/{$template->id}.png' style='max-height: 50px;'>",
            ['/ids', 'template_id' =>$template->id],
            ['data-pjax' => 0]);
    }
}

if($template){
    $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => Yii::getAlias("@web/tplicons/{$template->id}.png")]);
}

?>

<style type="text/css">
    <?php if($template): ?>
        <?php foreach ($template->statusNames as $statusName): ?>
            <?php if ($statusName->style): ?>
                .status<?= $statusName->id ?> td {
                    <?= $statusName->style ?>
                }
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
</style>

<div class="ids-index">

    <h1><?= $icon ?> <?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Ids'),
            ['/ids/create', 'template_id' => Yii::$app->request->get("template_id")],
            ['class' => 'btn btn-success']) ?>

        <?php if(Yii::$app->request->get('template_id')): ?>
            <?= Html::a("Cancel Template",
                ['/ids'],
                ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>

        <?php if($template): ?>
            <?= Html::a("Template Options",
                ['/ids_template/name/view', 'id' => $template->id],
                ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>

        <?php if($template): ?>
            <?= Html::a("New view",
                ['/ids_crud_index',
                    'template_id' => aGet('template_id'),
                    'status' => aGet('status')
                ],
                ['class' => 'btn btn-warning']) ?>

            <?= Html::a(Yii::t('app', 'Kanban table'),
                ['/ids_kanban',
                    'template_id' => aGet('template_id'),
                    'view' => 'table'
                ],
                ['class' => 'btn btn-warning', 'target' => '_blank']) ?>

            <?= Html::a(Yii::t('app', 'Kanban tree'),
                ['/ids_kanban',
                    'template_id' => aGet('template_id'),
                    'view' => 'tree'
                ],
                ['class' => 'btn btn-warning', 'target' => '_blank']) ?>
        <?php endif; ?>

    </p>

    <p>
        <?php $temps = TemplateName::find()
            ->joinWith('usersAccess')
            ->andWhere([IdsTemplateUserAccess::tableName().".sys_user_id" => Yii::$app->user->id])
            ->all();
        ?>

        <?php if(count($temps) > 0): ?>
            <?= Html::a("Все",
                ['/ids'],
                ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>

        <?php foreach ($temps as $temp): ?>
            <?php if(file_exists(Yii::getAlias("@app")."/web/tplicons/{$temp->id}.png"))
            { $icon = "<img src='/tplicons/{$temp->id}.png' height='20'>"; } else { $icon = ""; } ?>

            <?= Html::a("{$icon} {$temp->name}",
                ['/ids', 'template_id' => $temp->id],
                ['class' =>
                    aGet('template_id') == $temp->id
                        ? 'btn btn-warning' : 'btn btn-primary']) ?>
        <?php endforeach; ?>
    </p>

    <p>
        <?php if($template): ?>

            <?php if(count($template->statusNames) > 0): ?>
                <?= Html::a("Все статусы",
                    ['/ids',
                        'template_id' => $template->id,
                        'status' => null],
                    ['class' => aGet('status') ? 'btn btn-primary' : 'btn btn-warning'])?>

                <?php $ids_count = Ids::find()
                    ->andWhere(['ids_template_status_name_id' => null])
                    ->andWhere(['template_name_id' => $template->id])
                    ->count(); ?>

                <?= Html::a("Без статуса ({$ids_count})",
                    ['/ids',
                        'template_id' => $template->id,
                        'status' => "null"],
                    ['class' => (aGet('status') == null) ? 'btn btn-primary' : 'btn btn-warning'])?>

            <?php endif; ?>

            <?php foreach ($template->statusNames as $statusName): ?>
                <?= Html::a($statusName->name,
                    ['/ids',
                        'template_id' => $template->id,
                        'status' => $statusName->id],
                    ['class' => aGet('status') == $statusName->id
                        ? 'btn btn-warning' : 'btn btn-primary',
                        'style' => $statusName->style ?? null])?>
            <?php endforeach; ?>
        <?php endif; ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function(Ids $model, $key, $index, $grid){
            if($model->idsTemplateStatusName && $model->idsTemplateStatusName->style){
                return ['class' => "status{$model->idsTemplateStatusName->id}",
                    'style' => $model->idsTemplateStatusName->style];

                //return ['class' => "status{$model->idsTemplateStatusName->id}"];
                //return ['class' => 'all-unset', ];
            }
        },
        'columns' => $columns
    ]); ?>

    <?php Pjax::end(); ?>

</div>
