<?php

use app\modules\ids\models\IdsValue;
use app\modules\ids_template\models\IdsTemplateLinks;
use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\modules\ids\models\Ids $model */

/** @var array $attributes */

/** @var \app\modules\ids_flow\models\IdsFlowSearch $flowSearchModel */
/** @var \yii\data\ActiveDataProvider $flowDataProvider */

/** @var \app\modules\ids_tree\models\IdsTreeSearch $parentSearchModel */
/** @var \yii\data\ActiveDataProvider $parentDataProvider */

/** @var \app\modules\ids_tree\models\IdsTreeSearch $childSearchModel */
/** @var \yii\data\ActiveDataProvider $childDataProvider */

/** @var \app\modules\ids\models\IdsValuesHistory $historySearchModel */
/** @var \yii\data\ActiveDataProvider $historyDataProvider */

/** @var \app\modules\ids_template\models\IdsPrintTemplateSearch $printSearchModel */
/** @var \yii\data\ActiveDataProvider $printDataProvider */

$this->title = "#{$model->id}: {$model->name}";
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ids'), 'url' => ['/ids']];

if($model->templateName){
    $this->params['breadcrumbs'][] = [
        'label' => Yii::t('app', $model->templateName->name),
        'url' => ['/ids', 'template_id' => $model->template_name_id]];
}

if(count($model->parentsId) == 1){
    $this->params['breadcrumbs'][] = [
            'label' => $model->parentsId[0]->parentId->name,
            'url' => ['/ids/view', 'id' => $model->parentsId[0]->parentId->id]
        ];
} elseif(count($model->parentsId) > 1) {
    $this->params['breadcrumbs'][] = [
        'label' => "Настроить выдачу родителей",
        'url' => ['/ids', 'parent_id' => $model->id]
    ];
}

$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$icon = Yii::getAlias("@app/web/tplicons/{$model->template_name_id}.png");
if(file_exists($icon)){
    $icon = Html::a("<img src='/tplicons/{$model->template_name_id}.png' style='max-height: 50px;'>",
        ['/ids', 'template_id' =>$model->template_name_id],
        ['data-pjax' => 0]);
} else {
    $icon = "";
}

if($model && $model->template_name_id){
    $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => Yii::getAlias("@web/tplicons/{$model->template_name_id}.png")]);
}

?>
<div class="ids-view">

    <?php if($model->idsTemplateStatusName && $model->idsTemplateStatusName->style) {
        $statusStyle = $model->idsTemplateStatusName->style;
    } else {
        $statusStyle = null;
    }?>
    <h1 style="<?= $statusStyle ?>; padding: 0px;">
        <?php if($model->markdelBy): ?>
            <img src="/media/icon_trash_red.png" height="30" title="Deleted by <?= $model->markdelBy->username ?> at <?= $model->markdel_at ?>">
        <?php endif; ?>
        <?= $icon ?>
        <?= Html::encode($this->title) ?>
    </h1>

    <p>
        <?php if($model->markdel_by): ?>
            <?= Html::a(Yii::t('app', 'Restore'),
                ['/ids/delete', 'id' => $model->id,
                    'returnto' => "/ids/view?id={$model->id}"],
                [
                    'class' => 'btn btn-success',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to restore this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
        <?php else: ?>
            <?= Html::a(Yii::t('app', 'Update'),
                ['/ids/update', 'id' => $model->id],
                ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>

        <?= Html::a("<img src='/media/icon_yandex_disk.png' height='15'> YDisk",
            "https://disk.yandex.ru/client/disk/yiirp/{$model->id}",
            ['class' => 'btn btn-primary', 'target' => '_blank']) ?>

        <?= Html::a(Yii::t('app', 'Copy'),
            ['/ids/create', 'template_id' => $model->template_name_id, 'copy_id' => $model->id],
            ['class' => 'btn btn-primary']) ?>

        <?php if($model->templateName): ?>
            <?= Html::a(Yii::t('app', "Create new {$model->templateName->name}"),
                ['/ids/create', 'template_id' => $model->template_name_id],
                ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>

        <?php if($model->previous): ?>
            <?= Html::a("<<", ['/ids/view', 'id' => $model->previous->id], ['class' => 'btn btn-primary'])?>
        <?php endif; ?>

        <?php if($model->next): ?>
            <?= Html::a(">>", ['/ids/view', 'id' => $model->next->id], ['class' => 'btn btn-primary'])?>
        <?php endif; ?>


        <?php /*= Html::a(Yii::t('app', 'Create new '.$model->templateName->name),
            ['/ids/create', 'template_id' => $model->template_name_id],
            ['class' => 'btn btn-success']) */?>

        <?php if($model->template_name_id && $model->templateName
            && $model->templateName->owner_user_id == aUserMyId()): ?>
            <?= Html::a("Template Options",
                ['/ids_template/name/view', 'id' => $model->template_name_id],
                ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>

        <?php foreach (IdsTemplateLinks::find()
            ->andWhere(['ids_template_name_id' => $model->template_name_id])
            ->all()
        as $link): ?>

            <?php
                $xLink = $link->link;

                $xLink = str_replace('{id}', aGet('id'), $xLink);

                preg_match_all('#{.*(.*)}#U', $link->link,$matches);
                foreach ($matches[1] as $match){
                    $xLink = str_replace('{'.$match.'}',
                        IdsValue::getValueByFieldIdAndIdsId($match, $model->id),
                        $xLink
                    );
                }

            ?>

            <?= Html::a($link->text,
                $link->redirect ? ["/redirect", 'url' => $xLink] : $xLink,
                ['class' => 'btn btn-success', 'title' => $link->title, 'target' => $link->target]) ?>
        <?php endforeach; ?>

        <?php if ($model->printTemplates): ?>
            <?php foreach ($model->printTemplates as $printTemplate): ?>
                <?= Html::a("<img src='/media/icon_printer.png' height='21'> {$printTemplate->name}",
                    ['/ids/print', 'id' => $model->id, 'print_template' => $printTemplate->id],
                    ['class' => 'btn btn-primary', 'target' => '_blank']) ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ]) ?>

    <?= \yii\bootstrap5\Tabs::widget(['items' => [
        [
            'label' => 'ID',
            'active' => aGet('tab') == 'id' ? true : null,
            'content' => "<br>".DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'createdBy.fullname',
                    'created_at',
                    'updatedBy.fullname',
                    'updated_at',
                    'markdelBy.fullname',
                    'markdel_at',
                    'owner_sys_user_id',
                    'owner_sys_group_id',
                    'template_name_id',
                ],
            ])
        ],

        [
            'label' => 'Flow',
            'active' => aGet('tab') == 'flow' ? true : null,
            'content' => "<br>".$this->render("@app/modules/ids_flow/views/flow/index.php",[
                    'searchModel' => $flowSearchModel,
                    'dataProvider' => $flowDataProvider
                ])
        ],

        [
            'label' => "Child ({$parentDataProvider->totalCount})",
            'active' => aGet('tab') == 'child' ? true : (empty(aGet('tab')) ? true : null),
            'content' => "<br>".$this->render("@app/modules/ids_tree/views/tree-manager/index2.php",[
                    'searchModel' => $parentSearchModel,
                    'dataProvider' => $parentDataProvider,
                    'parent_hide' => true,
                    'id_hide' => true,
                ])
        ],

        [
            'label' => "Parent ({$childDataProvider->totalCount})",
            'active' => aGet('tab') == 'parent' ? true : null,
            'content' => "<br>".$this->render("@app/modules/ids_tree/views/tree-manager/index2.php",[
                    'searchModel' => $childSearchModel,
                    'dataProvider' => $childDataProvider,
                    'child_hide' => true,
                    'id_hide' => true,
                ])
        ],

        [
            'label' => "History ({$historyDataProvider->totalCount})",
            'active' => aGet('tab') == 'history' ? true : null,
            'content' => "<br>".$this->render("../values-history/index.php",[
                    'searchModel' => $historySearchModel,
                    'dataProvider' => $historyDataProvider,
                ])
        ],

        [
            'label' => "Print ({$printDataProvider->totalCount})",
            'active' => aGet('tab') == 'print' ? true : null,
            'content' => "<br>".$this->render("@app/modules/ids_template/views/print-template/index.php",[
                    'searchModel' => $printSearchModel,
                    'dataProvider' => $printDataProvider,
                ])
        ],

        [
            'label' => "Files (??)",
            'active' => aGet('tab') == 'files' ? true : null,
            'content' => "<br>"."тут будут файлы"
        ],

        [
            'label' => "Tree",
            'active' => aGet('tab') == 'tree' ? true : null,
           // 'content' => "<br>".$this->render("tree", [])
            //            'content' => "<br>"."тут будет дерево"
            'content' => "<br>".$this->render('@app/modules/ids_tree_fancy/views/default/fancy-tree-for-ids-view')
        ],


    ]]) ?>

</div>
