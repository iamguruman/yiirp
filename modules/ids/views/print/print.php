<?php

/** @var \yii\web\View $this */
/** @var string $html */
/** @var integer $template_name_id */

?>

<style type="text/css">
    @media print {
        .pageBreakAfter {
            page-break-after: always;
        }

        .pageBreakAfterHide {
            display: none;
        }
    }
</style>
<script>
    function printthis(){
        window.print();
        window.close();
    }
</script>
<div id="printbtn" class="pageBreakAfterHide" style="border-bottom: 1px solid red; padding: 5px;">
    <input type="button" value="Print now" onclick="printthis();"/>
    <input type="button" value="Open Id=<?= aGet('id') ?>" onclick="window.location.href='/ids/view/?id=<?= aGet('id') ?>'"/>
    <input type="button" value="Edit template" onclick="window.location.href='/ids_template/print-template/update/?id=<?= aGet('print_template') ?>&returnto=<?= urlencode($_SERVER['REQUEST_URI']) ?>'"/>
</div>

<?= eval("?>{$html}") ?>