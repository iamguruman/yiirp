<?php

use app\modules\ids\models\Ids;
use app\modules\ids_flow\models\IdsFlow;
use app\modules\ids_flow\models\IdsFlowFieldConnection;
use app\modules\ids_template\models\IdsTemplateStatusName;
use app\modules\ids_template\models\IdsTemplateUserAccess;
use app\modules\ids_template\models\TemplateField;
use app\modules\ids_template\models\TemplateName;
use app\modules\teams\models\Team;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\ids\models\Ids $model */
/** @var yii\widgets\ActiveForm $form */

$onload = [];
?>

<p class="ids-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php /*= $form->field($model, 'owner_sys_user_id')->textInput() */?>
    <?php /*= $form->field($model, 'owner_sys_group_id')->textInput() */?>

    <?php if($parentModel = Ids::findOne(aGet('parent_id'))): ?>
        <?php /** @var $parentId \app\modules\ids_tree\models\IdsTree */ ?>
        <?php foreach ($parentModel->parentsId as $parentId): ?>

            <?= $form->field($model, "extra_parents_id[{$parentId->parent_ids_id}]",[
                'template' => "<div class=\"check\">{input}&nbsp;{label}</div>\n<div class=\"col-lg-8\">{error}</div>"
            ])
                ->checkbox([], false)
                ->label($parentId->parentId->name) ?>

        <?php endforeach; ?>

        <br>

    <?php endif; ?>

    <?= $form->field($model, 'template_name_id')->widget(Select2::className(), [
            'data' => TemplateName::find()
                ->select(['name', TemplateName::tableName().'.id'])
                ->indexBy(TemplateName::tableName().'.id')
                ->joinWith('usersAccess')
                ->andWhere([IdsTemplateUserAccess::tableName().'.sys_user_id' => aUserMyId()])
                ->column(),
            'options' => [
                    'placeholder' => 'Select a template ...',
                    'onchange' => "changeTemplate();"
                ],
            'pluginOptions' => [
                'allowClear' => true
            ],
    ]) ?>

    <br>

    <?php if(IdsTemplateStatusName::find()->andWhere(['ids_template_name_id' => $model->template_name_id])->count() > 0): ?>
        <?= $form->field($model, 'ids_template_status_name_id')->widget(Select2::className(), [
            'data' => IdsTemplateStatusName::find()
                ->select(['name', 'id'])
                ->andWhere(['ids_template_name_id' => $model->template_name_id])
                ->indexBy('id')
                ->orderBy(['sort' => SORT_ASC])
                ->column(),
            'options' => ['placeholder' => 'Select a template ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>

        <br>

    <?php endif; ?>

    <?php if($model->templateName && $model->templateName->hint): ?>
        <p><?= str_replace("\n", "<br>", $model->templateName->hint) ?></p>
    <?php endif; ?>

    <?php if($model->template_name_id): ?>

        <?php $fields = TemplateField::find()
            ->andWhere(['template_name_id' => $model->template_name_id])
            ->orderBy(['sort' => SORT_ASC])
            ->all(); ?>

        <?php $i = 0; ?>
        <?php /** @var $field TemplateField */ ?>
        <?php foreach ($fields as $field): ?>

            <?php
                $onchange = null;
                if($field->onchange){
                    $onchange = $field->onchange;
                    $onchangeMatches = null;
                    preg_match_all('#{.*(.*)}#U', $field->onchange, $onchangeMatches);
                    foreach ($onchangeMatches[1] as $onchangeMatch){
                        $onchange = str_replace('{'.$onchangeMatch.'}', "document.getElementById('ids-fieldvalue-{$onchangeMatch}')", $onchange);
                    }
                }

                $onload[$field->id] = null;
                if($field->onload){
                    $onload[$field->id] = $field->onload;
                    $onloadMatches = null;
                    preg_match_all('#{.*(.*)}#U', $field->onload, $onloadMatches);
                    foreach ($onloadMatches[1] as $onloadMatch){
                        $onload[$field->id] = str_replace('{'.$onloadMatch.'}', "document.getElementById('ids-fieldvalue-{$onloadMatch}')", $onload[$field->id]);
                    }
                }
            ?>

            <?php if($i > 0): ?>
                <br>
            <?php endif; ?>

            <?php $fieldName = "{$field->template_name_id}_{$field->value_field}"; ?>
            <?php $fieldName = $field->id; ?>

            <?php // заполняю значения полей ?>
            <?php if(aIfModuleControllerAction("ids",'create', 'index')): ?>
                <?php // при создании беру значения по умолчанию ?>

                <?php if($field->default_value) {
                    $model->fieldValue[$fieldName] = $field->default_value;
                } else {
                    $model->fieldValue[$fieldName] = null;
                } ?>

                <?php


                ?>
                <?php //заполняю значение из родительского потока
                    if(aGet('parent_id') && $idsFlow = IdsFlow::findOne(aGet('flow_id'))){

                        if($pid = IdsFlowFieldConnection::find()
                            ->select(['parent_ids_template_field_id'])
                            ->andWhere(['ids_flow_id' => $idsFlow->id])
                            ->andWhere(['child_ids_template_field_id' => $field->id])
                        ->column()){

                            if(count($pid) > 0){

                                $model->fieldValue[$fieldName] =
                                    Ids::staticGetValueField(
                                        aGet('parent_id'),
                                        $pid[0]
                                    );

                            }


                        }


                } ?>

            <?php elseif(aIfModuleControllerAction("ids",'update', 'index')): ?>

                <?php // при редактировании заполняю значения из таблиц ids_value_XXX ?>

                <?php $sql = "
                    select value 
                    from `ids_value_{$field->value_field}` 
                    where 
                        `ids_id` = '{$model->id}' 
                      and `template_field_id` = '{$field->id}' 
                    order by created_at ASC 
                    limit 1
                "; ?>
                <?php $model->fieldValue[$fieldName] = Yii::$app->db->createCommand($sql)->queryScalar(); ?>
            <?php endif; ?>

            <?php if($html_element = $field->html_element): ?>

                <?php if($html_element == 'date'):?>

                    <?= $form->field($model, "fieldValue[{$fieldName}]")
                        ->textInput(['type' => 'date'])
                        ->label($field->title)
                        ->hint($field->hint)
                    ?>

                <?php elseif($html_element == 'time'):?>

                    <?= $form->field($model, "fieldValue[{$fieldName}]")
                        ->textInput(['type' => 'time'])
                        ->label($field->title)
                        ->hint($field->hint)
                    ?>

                <?php elseif($html_element == 'ckeditor_4_13_1'):?>

                    <?= $form->field($model, "fieldValue[{$fieldName}]")->textarea() ?>
                    <?php
                        $this->registerJsFile('/vendor/ckeditor_4_13_1/ckeditor.js');
                        $this->registerJsFile('/vendor/ckeditor_4_13_1/style.js');
                        $this->registerJs(<<<JS
                            var ckeditor1 = CKEDITOR.replace("Ids[fieldValue][{$fieldName}]");
                        JS);
                    ?>

                <?php elseif($html_element == 'textarea'):?>

                    <?= $form->field($model, "fieldValue[{$fieldName}]")
                        ->textarea(['rows' => 7])
                        ->label($field->title)
                        ->hint($field->hint)
                    ?>

                <?php elseif($html_element == 'select2'):?>

                    <?php if($field->source_template_name_id){

                    } ?>
                    <?= $form->field($model, "fieldValue[{$fieldName}]")
                        ->widget(Select2::className(), [
                            'data' => $field->source_template_name_id
                                ? Ids::find()->select(['name', 'id'])->indexBy("id")->andWhere(['template_name_id' => $field->source_template_name_id])->column()
                                : null,
                            'options' => ['placeholder' => 'Select item'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label($field->title)
                        ->hint($field->hint)
                    ?>

                <?php else:?>

                    <?= $form->field($model, "fieldValue[{$fieldName}]")
                        ->$html_element()
                        ->label($field->title)
                        ->hint($field->hintWithHintInsertion)
                    ?>

                <?php endif;?>


            <?php else: ?>

                <?= $form->field($model, "fieldValue[{$fieldName}]")
                    ->textInput(['onchange' => $onchange])
                    ->label($field->title)
                    ->hint($field->hintWithHintInsertion)
                ?>

            <?php endif; ?>


            <?php $i++; ?>
        <?php endforeach; ?>

    <?php endif; ?>

    <br>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    function changeTemplate(){
        let template = document.getElementById("ids-template_name_id").value;
        window.location.href = "/ids/create?template_id=" + template;
    }

    function idsFieldSetValue(fieldId, setValue){
        let fieldElement = document.getElementById(fieldId);
        fieldElement.value = setValue;
    }

    function idsFieldInsertValue(fieldId, insertValue){
        let fieldElement = document.getElementById(fieldId);

        if(fieldElement.value.length > 0){
            fieldElement.value += " ";
        }

        fieldElement.value += insertValue;
    }
</script>


<script>
    window.onload=function(){ <?= implode($onload) ?> };
</script>