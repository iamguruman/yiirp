<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\ids\models\Ids $model */

$this->title = "ID {$model->id}: {$model->name}";

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ids'), 'url' => ['/ids']];

if($model->templateName){
    $this->params['breadcrumbs'][] = [
        'label' => Yii::t('app', $model->templateName->name),
        'url' => ['/ids', 'template_id' => $model->template_name_id]];
}

$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['/ids/view', 'id' => $model->id]];

$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

?>
<div class="ids-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if($model->markdel_by): ?>
            <?= Html::a(Yii::t('app', 'Restore'),
                ['/ids/delete', 'id' => $model->id,
                    'returnto' => "/ids/view?id={$model->id}"
                ],
                [
                    'class' => 'btn btn-success',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to restore this item?'),
                        'method' => 'post',
                ],
            ]) ?>
        <?php else: ?>
            <?= Html::a(Yii::t('app', 'Delete'),
                ['/ids/delete', 'id' => $model->id,
                    'returnto' => "/ids/view?id={$model->id}"
                ],
                [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>

        <?= Html::a("Change template",
            ['/ids/change-template', 'id' => $model->id],
            ['class' => 'btn btn-primary']) ?>

        <?php if($model->template_name_id && $model->templateName
            && $model->templateName->owner_user_id == aUserMyId()): ?>
            <?= Html::a("Template Options",
                ['/ids_template/name/view', 'id' => $model->template_name_id],
                ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
    </p>

    <?= $this->render('../_form', [
        'model' => $model,
    ]) ?>

</div>
