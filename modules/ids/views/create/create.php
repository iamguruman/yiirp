<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\ids\models\Ids $model */

$this->title = Yii::t('app', 'Create Ids');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ids'), 'url' => ['/ids']];

if($model->templateName){
    $this->params['breadcrumbs'][] = [
        'label' => Yii::t('app', $model->templateName->name),
        'url' => ['/ids', 'template_id' => $model->template_name_id]];
}

if($parentIdModel = \app\modules\ids\models\Ids::findOne(aGet('parent_id'))){
    $this->params['breadcrumbs'][] = [
        'label' => $parentIdModel->name,
        'url' => ['/ids/view', 'id' => $parentIdModel->id]
    ];
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ids-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if($model->template_name_id): ?>
            <?= Html::a("Template Option",
                ['/ids_template/name/view', 'id' => $model->template_name_id],
                ['class' => 'btn btn-primary'])?>
        <?php endif; ?>
    </p>

    <?= $this->render('../_form', [
        'model' => $model,
    ]) ?>

</div>
