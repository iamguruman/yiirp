<?php

namespace app\modules\ids\models;

/**
 * This is the ActiveQuery class for [[Ids]].
 *
 * @see Ids
 */
class IdsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ids[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ids|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
