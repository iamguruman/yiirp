<?php

namespace app\modules\ids\models;

use app\modules\ids_template\models\IdsPrintTemplate;
use app\modules\ids_template\models\IdsTemplateStatusName;
use app\modules\ids_template\models\IdsTemplateUserAccess;
use app\modules\ids_template\models\TemplateField;
use app\modules\ids_template\models\TemplateName;
use app\modules\ids_tree\models\IdsTree;
use app\modules\user\models\User;
use Yii;

/**
 * This is the model class for table "ids".
 *
 * @property int $id
 *
 * @property-read User|null $createdBy
 * @property int|null $created_by
 *
 * @property string|null $created_at
 *
 * @property int|null $updated_by
 * @property-read User|null $updatedBy
 *
 * @property string|null $updated_at
 *
 * @property int|null $markdel_by
 * @property-read User|null $markdelBy
 *
 * @property string|null $markdel_at
 * @property-read string|null $markdelAtFormatted
 *
 * @property int $owner_sys_user_id
 * @property int $owner_sys_group_id
 *
 * @property int|null $template_name_id
 * @property-read  TemplateName|null $templateName
 *
 * @property string|null $name
 *
 * @property-read  IdsTree|null $parentsId
 *
 * @property-read  IdsTree|null $childsId
 *
 * @property-read  Ids|null $previous
 * @property-read  Ids|null $next
 *
 * @property  integer|null $ids_template_status_name_id
 * @property-read IdsTemplateStatusName|null $idsTemplateStatusName
 *
 * @property-read IdsPrintTemplate[] $printTemplates
 *
 */
class Ids extends \yii\db\ActiveRecord
{

    public $fieldValue = [];
    public $parent_id;
    public $extra_parents_id = [];
    public $child_id;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ids';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_by', 'updated_by', 'markdel_by',
                'owner_sys_user_id', 'owner_sys_group_id', 'template_name_id'], 'integer'],

            [['ids_template_status_name_id'], 'integer'],

            [['created_at', 'updated_at', 'markdel_at'], 'string'],

            //[['owner_sys_user_id', 'owner_sys_group_id'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['fieldValue'], 'safe'],

            [['extra_parents_id'], 'safe'],
            [['parent_id'], 'integer'],
            [['child_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ids_template_status_name_id' => Yii::t('app', 'Status Name'),
            'id' => Yii::t('app', 'ID'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'markdel_by' => Yii::t('app', 'Markdel By'),
            'markdel_at' => Yii::t('app', 'Markdel At'),
            'owner_sys_user_id' => Yii::t('app', 'Owner Sys User ID'),
            'owner_sys_group_id' => Yii::t('app', 'Owner Sys Group ID'),
            'template_name_id' => Yii::t('app', 'Template Name ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    public function getTemplateName(){
        return $this->hasOne(TemplateName::className(), ['id' => 'template_name_id']);
    }

    public function getParentsId(){
        return $this->hasMany(IdsTree::className(), ['child_ids_id' => 'id']);
    }

    public function getChildsId(){
        return $this->hasMany(IdsTree::className(), ['child_ids_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return IdsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IdsQuery(get_called_class());
    }

    public static function staticGetValueField($ids_id, $field_id){

        $fieldModel = TemplateField::findOne($field_id);

        $sql = "
                        select value 
                        from `ids_value_{$fieldModel->value_field}`
                        where 
                            `ids_id` = {$ids_id}
                        and `template_field_id` = {$field_id}
                    ";

        return Yii::$app->db->createCommand($sql)->queryScalar();

    }

    public function getFieldValue($field_id){
        return self::staticGetValueField($this->id, $field_id);
    }

    public function getmarkdelAtFormatted($format = "H:m d/m/Y"){
        if($this->markdel_at){
            return date($format, strtotime($this->markdel_at));
        } else {
            return;
        }
    }

    public function getMarkdelBy(){
        return $this->hasOne(User::className(), ['id' => 'markdel_by']);
    }

    public function getPrevious(){
        return Ids::find()
            ->andWhere(['template_name_id' => $this->template_name_id])
            ->andWhere(['<', 'id', $this->id])
            ->one();
    }

    public function getNext(){
        return Ids::find()
            ->andWhere(['template_name_id' => $this->template_name_id])
            ->andWhere(['>', 'id', $this->id])
            ->one();
    }

    public function getIdsTemplateStatusName(){
        return $this->hasOne(IdsTemplateStatusName::className(), ['id' => 'ids_template_status_name_id']);
    }

    public function getCreatedBy(){
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy(){
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function getPrintTemplates(){
        return $this->hasMany(IdsPrintTemplate::className(),
            ['ids_template_name_id' => 'template_name_id']);
    }

}
