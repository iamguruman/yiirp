<?php

namespace app\modules\ids\models;

use app\modules\ids_template\models\IdsTemplateUserAccess;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\ids\models\Ids;

/**
 * IdsSearch represents the model behind the search form of `app\modules\ids\models\Ids`.
 */
class IdsSearch extends Ids
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by', 'markdel_by', 'owner_sys_user_id',
                'owner_sys_group_id', 'template_name_id'], 'integer'],
            [['created_at', 'updated_at', 'markdel_at', 'name'], 'safe'],

            [['ids_template_status_name_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $params2 = [])
    {
        $query = Ids::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'markdel_by' => $this->markdel_by,
            'markdel_at' => $this->markdel_at,
            'owner_sys_user_id' => $this->owner_sys_user_id,
            'owner_sys_group_id' => $this->owner_sys_group_id,
            'template_name_id' => $this->template_name_id,
        ]);

        $query->andFilterWhere(['like', Ids::tableName().'.name', $this->name]);

        if(!empty($params2['template_id'])){
            $query->andWhere(['template_name_id' => $params2['template_id']]);
        }

        if(!empty($params2['search'])){
            $query->andWhere(['or',
                ['like', Ids::tableName().'.id', $params2['search']],
                ['like', Ids::tableName().'.name', $params2['search']],
            ]);
        }

        if(!empty($params2['user_can_list'])){
            $query->joinWith('templateName.usersAccess')
                ->andWhere([IdsTemplateUserAccess::tableName().'.sys_user_id' => Yii::$app->user->id])
                ->andWhere([IdsTemplateUserAccess::tableName().'.can_list' => 1]);
        }

        if(!empty($params2['ids_template_status_name_id'])){
            if($params2['ids_template_status_name_id'] == 'null'){
                $query->andWhere(['ids_template_status_name_id' => null]);
            } else {
                $query->andWhere(['ids_template_status_name_id' => $params2['ids_template_status_name_id']]);
            }
        }

        if(!empty($params2['hide_markdel'])){
            $query->andWhere(['is', Ids::tableName().'.markdel_by', null]);
        } else {
            //$query->andWhere(['is not', Ids::tableName().'.markdel_by', null]);
        }

        return $dataProvider;
    }
}
