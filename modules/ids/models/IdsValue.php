<?php

namespace app\modules\ids\models;

use app\modules\ids_template\models\TemplateField;
use app\modules\ids_template\models\TemplateName;
use app\modules\ids_tree\models\IdsTree;
use Yii;

/**
 * This is the model class for table "ids".
 *
 * @property int $value
 *
 */
class IdsValue
{

    /**
     * {@inheritdoc}
     */
    public static function getValueByFieldIdAndIdsId($field_id, $ids_id)
    {

        if($fieldModel = TemplateField::find()->andWhere(['id' => $field_id])->one()){

            $sql = [];

            $sql [] = "select value ";

            $sql [] = "from `ids_value_{$fieldModel->value_field}` ";

            $sql [] = "where ";

            $sql [] = "`ids_id` = {$ids_id} ";

            $sql [] = "and `template_field_id` = {$field_id} ";

            $sql [] = "order by `created_at` DESC ";

            $sql = implode($sql);

            $ret = Yii::$app->db->createCommand($sql)->queryScalar();

            return $ret;

        }

    }
}
