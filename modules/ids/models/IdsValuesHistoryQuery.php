<?php

namespace app\modules\ids\models;

/**
 * This is the ActiveQuery class for [[IdsValuesHistory]].
 *
 * @see IdsValuesHistory
 */
class IdsValuesHistoryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return IdsValuesHistory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return IdsValuesHistory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
