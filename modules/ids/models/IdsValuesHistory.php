<?php

namespace app\modules\ids\models;

use app\modules\ids_template\models\TemplateField;
use app\modules\ids_template\models\TemplateName;
use Yii;

/**
 * This is the model class for table "ids_values_history".
 *
 * @property int $id
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property string|null $markdel_at
 * @property int|null $markdel_by
 *
 * @property int $ids_id
 * @property-read Ids $idsId
 *
 * @property int $ids_template_name_id
 * @property-read TemplateName $idsTemplateName
 *
 * @property int $ids_template_field_id
 * @property-read TemplateField $idsTemplateField
 *
 * @property resource $value
 *
 * @property string $action_type insert update restore
 *
 * @property integer $restored_id
 */
class IdsValuesHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ids_values_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['restored_id'], 'integer'],
            [['action_type'], 'string', 'max' => 255],
            [['created_by', 'updated_by', 'markdel_by', 'ids_id', 'ids_template_name_id', 'ids_template_field_id'], 'integer'],
            [['ids_id', 'ids_template_name_id', 'ids_template_field_id', 'value'], 'required'],
            [['value'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'restored_id' => Yii::t('app', 'Restored Id'),
            'action_type' => Yii::t('app', 'Action type'),
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'markdel_at' => Yii::t('app', 'Markdel At'),
            'markdel_by' => Yii::t('app', 'Markdel By'),
            'ids_id' => Yii::t('app', 'Ids ID'),
            'ids_template_name_id' => Yii::t('app', 'Ids Template Name ID'),
            'ids_template_field_id' => Yii::t('app', 'Ids Template Field ID'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    public function getIdsId(){
        return $this->hasOne(Ids::className(), ['id' => 'ids_id']);
    }

    public function getIdsTemplateName(){
        return $this->hasOne(TemplateName::className(), ['id' => 'ids_template_name_id']);
    }

    public function getIdsTemplateField(){
        return $this->hasOne(TemplateField::className(), ['id' => 'ids_template_field_id']);
    }

    /**
     * {@inheritdoc}
     * @return IdsValuesHistoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IdsValuesHistoryQuery(get_called_class());
    }
}
