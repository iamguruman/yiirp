<?php

namespace app\modules\pbx_whapi;

use app\components\ModuleAccess;
use yii\helpers\ArrayHelper;

/**
 * pbx module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\pbx_whapi\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {

        //ModuleAccess::check($this->id);

        parent::init();

        // custom initialization code goes here
    }
}
