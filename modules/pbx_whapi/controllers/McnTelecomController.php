<?php


namespace app\modules\pbx_whapi\controllers;

use app\modules\calls\models\Call;
use app\modules\pbx\models\PbxCalls;
use app\modules\pbx\models\MMcnCallsUniqueInternalNumber;
use app\modules\pbx\models\MMcnCallsUniqueInternalNumberQuery;
use app\modules\pbx_list\models\PbxList;
use app\modules\pbx_webhook\models\PbxWebhook;
use app\modules\vatssms\models\PbxSms;
use yii\web\Controller;

class McnTelecomController extends Controller
{

    public function actionGetHook()
    {

        $json = json_encode($_GET, JSON_UNESCAPED_UNICODE);

        $model = new PbxWebhook();

        if($pbxList = PbxList::findOne(['public_key' => aGet('pbx_list_id')])){
            $model->pbx_list_id = $pbxList->id;
        }

        $model->created_at = aDateNow();
        $model->created_by = null;
        $model->call_id = aGet('callId');
        $model->webhook_type_id = aGet('my_type_id');

        if (aGet('internalNumber')) {
            $model->internalNumber = aGet('internalNumber');
        }

        $model->json = $json;
        $model->type = "new";

        if ($model->save()) {

            if (aGet('my_type_id') == 18) {

                $newSmsModel = new PbxSms();
                $newSmsModel->created_at = aDateNow();

                $newSmsModel->datetime = aDateNow();
                $newSmsModel->from = aGet('src_number');
                $newSmsModel->to = aGet('dst_number');
                $newSmsModel->message = aGet('message');

                $newSmsModel->save();

            }

            d($json);
            self::createFromHook(aGet('callId'), $json);

            //MMcnCallsUniqueInternalNumber::createNew($model->call_id, $model->internalNumber);


        }

    }


    public static function createFromHook($call_id, $json){

        $json = json_decode($json);

        if($model = PbxCalls::findOne(['call_id' => $call_id])){

        } else {
            $model = new PbxCalls();
            $model->created_at = aDateNow();
            $model->call_id = $call_id;
        }

        // ватс начало входящего звонка
        if($json->my_type_id == 2) {
            $model->direction = "in";

            if(!empty($json->callId))
                $model->call_id = $json->callId;

            if(!empty($json->timestamp))
                $model->datetime_start = date("Y-m-d H:i:s", $json->timestamp);

            if(!empty($json->did))
                $model->office_tel = $json->did;

            if(!empty($json->callerNumber))
                $model->external_number = $json->callerNumber;

            // начало звонка через очередь
        } elseif($json->my_type_id == 13) {
            $model->direction = "in";

            if(!empty($json->internalNumber))
                $model->internal_number = $json->internalNumber;

            if(!empty($json->callerNumber) && empty($model->external_number))
                $model->external_number = $json->callerNumber;

            if(!empty($json->did) && empty($model->office_tel))
                $model->office_tel = $json->did;

            // ватс начало звонка на внутренний номер
        } elseif($json->my_type_id == 4) {
//            "accountId": "129908",
//            "isIncoming": "true",
//            "externalNumber": "79253733641",
//            "internalNumber": "6234",
//            "to": "6234",
//            "from": "79253733641",
//            "calledNumber": "74951851234",
//            "callerNumber": "79253733641",
//            "timestamp": "1665417844",
//            "did": "74951851234",
//            "callId": "11-1665417843.13505458",
//            "vpbxId": "15823"

            // ответ на звонок
        } elseif($json->my_type_id == 14) {

            if(!empty($json->callerNumber) && empty($model->external_number))
                $model->external_number = $json->callerNumber;

            if(!empty($json->calledNumber) && empty($model->office_tel))
                $model->office_tel = $json->calledNumber;

            if(!empty($json->internalNumber))
                $model->internal_number = $json->internalNumber;

            if(!empty($json->durationWaiting) && empty($model->duration_waiting))
                $model->duration_waiting = $json->durationWaiting;

            // ответ на звонок через очередь
        } elseif($json->my_type_id == 15) {
            $model->answered = 1;
//            "callerNumber": "79253733641",
//            "internalNumber": "6234",
//            "accountId": "129908",
//            "vpbxId": "15823",
//            "callId": "11-1665417843.13505458",
//            "did": "74951851234",
//            "queue": "8006",
//            "durationWaiting": "18",
//            "timestamp": "1665417862",

            // ватс завершение входящего звонка
        } elseif($json->my_type_id == 1) {

            $model->datetime_end = aDateNow();

            if(!empty($json->file) && empty($model->file))
                $model->file = $json->file;

            if(!empty($json->did) && empty($model->office_tel))
                $model->office_tel = $json->did;

            if(!empty($json->billsec) && empty($model->billsec))
                $model->billsec = $json->billsec;

            if(!empty($json->answered) && $json->answered == "1")
                $model->answered = 1;

//            "callerNumber": "79253733641",
//            "did": "74951851234",
//            "accountId": "129908",
//            "vpbxId": "15823",
//            "callId": "11-1665417843.13505458",
//            "answered": "1",
//            "billsec": "16",
//            "file": "20221010-160403-79253733641-74951851234-11166541784313505458",
//            "timestamp": "1665417878",
//            "time": "09:36:57",

            // ватс завершение звонка на внутренний номер
        } elseif($json->my_type_id == 3) {

            if(!empty($json->externalNumber) && empty($model->external_number))
                $model->external_number = $json->externalNumber;

            if(!empty($json->internalNumber) && empty($model->internal_number))
                $model->internal_number = $json->internalNumber;

            if(!empty($json->calledNumber) && empty($model->office_tel))
                $model->office_tel = $json->calledNumber;

            if(!empty($json->duration) && empty($model->duration))
                $model->duration = $json->duration;

            if(!empty($json->billsec) && empty($model->billsec))
                $model->billsec = $json->billsec;

            if(!empty($json->durationWaiting) && empty($model->duration_waiting))
                $model->duration_waiting = $json->durationWaiting;

            if(!empty($json->answered) && $json->answered == "1")
                $model->answered = 1;

        }

        // ЦИКЛ ЖИЗНИ ИСХОДЯЩЕГО ЗВОНКА

        // ватс начало исходящего звонка
        if($json->my_type_id == 10) {
            $model->direction = "out";

            if(!empty($json->timestamp))
                $model->datetime_start = date('Y-m-d H:i:s', $json->timestamp);

            if(!empty($json->internalNumber))
                $model->internal_number = $json->internalNumber;

            if(!empty($json->calledNumber))
                $model->external_number = $json->calledNumber;

            if(!empty($json->did))
                $model->office_tel = $json->did;

            // ватс исходящий звонок отвечен
        } elseif($json->my_type_id == 11) {
            $model->direction = "out";

            $model->answered = 1;

            if(!empty($json->internalNumber) && empty($model->internal_number))
                $model->internal_number = $json->internalNumber;

            if(!empty($json->calledNumber) && empty($model->external_number))
                $model->external_number = $json->calledNumber;

            if(!empty($json->did) && empty($model->office_tel))
                $model->office_tel = $json->did;

            if(!empty($json->durationWaiting) && empty($model->duration_waiting))
                $model->duration_waiting = $json->durationWaiting;

            // ватс конец исходящего звонка
        } elseif($json->my_type_id == 12) {
            $model->direction = "out";

            if(!empty($json->timestamp))
                $model->datetime_end = date("Y-m-d H:i:s", $json->timestamp);

            if(!empty($json->internalNumber) && empty($model->internal_number))
                $model->internal_number = $json->internalNumber;

            if(!empty($json->calledNumber) && empty($model->external_number))
                $model->external_number = $json->calledNumber;

            if(!empty($json->did) && empty($model->office_tel))
                $model->office_tel = $json->did;

            if(!empty($json->duration) && empty($model->duration))
                $model->duration = $json->duration;

            if(!empty($json->file))
                $model->file = $json->file;

        }

        if($model->save()){
            //d($model->id);
        } else {
            d($model->errors);
        }

    }


    public function actionOld()
    {

        $postData = file_get_contents('php://input');

        $model = new PbxWebhook();
        $model->created_at = aDateNow();
        $model->created_by = null;
        $model->type = "old";
        $model->json = $postData;
        $model->save();
    }

}