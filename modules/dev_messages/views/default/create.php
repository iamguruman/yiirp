<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\dev_messages\models\DevMessages $model */

$this->title = Yii::t('app', 'Create Dev Messages');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dev Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dev-messages-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
