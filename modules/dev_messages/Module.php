<?php

namespace app\modules\dev_messages;

use app\components\ModuleAccess;
use yii\helpers\ArrayHelper;

/**
 * pbx module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\dev_messages\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
    }
}
