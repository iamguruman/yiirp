<?php

namespace app\modules\dev_messages\models;

/**
 * This is the ActiveQuery class for [[DevMessages]].
 *
 * @see DevMessages
 */
class DevMessagesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return DevMessages[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return DevMessages|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
