<?php

namespace app\modules\dev_messages\models;

use Yii;

/**
 * This is the model class for table "dev_messages".
 *
 * @property int $id
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $message
 */
class DevMessages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dev_messages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'message'], 'string'],
            [['created_by'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'message' => Yii::t('app', 'Message'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return DevMessagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DevMessagesQuery(get_called_class());
    }
}
