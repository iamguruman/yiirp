<?php

namespace app\modules\pbx_list\models;

use app\modules\user\models\User;
use Yii;

/**
 * This is the model class for table "pbx_list".
 *
 * @property int $id
 * @property int|null $created_by
 * @property string|null $created_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @property int|null $markdel_by
 * @property string|null $markdel_at
 *
 * @property int|null $owner_sys_user_id
 * @property-read User $ownerSysUser
 *
 * @property int|null $owner_sys_group_id
 * @property string|null $name
 * @property string|null $operator_name
 * @property string|null $public_key
 */
class PbxList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pbx_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_by', 'updated_by', 'markdel_by', 'owner_sys_user_id', 'owner_sys_group_id'], 'integer'],
            [['created_at', 'updated_at', 'markdel_at'], 'safe'],
            [['name', 'operator_name', 'public_key'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'markdel_by' => Yii::t('app', 'Markdel By'),
            'markdel_at' => Yii::t('app', 'Markdel At'),
            'owner_sys_user_id' => Yii::t('app', 'Owner Sys User ID'),
            'owner_sys_group_id' => Yii::t('app', 'Owner Sys Group ID'),
            'name' => Yii::t('app', 'Name'),
            'operator_name' => Yii::t('app', 'Operator Name'),
            'public_key' => Yii::t('app', 'Public Key'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return PbxListQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PbxListQuery(get_called_class());
    }

    public function getOwnerSysUser(){
        return $this->hasOne(User::className(), ['id' => 'owner_sys_user_id']);
    }
}
