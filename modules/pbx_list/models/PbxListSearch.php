<?php

namespace app\modules\pbx_list\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\pbx_list\models\PbxList;

/**
 * PbxListSearch represents the model behind the search form of `app\modules\pbx_list\models\PbxList`.
 */
class PbxListSearch extends PbxList
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by', 'markdel_by', 'owner_sys_user_id', 'owner_sys_group_id'], 'integer'],
            [['created_at', 'updated_at', 'markdel_at', 'name', 'operator_name', 'public_key'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PbxList::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'markdel_by' => $this->markdel_by,
            'markdel_at' => $this->markdel_at,
            'owner_sys_user_id' => $this->owner_sys_user_id,
            'owner_sys_group_id' => $this->owner_sys_group_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'operator_name', $this->operator_name])
            ->andFilterWhere(['like', 'public_key', $this->public_key]);

        return $dataProvider;
    }
}
