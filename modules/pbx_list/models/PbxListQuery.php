<?php

namespace app\modules\pbx_list\models;

/**
 * This is the ActiveQuery class for [[PbxList]].
 *
 * @see PbxList
 */
class PbxListQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return PbxList[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PbxList|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
