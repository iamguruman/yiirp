<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\pbx_list\models\PbxList $model */

$this->title = Yii::t('app', 'Create Pbx List');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pbx Lists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pbx-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
