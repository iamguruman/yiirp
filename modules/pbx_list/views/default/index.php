<?php

use app\modules\pbx_list\models\PbxList;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this */
/** @var app\modules\pbx_list\models\PbxListSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app', 'Pbx Lists');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pbx-list-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Pbx'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'created_by',
            //'created_at',
            //'updated_by',
            //'updated_at',
            //'markdel_by',
            //'markdel_at',
            'ownerSysUser.username',
            //'owner_sys_group_id',
            'name',
            'operator_name',
            //'public_key',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, PbxList $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
