<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\modules\pbx_list\models\PbxList $model */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pbx Lists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pbx-list-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'owner_sys_user_id',
            'owner_sys_group_id',
            'name',
            'operator_name',
            'public_key',
        ],
    ]) ?>

    <?= \yii\bootstrap5\Tabs::widget(['items' => [
        [
            'label' => 'Details',
            'active' => false,
            'content' => "<br>".DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'created_by',
                        'created_at',
                        'updated_by',
                        'updated_at',
                        'markdel_by',
                        'markdel_at',
                    ],
                ]),
        ],
    ]])?>

</div>
