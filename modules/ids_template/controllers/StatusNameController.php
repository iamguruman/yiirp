<?php

namespace app\modules\ids_template\controllers;

use app\modules\ids_template\models\IdsTemplateStatusName;
use app\modules\ids_template\models\IdsTemplateStatusNameSearch;
use app\modules\ids_template\models\TemplateName;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StatusNameController implements the CRUD actions for IdsTemplateStatusName model.
 */
class StatusNameController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all IdsTemplateStatusName models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new IdsTemplateStatusNameSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single IdsTemplateStatusName model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new IdsTemplateStatusName model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new IdsTemplateStatusName();

        $model->ids_template_name_id = TemplateName::findOne(aGet('template_id'));

        $model->created_by = aUserMyId();
        $model->created_at = aDateNow();

        $model->sort = aGet('sort');

        if ($this->request->isPost) {

            if ($model->load($this->request->post())) {

                if(!$model->sort){
                    $last = IdsTemplateStatusName::find()
                        ->andWhere(['ids_template_name_id' => $model->ids_template_name_id])
                        ->orderBy(['sort' => SORT_DESC])
                        ->one();

                    if($last){
                        $model->sort = $last->sort + 10;
                    } else {
                        $model->sort = 0;
                    }
                }

                if($model->save()){

                }

                self::recalcSort($model->ids_template_name_id);

                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    private static function recalcSort($template_name_id){

        $i = 0;
        foreach ($names = IdsTemplateStatusName::find()
            ->andWhere(['ids_template_name_id' => $template_name_id])
            ->orderBy(['sort' => SORT_ASC])
            ->all() as $name)
        {

            $i += 10;
            $name->sort = $i;
            $name->save();
        }

    }

    /**
     * Updates an existing IdsTemplateStatusName model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post())) {

            if ($model->save()) {

                self::recalcSort($model->ids_template_name_id);

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing IdsTemplateStatusName model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the IdsTemplateStatusName model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return IdsTemplateStatusName the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IdsTemplateStatusName::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
