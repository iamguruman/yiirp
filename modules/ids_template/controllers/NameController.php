<?php

namespace app\modules\ids_template\controllers;

use app\modules\ids_flow\models\IdsFlowSearch;
use app\modules\ids_template\models\IdsPrintTemplateSearch;
use app\modules\ids_template\models\IdsTemplateLinks;
use app\modules\ids_template\models\IdsTemplateLinksSearch;
use app\modules\ids_template\models\IdsTemplateStatusNameSearch;
use app\modules\ids_template\models\IdsTemplateUserAccess;
use app\modules\ids_template\models\IdsTemplateUserAccessSearch;
use app\modules\ids_template\models\TemplateFieldSearch;
use app\modules\ids_template\models\TemplateName;
use app\modules\ids_template\models\TemplateNameSearch;
use app\modules\ids_import\models\IdsImportNameSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * NameController implements the CRUD actions for TemplateName model.
 */
class NameController extends Controller
{
    private static function uploadIcon(TemplateName $model)
    {
        $directory = Yii::getAlias('@app/web/tplicons') . DIRECTORY_SEPARATOR;

        if($model->removeIcon && file_exists($directory."/{$model->id}.png")){
            unlink($directory."/{$model->id}.png");
        }

        if(count(UploadedFile::getInstances($model, 'iconFile')) > 0){
            $uploadedFile = UploadedFile::getInstances($model, 'iconFile')[0];
            if(!file_exists($directory)){
                mkdir($directory);
            }
            $uploadedFile->saveAs($directory."/{$model->id}.".$uploadedFile->extension);
        }
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all TemplateName models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new TemplateNameSearch();
        $dataProvider = $searchModel->search($this->request->queryParams, [
            'user_id' => Yii::$app->user->id
        ]);

        foreach (IdsTemplateUserAccess::find()->all() as $access){
            if($access->idsTemplateName && $access->idsTemplateName->owner_user_id == $access->sys_user_id){
                $access->can_edit_template = 1;
                $access->save();
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TemplateName model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $fieldSearchModel = new TemplateFieldSearch();
        $fieldDataProvider = $fieldSearchModel->search(Yii::$app->request->queryParams, [
            'template_name_id' => $id
        ]);
        $fieldDataProvider->setSort(['defaultOrder' => ['sort' => SORT_ASC]]);

        $printTemaplateSearchModel = new IdsPrintTemplateSearch();
        $printTemaplateDataProvider = $printTemaplateSearchModel->search(Yii::$app->request->queryParams, [
            'ids_template_name_id' => $id
        ]);

        $flowSearchModel = new IdsFlowSearch();
        $flowDataProvider = $flowSearchModel->search(Yii::$app->request->queryParams, [
            'template_id' => $id
        ]);

        $linksSearchModel = new IdsTemplateLinksSearch();
        $linksDataProvider = $linksSearchModel->search(Yii::$app->request->queryParams, [
            'ids_template_name_id' => $id
        ]);

        $importSearchModel = new IdsImportNameSearch();
        $importDataProvider = $importSearchModel->search($this->request->queryParams,[
            'ids_template_name_id' => $id
        ]);

        $userAccessSearchModel = new IdsTemplateUserAccessSearch();
        $userAccessDataProvider = $userAccessSearchModel->search($this->request->queryParams,[
            'ids_template_name_id' => $id
        ]);

        $statusNameSearchModel = new IdsTemplateStatusNameSearch();
        $statusNameDataProvider = $statusNameSearchModel->search($this->request->queryParams,[
            'ids_template_name_id' => $id
        ]);
        $statusNameDataProvider->setSort(['defaultOrder' => ['sort' => SORT_ASC]]);


        return $this->render('view', [
            'statusNameSearchModel' => $statusNameSearchModel,
            'statusNameDataProvider' => $statusNameDataProvider,

            'userAccessSearchModel' => $userAccessSearchModel,
            'userAccessDataProvider' => $userAccessDataProvider,

            'importSearchModel' => $importSearchModel,
            'importDataProvider' => $importDataProvider,

            'linksSearchModel' => $linksSearchModel,
            'linksDataProvider' => $linksDataProvider,

            'flowSearchModel' => $flowSearchModel,
            'flowDataProvider' => $flowDataProvider,

            'printTemaplateSearchModel' => $printTemaplateSearchModel,
            'printTemaplateDataProvider' => $printTemaplateDataProvider,

            'fieldSearchModel' => $fieldSearchModel,
            'fieldDataProvider' => $fieldDataProvider,

            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TemplateName model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new TemplateName();

        $model->owner_user_id = Yii::$app->user->id;

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {

                if ($model->save()) {

                    self::uploadIcon($model);

                    $access = new IdsTemplateUserAccess();
                    $access->created_at = aDateNow();
                    $access->created_by = aUserMyId();
                    $access->ids_template_name_id = $model->id;
                    $access->sys_user_id = aUserMyId();
                    $access->can_list = 1;
                    $access->can_create = 1;
                    $access->can_update_self_created = 1;
                    $access->can_update_other_created = 1;
                    if($access->save()){

                    } else {
                        ddd($access->errors);
                    }

                    aReturnto();
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TemplateName model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post())) {

            if ($model->save()) {

                self::uploadIcon($model);

                aReturnto();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TemplateName model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TemplateName model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return TemplateName the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TemplateName::findOne(['id' => $id, 'owner_user_id' => Yii::$app->user->id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
