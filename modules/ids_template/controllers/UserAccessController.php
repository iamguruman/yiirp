<?php

namespace app\modules\ids_template\controllers;

use app\modules\ids_template\models\IdsTemplateUserAccess;
use app\modules\ids_template\models\IdsTemplateUserAccessSearch;
use app\modules\ids_template\models\TemplateName;
use app\modules\user\models\User;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserAccessController implements the CRUD actions for IdsTemplateUserAccess model.
 */
class UserAccessController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all IdsTemplateUserAccess models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new IdsTemplateUserAccessSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single IdsTemplateUserAccess model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new IdsTemplateUserAccess model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new IdsTemplateUserAccess();

        $model->created_at = aDateNow();
        $model->created_by = Yii::$app->user->id;

        if($templateName = TemplateName::findOne(aGet('template_id'))){
            $model->ids_template_name_id = $templateName->id;
        }

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {

                if($userModel = User::findOne(['username' => $model->username])){

                    $model->sys_user_id = $userModel->id;

                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->id]);
                    }

                } else {
                    ddd( "Username {$model->username} not found");
                }

            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing IdsTemplateUserAccess model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if($model->sysUser){
            $model->username = $model->sysUser->username;
        }

        if ($this->request->isPost && $model->load($this->request->post())) {

            if($userModel = User::findOne(['username' => $model->username])) {

                $model->sys_user_id = $userModel->id;

                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } else {
                ddd( "Username {$model->username} not found");
            }

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing IdsTemplateUserAccess model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the IdsTemplateUserAccess model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return IdsTemplateUserAccess the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IdsTemplateUserAccess::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
