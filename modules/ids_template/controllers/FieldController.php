<?php

namespace app\modules\ids_template\controllers;

use app\modules\ids_field\models\IdsTemplateFieldReplaceSearch;
use app\modules\ids_template\models\TemplateField;
use app\modules\ids_template\models\TemplateFieldSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FieldController implements the CRUD actions for TemplateField model.
 */
class FieldController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all TemplateField models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new TemplateFieldSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TemplateField model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $model = $this->findModel($id);

        $fieldReplaceSearchModel = new IdsTemplateFieldReplaceSearch();
        $fieldReplaceDataProvider = $fieldReplaceSearchModel->search(Yii::$app->request->queryParams, [
            'ids_template_field_id' => $model->id
        ]);

        return $this->render('view', [

            'fieldReplaceSearchModel' => $fieldReplaceSearchModel,
            'fieldReplaceDataProvider' => $fieldReplaceDataProvider,

            'model' => $model,
        ]);
    }

    /**
     * Creates a new TemplateField model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new TemplateField();

        $model->sort = aGet('sort');

        $model->value_field = "varchar255";

        if(Yii::$app->request->get("template_name_id")){
            $model->template_name_id = Yii::$app->request->get("template_name_id");
        }

        if ($this->request->isPost) {
            if ($model->load($this->request->post())){

                if(!$model->sort){
                    $lastSort = TemplateField::find()
                        ->select("sort")
                        ->andWhere(['template_name_id' => $model->template_name_id])
                        ->orderBy(['sort' => SORT_DESC])
                        ->limit(1)
                        ->scalar();

                    //ddd($lastSort);
                    $model->sort = $lastSort + 10;

                    //ddd($model->sort);

                }

                if($model->save()) {

                    self::recalcSort($model->template_name_id);

                    aReturnto();
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    private static function recalcSort($template_id){

        $fields = TemplateField::find()
            ->andWhere(['template_name_id' => $template_id])
            ->orderBy(['sort' => SORT_ASC])
            ->all();

        $i = 10;
        foreach ($fields as $field){
            $field->sort = $i;

            $i += 10;

            $field->save();
        }

    }

    /**
     * Updates an existing TemplateField model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                if ($model->save()) {
                    aReturnto();
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TemplateField model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $templateName = $model->templateName;

        $model->delete();

        return $this->redirect(['/ids_template/name/view', 'id' => $templateName->id]);
    }

    /**
     * Finds the TemplateField model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return TemplateField the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TemplateField::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
