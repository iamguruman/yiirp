<?php

namespace app\modules\ids_template\models;

use Yii;

/**
 * This is the model class for table "ids_template_links".
 *
 * @property int $id
 * @property int $created_by
 * @property string $created_at
 * @property int $markdel_by
 * @property string $markdel_at
 * @property string $updated_at
 * @property int $updated_by
 *
 * @property int|null $ids_template_name_id
 * @property int|null $ids_template_field_id
 *
 * @property string|null $text
 * @property string|null $title
 * @property string|null $link
 * @property string|null $target
 *
 * @property-read TemplateName $idsTemplateName
 * @property-read TemplateField[] $idsTemplateField
 *
 * @property int $redirect
 */
class IdsTemplateLinks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ids_template_links';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['redirect'], 'integer', 'max' => 1],
            [['target'], 'string', 'max' => 50],
            [['ids_template_name_id'], 'required'],
            [['created_by', 'markdel_by', 'updated_by', 'ids_template_name_id', 'ids_template_field_id'], 'integer'],
            [['created_at', 'markdel_at', 'updated_at'], 'safe'],
            [['text', 'title'], 'string', 'max' => 255],
            [['link'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'redirect' => Yii::t('app', 'Redirect'),
            'target' => Yii::t('app', 'Target'),
            'id' => Yii::t('app', 'ID'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'markdel_by' => Yii::t('app', 'Markdel By'),
            'markdel_at' => Yii::t('app', 'Markdel At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'ids_template_name_id' => Yii::t('app', 'Ids Template Name ID'),
            'ids_template_field_id' => Yii::t('app', 'Ids Template Field ID'),
            'text' => Yii::t('app', 'Text'),
            'title' => Yii::t('app', 'Title'),
            'link' => Yii::t('app', 'Link'),
        ];
    }

    public function getIdsTemplateName(){
        return $this->hasOne(TemplateName::className(), ['id' => 'ids_template_name_id']);
    }

    public function getIdsTemplateField(){
        return $this->hasOne(TemplateName::className(), ['id' => 'ids_template_field_id']);
    }

    /**
     * {@inheritdoc}
     * @return IdsTemplateLinksQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IdsTemplateLinksQuery(get_called_class());
    }
}
