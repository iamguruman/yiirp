<?php

namespace app\modules\ids_template\models;

use Yii;

/**
 * This is the model class for table "ids_template_status_name".
 *
 * @property int $id
 * @property int|null $created_by
 * @property string|null $created_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @property int|null $markdel_by
 * @property string|null $markdel_at
 * @property string $name Template Name
 *
 * @property int $ids_template_name_id
 * @property-read TemplateName $idsTemplateName
 *
 * @property string|null $description
 * @property int|null $sort
 * @property string|null $style
 *
 * @property int|null $show_counter
 */
class IdsTemplateStatusName extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ids_template_status_name';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['show_counter'], 'integer', 'max' => 1],

            [['sort'], 'integer'],

            [['style'], 'string', 'max' => 500],

            [['name', 'ids_template_name_id'], 'required'],
            [['id', 'created_by', 'updated_by', 'markdel_by', 'ids_template_name_id'], 'integer'],
            [['created_at', 'updated_at', 'markdel_at'], 'safe'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'show_counter' => Yii::t('app', 'Show counter'),
            'style' => Yii::t('app', 'Style'),
            'sort' => Yii::t('app', 'Sort'),
            'id' => Yii::t('app', 'ID'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'markdel_by' => Yii::t('app', 'Markdel By'),
            'markdel_at' => Yii::t('app', 'Markdel At'),
            'name' => Yii::t('app', 'Name'),
            'ids_template_name_id' => Yii::t('app', 'Ids Template Name ID'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    public function getIdsTemplateName(){
        return $this->hasOne(TemplateName::className(), ['id' => 'ids_template_name_id']);
    }

    /**
     * {@inheritdoc}
     * @return IdsTemplateStatusNameQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IdsTemplateStatusNameQuery(get_called_class());
    }
}
