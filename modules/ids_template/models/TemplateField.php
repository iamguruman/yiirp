<?php

namespace app\modules\ids_template\models;

use app\modules\ids\models\Ids;
use app\modules\ids_field\models\IdsTemplateFieldReplace;
use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "template_field".
 *
 * @property int $id
 *
 * @property string|null $title
 * @property-read  string|null $titleTranslitered
 *
 * @property string|null $comment
 * @property int|null $sort
 *
 * @property-read  TemplateName $templateName
 * @property int $template_name_id
 *
 * @property string $value_field
 *
 * @property int $show_as_column_at_template_list
 *
 * @property int $source_template_name_id
 * @property-read  TemplateName $sourceTemplateName
 *
 * @property int $show_in_tree
 *
 * @property string $html_element
 *
 * @property string $default_value
 *
 * @property string $saving_changes
 *
 * @property int $auto_increment
 * @property string $auto_increment_template
 *
 * @property string $onchange
 * @property string $onload
 *
 * @property string $hint
 *
 * @property-read string $hintWithHintInsertion
 *
 * @property string $hint_insertion_set
 * @property string $hint_insertion_insert
 *
 * @property-read IdsTemplateFieldReplace[] $idsTemplateFieldReplaces
 *
 * @property int $this_is_telnumber
 * @property int $this_is_url
 * @property int $this_is_email
 *
 * @property int $is_searchable
 *
 */
class TemplateField extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ids_template_field';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hint_insertion_set'], 'string'],
            [['hint_insertion_insert'], 'string'],

            [['is_searchable'], 'integer', 'max' => 1],

            [['this_is_email'], 'integer', 'max' => 1],
            [['this_is_telnumber'], 'integer', 'max' => 1],
            [['this_is_url'], 'integer', 'max' => 1],

            [['onload'], 'string'],
            [['onchange'], 'string'],

            [['auto_increment'], 'integer', 'max' => 1],
            [['auto_increment_template'], 'string', 'max' => 255],

            [['saving_changes'], 'integer', 'max' => 1],

            [['default_value'], 'string', 'max' => 255],

            [['html_element'], 'string', 'max' => 255],

            [['show_in_tree'], 'integer', 'max' => 1],

            [['source_template_name_id'], 'integer'],

            [['show_as_column_at_template_list'], 'integer', 'max' => 1],

            [['value_field'], 'string', 'max' => 255],

            [['template_name_id'], 'required'],
            [['template_name_id', 'sort'], 'integer'],
            [['comment'], 'string'],

            [['title'], 'string', 'max' => 255],
            [['hint'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hint_insertion_set' => Yii::t('app', 'Hint Setter'),
            'hint_insertion_insert' => Yii::t('app', 'Hint Inserter'),

            'is_searchable' => Yii::t('app', 'Is Searchable'),

            'this_is_email' => Yii::t('app', 'This is email'),
            'this_is_telnumber' => Yii::t('app', 'This is telephone number'),
            'this_is_url' => Yii::t('app', 'This is website URL'),

            'onchange' => Yii::t('app', 'On change'),
            'saving_changes' => Yii::t('app', 'Saving changes'),
            'default_value' => Yii::t('app', 'Default value'),
            'html_element' => Yii::t('app', 'Html element'),
            'show_in_tree' => Yii::t('app', 'Show in tree'),
            'source_template_name_id' => Yii::t('app', 'Source template name'),
            'show_as_column_at_template_list' => Yii::t('app', 'Show as column at template list'),
            'value_field' => Yii::t('app', 'Field Value'),
            'id' => Yii::t('app', 'ID'),
            'template_name_id' => Yii::t('app', 'Template Name ID'),
            'title' => Yii::t('app', 'Title'),
            //'name' => Yii::t('app', 'Name'),
            'comment' => Yii::t('app', 'Comment'),
            'sort' => Yii::t('app', 'Sort'),
        ];
    }

    public function getTemplateName(){
        return $this->hasOne(TemplateName::className(), ['id' => 'template_name_id']);
    }

    public function getTitleTranslitered(){

        $converter = array(
            'а' => 'a',    'б' => 'b',    'в' => 'v',    'г' => 'g',    'д' => 'd',
            'е' => 'e',    'ё' => 'e',    'ж' => 'zh',   'з' => 'z',    'и' => 'i',
            'й' => 'y',    'к' => 'k',    'л' => 'l',    'м' => 'm',    'н' => 'n',
            'о' => 'o',    'п' => 'p',    'р' => 'r',    'с' => 's',    'т' => 't',
            'у' => 'u',    'ф' => 'f',    'х' => 'h',    'ц' => 'c',    'ч' => 'ch',
            'ш' => 'sh',   'щ' => 'sch',  'ь' => '',     'ы' => 'y',    'ъ' => '',
            'э' => 'e',    'ю' => 'yu',   'я' => 'ya',

            'А' => 'A',    'Б' => 'B',    'В' => 'V',    'Г' => 'G',    'Д' => 'D',
            'Е' => 'E',    'Ё' => 'E',    'Ж' => 'Zh',   'З' => 'Z',    'И' => 'I',
            'Й' => 'Y',    'К' => 'K',    'Л' => 'L',    'М' => 'M',    'Н' => 'N',
            'О' => 'O',    'П' => 'P',    'Р' => 'R',    'С' => 'S',    'Т' => 'T',
            'У' => 'U',    'Ф' => 'F',    'Х' => 'H',    'Ц' => 'C',    'Ч' => 'Ch',
            'Ш' => 'Sh',   'Щ' => 'Sch',  'Ь' => '',     'Ы' => 'Y',    'Ъ' => '',
            'Э' => 'E',    'Ю' => 'Yu',   'Я' => 'Ya',

            ' ' => '_',
            '-' => '_',
            '.' => '_',
            ',' => '_',
            '(' => '_',
            ')' => '_',
            '/' => '_',
            '\\' => '_',
        );

        $value = strtr($this->title, $converter);

        return $value;

    }

    public function getFieldValue($ids_id, $template_field_id){
        return self::getFieldValueStatic($ids_id, $template_field_id, $this->value_field);
    }

    public static function getFieldValueStatic($ids_id, $template_field_id, $value_field){

        $sql = "
            select 
                value 
            from ids_value_{$value_field} 
            where ids_id={$ids_id} 
            and   template_field_id = {$template_field_id}
        ";

        $return = Yii::$app->db->createCommand($sql)->queryScalar();

        $fieldModel = TemplateField::findOne($template_field_id);
        if($fieldModel->sourceTemplateName){
            if($idsId = Ids::findOne($return)){
                if($idsId->name){
                    $ret = [];
                    $ret [] = Html::a($idsId->name, ['/ids/view', 'id' => $idsId->id], ['target' => '_blank']);
                    $ret [] = Html::a("^", ['/ids/view', 'id' => $idsId->id],
                        ['target' => '_blank', 'title' => 'Open in new window']);

                    return implode("&nbsp;", $ret);
                }
            }

        }

        return $return;


    }

    public function getSourceTemplateName(){
        return $this->hasOne(TemplateName::className(), ['id' => 'source_template_name_id']);
    }

    /**
     * {@inheritdoc}
     * @return TemplateFieldQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TemplateFieldQuery(get_called_class());
    }

    public function getIdsTemplateFieldReplaces(){
        return $this->hasMany(IdsTemplateFieldReplace::className(), ['ids_template_field_id' => 'id']);
    }

    public function gethintWithHintInsertion(){
        $ret = [];

        if($this->hint){
            $ret [] = $this->hint;
        }

        if($this->hint_insertion_set){
            $exps = explode(", ",$this->hint_insertion_set);

            if(count($exps) > 0){
                $set = [];

                foreach ($exps as $value){
                    $set [] = Html::a($value,'#', ['onClick' => "idsFieldSetValue('ids-fieldvalue-{$this->id}','{$value}'); return false;"]);
                    //$set [] = '<a href=\'#\' onclick=\'idsFieldSetValue(\'ids-fieldvalue-'.$this->id.'\','.'"'.$value.'"'."); return false;'>{$value}</a>";
                }

                $ret [] = "Set: ".implode(", ", $set);

            }

        }

        if($this->hint_insertion_insert){
            $exps = explode(", ",$this->hint_insertion_insert);

            if(count($exps) > 0){

                $insert = [];

                foreach ($exps as $value){
                    $insert [] = Html::a($value,'#', ['onClick' => "idsFieldInsertValue('ids-fieldvalue-{$this->id}','{$value}'); return false;"]);
                    //$set [] = '<a href=\'#\' onclick=\'idsFieldSetValue(\'ids-fieldvalue-'.$this->id.'\','.'"'.$value.'"'."); return false;'>{$value}</a>";
                }

                $ret [] = "Insert: ".implode(", ", $insert);
            }

        }

        return implode("<br>", $ret);
    }
}
