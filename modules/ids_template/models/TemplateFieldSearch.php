<?php

namespace app\modules\ids_template\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\ids_template\models\TemplateField;

/**
 * TemplateFieldSearch represents the model behind the search form of `app\modules\ids_template\models\TemplateField`.
 */
class TemplateFieldSearch extends TemplateField
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'template_name_id', 'sort'], 'integer'],
            [['title', 'comment'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $params2 = [])
    {
        $query = TemplateField::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'template_name_id' => $this->template_name_id,
            'sort' => $this->sort,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            //->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        if(!empty($params2['template_name_id'])){
            $query->andWhere(['template_name_id' => $params2['template_name_id']]);
        }

        return $dataProvider;
    }
}
