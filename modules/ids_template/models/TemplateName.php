<?php

namespace app\modules\ids_template\models;

use app\modules\user\models\User;
use Yii;

/**
 * This is the model class for table "temaplate_name".
 *
 * @property int $id
 * @property string $name Template Name
 *
 * @property string $name_template
 *
 * @property-read  TemplateField|null $templateFields
 * @property-read  TemplateField|null $templateFieldsForTableList
 *
 * @property int $owner_user_id
 * @property User $ownerUser
 *
 * @property-read IdsTemplateUserAccess[] $usersAccess
 *
 * @property string $extra_module_action_view
 *
 * @property string $hint
 *
 * @property int $show_field_name_on_crud_index
 *
 * @property-read IdsTemplateStatusName[] $statusNames
 *
 */
class TemplateName extends \yii\db\ActiveRecord
{

    public $iconFile;
    public $removeIcon = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ids_template_name';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['show_field_name_on_crud_index'], 'integer', 'max' => 1],

            [['hint'], 'string'],

            [['extra_module_action_view'], 'string', 'max' => 1000],
            [['owner_user_id'], 'integer'],

            [['removeIcon'], 'integer', 'max' => 1],

            [['name_template'], 'string', 'max' => 255],

            [['iconFile'], 'file', 'extensions' => 'png'],

            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hint' => Yii::t('app', 'Hint'),
            'owner_user_id' => Yii::t('app', 'User owner'),
            'removeIcon' => Yii::t('app', 'Remove icon'),
            'name_template' => Yii::t('app', 'Name template'),
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Template Name'),
        ];
    }

    public function getTemplateFields(){
        return $this->hasMany(TemplateField::className(), ['template_name_id' => 'id'])
            ->orderBy([TemplateField::tableName().'.sort' => SORT_ASC]);
    }

    public function getTemplateFieldsForTableList(){
        return $this->hasMany(TemplateField::className(), ['template_name_id' => 'id'])
            ->andWhere([TemplateField::tableName().".show_as_column_at_template_list" => 1])
            ->orderBy([TemplateField::tableName().'.sort' => SORT_ASC]);
    }

    /**
     * {@inheritdoc}
     * @return TemplateNameQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TemplateNameQuery(get_called_class());
    }

    public function getUsersAccess(){
        return $this->hasMany(IdsTemplateUserAccess::className(), ['ids_template_name_id' => 'id']);
    }

    public function getOwnerUser(){
        return $this->hasOne(User::className(), ['id' => 'owner_user_id']);
    }

    public function getStatusNames(){
        return $this->hasMany(IdsTemplateStatusName::className(), ['ids_template_name_id' => 'id'])
            ->orderBy(['sort' => SORT_ASC]);
    }

    public function getUserCanEdit($user_id = null){

        if($user_id == null){
            $user_id = aUserMyId();
        }

        return IdsTemplateUserAccess::find()
            ->select('can_edit_template')
            ->andWhere(['sys_user_id' => $user_id])
            ->scalar();
    }
}
