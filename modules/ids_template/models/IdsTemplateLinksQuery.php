<?php

namespace app\modules\ids_template\models;

/**
 * This is the ActiveQuery class for [[IdsTemplateLinks]].
 *
 * @see IdsTemplateLinks
 */
class IdsTemplateLinksQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return IdsTemplateLinks[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return IdsTemplateLinks|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
