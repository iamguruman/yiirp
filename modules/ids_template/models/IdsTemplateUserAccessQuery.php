<?php

namespace app\modules\ids_template\models;

/**
 * This is the ActiveQuery class for [[IdsTemplateUserAccess]].
 *
 * @see IdsTemplateUserAccess
 */
class IdsTemplateUserAccessQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return IdsTemplateUserAccess[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return IdsTemplateUserAccess|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
