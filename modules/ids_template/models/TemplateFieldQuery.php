<?php

namespace app\modules\ids_template\models;

/**
 * This is the ActiveQuery class for [[TemplateField]].
 *
 * @see TemplateField
 */
class TemplateFieldQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return TemplateField[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return TemplateField|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
