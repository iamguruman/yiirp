<?php

namespace app\modules\ids_template\models;

use Yii;

/**
 * This is the model class for table "ids_print_template".
 *
 * @property int $id
 * @property int|null $created_by
 * @property string|null $created_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @property int|null $markdel_by
 * @property string|null $markdel_at
 *
 * @property int $ids_template_name_id
 * @property-read TemplateName $idsTemplateName
 *
 * @property string $name
 * @property resource|null $html_content
 *
 */
class IdsPrintTemplate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ids_print_template';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_by', 'updated_by', 'markdel_by', 'ids_template_name_id'], 'integer'],
            [['created_at', 'updated_at', 'markdel_at'], 'safe'],
            [['ids_template_name_id', 'name'], 'required'],
            [['html_content'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'markdel_by' => Yii::t('app', 'Markdel By'),
            'markdel_at' => Yii::t('app', 'Markdel At'),
            'ids_template_name_id' => Yii::t('app', 'Ids Template Name ID'),
            'name' => Yii::t('app', 'Name'),
            'html_content' => Yii::t('app', 'Html Content'),
        ];
    }

    public function getIdsTemplateName(){
        return $this->hasOne(TemplateName::className(), ['id' => 'ids_template_name_id']);
    }

    /**
     * {@inheritdoc}
     * @return IdsPrintTemplateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IdsPrintTemplateQuery(get_called_class());
    }


}
