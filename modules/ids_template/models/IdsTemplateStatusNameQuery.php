<?php

namespace app\modules\ids_template\models;

/**
 * This is the ActiveQuery class for [[IdsTemplateStatusName]].
 *
 * @see IdsTemplateStatusName
 */
class IdsTemplateStatusNameQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return IdsTemplateStatusName[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return IdsTemplateStatusName|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
