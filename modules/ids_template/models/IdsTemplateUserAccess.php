<?php

namespace app\modules\ids_template\models;

use app\modules\user\models\User;
use Yii;

/**
 * This is the model class for table "ids_template__user_access".
 *
 * @property int $id
 * @property int|null $created_by
 * @property string|null $created_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @property int|null $markdel_by
 * @property string|null $markdel_at
 *
 * @property int $ids_template_name_id
 * @property-read TemplateName $idsTemplateName
 *
 * @property int $sys_user_id
 * @property-read User $sysUser
 *
 * @property int|null $can_list
 *
 * @property int|null $can_create
 *
 * @property int $can_read_only_self_created
 *
 * @property int $can_update_self_created
 *
 * @property int $can_update_other_created
 *
 * @property int $can_edit_template
 */
class IdsTemplateUserAccess extends \yii\db\ActiveRecord
{

    public $username;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ids_template__user_access';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['can_edit_template'], 'integer', 'max' => 1],

            [['username'], 'string', 'max' => 50],

            [['can_list'], 'integer', 'max' => 1],
            [['can_read_only_self_created'], 'integer', 'max' => 1],
            [['can_create'], 'integer', 'max' => 1],
            [['can_update_self_created'], 'integer', 'max' => 1],
            [['can_update_other_created'], 'integer', 'max' => 1],


            [['created_by', 'updated_by', 'markdel_by', 'ids_template_name_id', 'sys_user_id'], 'integer'],
            [['created_at', 'updated_at', 'markdel_at'], 'safe'],
            [['ids_template_name_id', 'sys_user_id'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'markdel_by' => Yii::t('app', 'Markdel By'),
            'markdel_at' => Yii::t('app', 'Markdel At'),
            'ids_template_name_id' => Yii::t('app', 'Ids Template Name ID'),
            'sys_user_id' => Yii::t('app', 'Sys User ID'),
            'can_create' => Yii::t('app', 'Can Create'),
            'can_read_only_self_created' => Yii::t('app', 'Can Read Only Self Created'),
            'can_update_self_created' => Yii::t('app', 'Can Update Self Created'),
            'can_update_other_created' => Yii::t('app', 'Can Update Other Created'),
            'can_edit_template' => Yii::t('app', 'Can Update Template'),
        ];
    }

    public function getIdsTemplateName(){
        return $this->hasOne(TemplateName::className(), ['id' => 'ids_template_name_id']);
    }

    public function getSysUser(){
        return $this->hasOne(User::className(), ['id' => 'sys_user_id']);
    }

    /**
     * {@inheritdoc}
     * @return IdsTemplateUserAccessQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IdsTemplateUserAccessQuery(get_called_class());
    }
}
