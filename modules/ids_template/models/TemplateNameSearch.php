<?php

namespace app\modules\ids_template\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\ids_template\models\TemplateName;

/**
 * TemplateNameSearch represents the model behind the search form of `app\modules\ids_template\models\TemplateName`.
 */
class TemplateNameSearch extends TemplateName
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $params2 = [])
    {
        $query = TemplateName::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        if(!empty($params2['owner_user_id'])){
            $query->andWhere(['owner_user_id' => $params2['owner_user_id']]);
        }

        if(!empty($params2['user_id'])){
            $query
                ->joinWith('usersAccess')
                ->andWhere(['or',
                    [TemplateName::tableName().'.owner_user_id' => $params2['user_id']],
                    [IdsTemplateUserAccess::tableName().'.sys_user_id' => $params2['user_id']],
                ])
            ;
        }

        return $dataProvider;
    }
}
