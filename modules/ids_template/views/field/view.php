<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\TemplateField $model */

/** @var \app\modules\ids_field\models\IdsTemplateFieldReplaceSearch $fieldReplaceSearchModel */
/** @var \yii\data\ActiveDataProvider $fieldReplaceDataProvider */

$this->title = $model->title;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Template Names'),
    'url' => ['/ids_template/index']];

if($model->templateName){
    $this->params['breadcrumbs'][] = ['label' => $model->templateName->name,
        'url' => ['/ids_template/name/view', 'id' => $model->template_name_id]];
}

$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="template-field-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'template_name_id',

            [
                'attribute' => 'value_field',
                'value' => function($model){
                    return "ids_value_{$model->value_field}";
                }
            ],

            'title',
            'html_element',
            'default_value',
            'comment:ntext',
            'sort',
            'onchange',
            'hint',

            'show_as_column_at_template_list:boolean',
            'show_in_tree:boolean',

            'sourceTemplateName.name:raw:Source Template Name',

            'saving_changes:boolean',
            'is_searchable:boolean',

            'this_is_email:boolean',
            'this_is_url:boolean',
            'this_is_telnumber:boolean',
        ],
    ]) ?>

    <?= \yii\bootstrap5\Tabs::widget(['items' => [
        [
            'label' => 'Action Links',
            'content' => "<br>"
        ],

        [
            'label' => "Replace ({$fieldReplaceDataProvider->totalCount})",
            'active' => aGet('tab') == 'field-replace' ? true : null,
            'content' => "<br>".$this->render("@app/modules/ids_field/views/field-replace/index.php", [
                    'searchModel' => $fieldReplaceSearchModel,
                    'dataProvider' => $fieldReplaceDataProvider,
                ])
        ],

    ]]) ?>

</div>
