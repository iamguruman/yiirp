<?php

use app\modules\ids_template\models\TemplateField;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\TemplateFieldSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

if(Yii::$app->controller->action->id == 'index'){
    $this->title = Yii::t('app', 'Template Fields');
    $this->params['breadcrumbs'][] = $this->title;
}
?>
<div class="template-field-index">

    <?php if(Yii::$app->controller->action->id == 'index'): ?>
        <h1><?= Html::encode($this->title) ?></h1>
    <?php endif; ?>

    <p>
        <?php if(Yii::$app->controller->module->id == "ids_template"):?>
            <?= Html::a(Yii::t('app', 'Create Template Field'),
                ['/ids_template/field/create',
                    'template_name_id' => Yii::$app->request->get("id"),
                    'returnto' => $_SERVER['REQUEST_URI']],
                ['class' => 'btn btn-success']) ?>

            <?= Html::a(Yii::t('app', '...as first field'),
                ['/ids_template/field/create',
                    'template_name_id' => Yii::$app->request->get("id"),
                    'returnto' => $_SERVER['REQUEST_URI'],
                    'sort' => 1
                ],
                ['class' => 'btn btn-success']) ?>
        <?php else: ?>
            <?= Html::a(Yii::t('app', 'Create Template Field'),
                ['create'], ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'sort',
            //'template_name_id',
            'title',
            'value_field',
            //'name',
            'comment:ntext',
            //'sort',
            'show_as_column_at_template_list',
            'show_in_tree:boolean',

            'sourceTemplateName.name:raw:Source Template Name',

            aIfModuleControllerAction("ids_template", "name", "view") ? [
                //'label' => 'Sort',
                'format' => 'raw',
                'value' => function(TemplateField $model){
                    $ret = [];

                    $ret [] = Html::a($direction = "up",
                        ['/ids_field/sort',
                            'returnto' => $_SERVER['REQUEST_URI'],
                            'direction' => $direction,
                            'template_field_id' => $model->id],
                        ['data-pjax' => 0, 'title' => 'Move up this element']);

                    $ret [] = "&nbsp;/&nbsp;";

                    $ret [] = Html::a($direction = "down",
                        ['/ids_field/sort',
                            'returnto' => $_SERVER['REQUEST_URI'],
                            'direction' => $direction,
                            'template_field_id' => $model->id],
                        ['data-pjax' => 0, 'title' => 'Move down this element']);

                    $ret [] = "&nbsp;/&nbsp;";

                    $ret [] = Html::a("++",
                        ['/ids_template/field/create',
                            'template_name_id' => aGet('id'),
                            'returnto' => aTabRequestUri("fields"),
                            'sort' => $model->sort + 1
                        ],
                        ['data-pjax' => 0]);

                    return implode($ret);
                }
            ] : [ 'visible' => false ],

            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, TemplateField $model, $key, $index, $column) {
                    $returnto = null;
                    if(aIfModuleControllerAction("ids_template","name","view")){
                        $returnto = "/ids_template/name/view?id=".aGet("id");
                    }
                    return Url::toRoute(['/ids_template/field/'.$action, 'id' => $model->id, 'returnto' => $returnto]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
