<?php

use app\modules\ids_template\models\TemplateField;
use app\modules\ids_template\models\TemplateName;
use kartik\select2\Select2;
use yii\bootstrap5\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\TemplateField $model */
/** @var yii\widgets\ActiveForm $form */

$types = [];
$types [] = "time";
$types [] = "date";
$types [] = "textarea";
$types [] = "select";
$types [] = "ckeditor_4_13_1";

asort($types);

?>

<?= $form->field($model, 'html_element')->textInput(['maxlength' => true]) ?>

<br>
<ul>Possible values:
    <?php foreach ($types as $type): ?>
        <li><?= $type?> (<a href="#" onclick="setIntoField('<?= $type ?>');return false;">insert</a>)</li>
    <?php endforeach; ?>
</ul>

<script>
    function setIntoField(iVal){
        place = document.getElementById("templatefield-html_element");
        place.value = iVal;
        return false;
    }
</script>