<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\TemplateField $model */

$this->title = Yii::t('app', 'Create Template Field');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Template Names'),
    'url' => ['/ids_template']];

if($model->templateName){
    $this->params['breadcrumbs'][] = ['label' => $model->templateName->name,
        'url' => ['/ids_template/name/view', 'id' => $model->template_name_id, 'tab' => 'fields']
    ];
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="template-field-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
