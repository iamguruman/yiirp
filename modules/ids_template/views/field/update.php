<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\TemplateField $model */

$this->title = Yii::t('app', 'Update Template Field: {name}', [
    'name' => $model->title,
]);

/*if(aIfModuleControllerAction("", "", "")){
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Template Fields'), 'url' => ['index']];
}*/

if($model->templateName){
    $this->params['breadcrumbs'][] = ['label' => $model->templateName->name,
        'url' => ['/ids_template/name/view', 'id' => $model->template_name_id]];
}

$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="template-field-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
