<?php

use app\modules\ids_template\models\TemplateField;
use app\modules\ids_template\models\TemplateName;
use kartik\select2\Select2;
use yii\bootstrap5\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\TemplateField $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="template-field-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'template_name_id')->widget(Select2::className(), [
        'data' => TemplateName::find()->select(['name', 'id'])->indexBy('id')->column(),
        'options' => ['placeholder' => 'Select a template ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <br>

    <?php
        if(aGet('sort')){
            $previousField = TemplateField::find()
                ->andWhere(['<', 'sort', aGet('sort')])
                ->andWhere(['template_name_id' => $model->template_name_id])
                ->one();
        } else {
            $previousField = TemplateField::find()
                ->andWhere(['template_name_id' => $model->template_name_id])
                ->orderBy(['sort' => SORT_DESC])
                ->one();
        }
    ?>
    <div class="form-group field-templatefield-title">
        <label class="control-label" for="templatefield-title">Previous Field</label>
        <div>
            <?php if($previousField): ?>
                <?= $previousField->title ?>
            <?php else: ?>
                No prevoius field, possible this is first field
            <?php endif; ?>
        </div>
        <div class="help-block"></div>
    </div>

    <br>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <br>

    <?= $form->field($model, 'hint')->textInput(['maxlength' => true]) ?>

    <br>

    <?= $form->field($model, 'hint_insertion_set')->textInput(['maxlength' => true]) ?>

    <br>

    <?= $form->field($model, 'hint_insertion_insert')->textInput(['maxlength' => true]) ?>

    <?php

        $type_sql = "
            SELECT TABLE_NAME 
            FROM information_schema.TABLES 
            WHERE TABLE_SCHEMA = 'yiirp' 
              AND TABLE_NAME LIKE '%ids_value_%' 
            ORDER BY TABLE_NAME ASC
        ";

        $types = Yii::$app->db->createCommand($type_sql)->queryColumn();

        $xType = [];
        foreach ($types as $type){
            $type = str_replace("ids_value_", "", $type);
            $xType [$type] = $type;
        }
    ?>
    <br>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <br>

    <?= Tabs::widget(['items' => [
        [
            'label' => 'Field Store',
            'active' => true,
            'content' => "<br>"
                .$form->field($model, 'value_field')->widget(\kartik\select2\Select2::className(), [
                    'data' => $xType,
                    'options' => ['placeholder' => 'Select a field ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])
                ."<br>".$form->field($model, 'this_is_telnumber')->checkbox()
                ."<br>".$form->field($model, 'this_is_url')->checkbox()
                ."<br>".$form->field($model, 'this_is_email')->checkbox()

        ],

        [
            'label' => 'Html element',
            'active' => false,
            'content' => "<br>".$this->render("_form_html_element.php", ['model' => $model, 'form' => $form])
        ],

        [
            'label' => 'Default Value',
            'active' => false,
            'content' => "<br>"
                .$form->field($model, 'default_value')->textInput(['maxlength' => true])
        ],

        [
            'label' => 'CRUD Index',
            'active' => false,
            'content' => "<br>"
                .$form->field($model, "crudview_index")->textarea(['rows' => 10])
                ."<br>"
                ."<br>"
                . $form->field($model, 'show_as_column_at_template_list')
                    ->checkbox()
                    ->hint("Will show this field on items list table (CRUD Index page)")
                ."<br>"
                . $form->field($model, 'is_searchable')
                    ->checkbox()
                    ->hint("You can search this field on items list table (CRUD Index Page)")
        ],

        [
            'label' => 'CRUD View',
            'active' => false,
            'content' => "<br>"
                . $form->field($model, "crudview_view")->textarea(['rows' => 10])
                ."<br>"
                ."<br>"
                .$form->field($model, 'sort')->textInput()
        ],

        [
            'label' => 'Auto increment',
            'active' => false,
            'content' => "<br>"
                . $form->field($model, 'auto_increment')->checkbox()
                ."<br>"
                ."<br>"
                . $form->field($model, 'auto_increment_template')->textInput()
        ],

        [
            'label' => 'Select',
            'active' => false,
            'content' => "<br>"
                . $form->field($model, 'source_template_name_id')->widget(Select2::className(), [
                    'data' => TemplateName::find()->select(['name', 'id'])->indexBy('id')->column(),
                    'options' => ['placeholder' => 'Select a template ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])
        ],

        [
            'label' => 'onChange',
            'active' => false,
            'content' => "<br>"
                . $form->field($model, 'onchange')
                    ->textarea(['rows' => 10])
                    ->hint("You can insert Javascript code, this code will used onChange in html element")
        ],

        [
            'label' => 'onLoad',
            'active' => false,
            'content' => "<br>"
                . $form->field($model, 'onload')
                    ->textarea(['rows' => 10])
                    ->hint("You can insert Javascript code, this code will used onLoad in html element")
        ],

        [
            'label' => 'Tree',
            'active' => false,
            'content' => "<br>"
                . $form->field($model, 'show_in_tree')
                    ->checkbox()
                    ->hint("Show this filed in Tree list")
        ],

        [
            'label' => 'History',
            'active' => false,
            'content' => "<br>"
                . $form->field($model, 'saving_changes')
                    ->checkbox()
                    ->hint("Save changes, will start show modification history")
        ],
    ]]) ?>

    <br>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
