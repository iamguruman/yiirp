<?php

use app\modules\ids_template\models\IdsPrintTemplate;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\IdsPrintTemplateSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

if(aIfModuleControllerAction("ids_template", "print-template", "index")){
    $this->title = Yii::t('app', 'Ids Print Templates');
    $this->params['breadcrumbs'][] = $this->title;
}

?>
<div class="ids-print-template-index">

    <?php if(aIfModuleControllerAction("ids_template", "print-template", "index")): ?>
        <h1><?= Html::encode($this->title) ?></h1>
    <?php endif; ?>

    <p>
        <?php if(
               aIfModuleControllerAction("ids_template", "print-template", "index")
            or aIfModuleControllerAction("ids_template", "name", "view")
        ): ?>
            <?= Html::a(Yii::t('app', 'Create Ids Print Template'),
                ['/ids_template/print-template/create', 'template_name_id' => Yii::$app->request->get("id")],
                ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',

            aIfModuleControllerAction("ids_template", "print-template", "index") ? [
                'attribute' => 'idsTemplateName.name',
            ] : ['visible' => false],

            'name',

            aIfModuleControllerAction("ids", "view", "index") ? [
                'label' => '',
                'format' => 'raw',
                'value' => function($model){
                    return Html::a("Print",
                        ['/ids/print', 'id' => aGet('id'), 'print_template' => $model->id],
                        ['class' => 'btn btn-primary', 'data-pjax' => 0, 'target' => '_blank']);
                }
            ] : ['visible' => false],

            aIfModuleControllerAction("ids", "view", "index") ? [
                'label' => '',
                'format' => 'raw',
                'value' => function(IdsPrintTemplate $model){
                    return Html::a("Edit template",
                        ['/ids_template/print-template/update', 'id' => $model->id],
                        ['class' => 'btn btn-primary', 'data-pjax' => 0, 'target' => '_blank']);
                }
            ] : ['visible' => false],

            //'html_content',
            (
               aIfModuleControllerAction("ids_template", "print-template", "index")
            or aIfModuleControllerAction("ids_template", "name", "view")
            )? [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, IdsPrintTemplate $model, $key, $index, $column) {
                    return Url::toRoute(["/ids_template/print-template/".$action, 'id' => $model->id]);
                 }
            ] : ['visible' => false],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
