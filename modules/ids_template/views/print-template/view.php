<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\IdsPrintTemplate $model */

$this->title = $model->name;

$this->params['breadcrumbs'][] = [
        'label' => Yii::t('app', 'Template Names'),
    'url' => ['/ids_template']];

if($model->idsTemplateName){
    $this->params['breadcrumbs'][] = ['label' => $model->idsTemplateName->name,
        'url' => ['/ids_template/name/view',
            'id' => $model->ids_template_name_id,
            'tab' => 'print-template'
        ]
    ];
}

$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ids-print-template-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'idsTemplateName.name',
            'name',
            'html_content:html',
        ],
    ]) ?>

</div>
