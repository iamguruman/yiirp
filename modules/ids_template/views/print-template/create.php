<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\IdsPrintTemplate $model */

$this->title = Yii::t('app', 'Create Ids Print Template');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Template Names'),
    'url' => ['/ids_template']];

if($model->idsTemplateName){
    $this->params['breadcrumbs'][] = ['label' => $model->idsTemplateName->name,
        'url' => ['/ids_template/name/view', 'id' => $model->ids_template_name_id, 'tab' => 'print-template']
    ];
}

/*$this->params['breadcrumbs'][] = [
        'label' => Yii::t('app', 'Ids Print Templates'),
    'url' => ['index']];*/

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ids-print-template-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
