<?php

use app\modules\ids_template\models\TemplateName;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\IdsTemplateLinks $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ids-template-links-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ids_template_name_id')->widget(Select2::className(), [
        'data' => TemplateName::find()->select(['name', 'id'])->indexBy('id')->column(),
        'options' => ['placeholder' => 'Select a template ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <br>

    <?= $form->field($model, 'ids_template_field_id')->textInput() ?>

    <br>

    <?= $form->field($model, 'text')->textInput(['maxlength' => true]) ?>

    <br>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <br>

    <?= $form->field($model, 'link')->textarea(['maxlength' => true]) ?>

    <br>

    <?php foreach ($model->idsTemplateName->templateFields as $templateField): ?>
        <li>{<?= $templateField->id ?>} (<a href="#" onclick="insertIntoField('{<?= $templateField->id ?>}');return false;">insert</a>) - <?= $templateField->title ?></li>
    <?php endforeach; ?>

    <script>
        function insertIntoField(iVal){
            place = document.getElementById("idstemplatelinks-link");
            place.value += " " + iVal;
            return false;
        }
    </script>

    <br>

    <?= $form->field($model, 'target')->textInput(['maxlength' => true])
        ->hint("_blank, _self, _parent, _top") ?>

    <br>

    <?= $form->field($model, 'redirect')->checkbox()
        ->hint("Open link throw /redirect?url=") ?>

    <br>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
