<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\IdsTemplateLinks $model */

$this->title = Yii::t('app', 'Create Ids Template Links');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ids Template Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ids-template-links-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
