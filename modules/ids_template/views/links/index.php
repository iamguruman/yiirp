<?php

use app\modules\ids_template\models\IdsTemplateLinks;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\IdsTemplateLinksSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

if(aIfModuleControllerAction("ids_template", "links", "index")){
    $this->title = Yii::t('app', 'Ids Template Links');
    $this->params['breadcrumbs'][] = $this->title;
}
?>
<div class="ids-template-links-index">

    <?php if(aIfModuleControllerAction("ids_template", "links", "index")): ?>
        <h1><?= Html::encode($this->title) ?></h1>
    <?php endif; ?>

    <p>
        <?php if(aIfModuleControllerAction("ids_template", "links", "index")): ?>
            <?= Html::a(Yii::t('app', 'Create Ids Template Links'),
                ['create'],
                ['class' => 'btn btn-success']) ?>
        <?php endif; ?>

        <?php if(aIfModuleControllerAction("ids_template", "name", "view")): ?>
            <?= Html::a(Yii::t('app', 'Create Ids Template Links'),
                ['/ids_template/links/create', 'template_id' => aGet('id')],
                ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'ids_template_name_id',
            //'ids_template_field_id',
            'text',
            'title',
            //'link',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, IdsTemplateLinks $model, $key, $index, $column) {
                    return Url::toRoute(['/ids_template/links/'.$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
