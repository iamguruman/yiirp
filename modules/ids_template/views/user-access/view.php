<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\IdsTemplateUserAccess $model */

$this->title = $model->id;

if($model->idsTemplateName){
    $this->params['breadcrumbs'][] =
        ['label' => $model->idsTemplateName->name,
        'url' => ['/ids_template/name/view', 'id' => $model->idsTemplateName->id, 'tab' => 'user_access']];
}

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ids Template User Accesses'),
    'url' => ['index']];

$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ids-template-user-access-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idsTemplateName.name',
            'sysUser.username',

            'can_create:boolean',
            'can_read_only_self_created:boolean',
            'can_update_self_created:boolean',
            'can_update_other_created:boolean',
            'can_edit_template:boolean',
        ],
    ]) ?>

</div>
