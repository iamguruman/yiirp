<?php

use app\modules\ids_template\models\TemplateName;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\IdsTemplateUserAccess $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ids-template-user-access-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ids_template_name_id')->widget(Select2::className(), [
        'data' => TemplateName::find()->select(['name', 'id'])->indexBy('id')->column(),
        'options' => ['placeholder' => 'Select a template ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <br>

    <?= $form->field($model, 'username')->textInput() ?>

    <br>

    <?= $form->field($model, 'can_list')->checkbox() ?>

    <br>

    <?= $form->field($model, 'can_create')->checkbox() ?>

    <br>

    <?= $form->field($model, 'can_read_only_self_created')->checkbox() ?>

    <br>

    <?= $form->field($model, 'can_update_self_created')->checkbox() ?>

    <br>

    <?= $form->field($model, 'can_update_other_created')->checkbox() ?>

    <br>

    <?= $form->field($model, 'can_edit_template')->checkbox() ?>

    <br>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
