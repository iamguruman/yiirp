<?php

use app\modules\ids_template\models\IdsTemplateUserAccess;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\IdsTemplateUserAccessSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

if(aIfModuleControllerAction("ids_template", "user-access", "index")){
    $this->title = Yii::t('app', 'Ids Template User Accesses');
    $this->params['breadcrumbs'][] = $this->title;
}
?>
<div class="ids-template-user-access-index">

    <?php if(aIfModuleControllerAction("ids_template", "user-access", "index")): ?>
        <h1><?= Html::encode($this->title) ?></h1>
    <?php endif; ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Ids Template User Access'),
            ['/ids_template/user-access/create',
                'template_id' => aIfModuleControllerAction("ids_template", "name", "view") ?
                    aGet('id')
                : null,
            ],
            ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'created_by',

            aIfModuleControllerAction("ids_template", "name", "view") ? ['visible' => false] : [
                'attribute' => 'created_at'
            ],

            //'updated_by',
            //'updated_at',
            //'markdel_by',
            //'markdel_at',

            aIfModuleControllerAction("ids_template", "name", "view") ? ['visible' => false] : [
                'attribute' => 'idsTemplateName.name'
            ],

            'sysUser.username',
            'can_list',
            'can_create',
            'can_read_only_self_created',
            'can_update_self_created',
            'can_update_other_created',
            'can_edit_template',

            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, IdsTemplateUserAccess $model, $key, $index, $column) {
                    return Url::toRoute(['/ids_template/user-access/'.$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
