<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\TemplateName $model */

$this->title = Yii::t('app', 'Create Template Name');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Template Names'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="temaplate-name-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
