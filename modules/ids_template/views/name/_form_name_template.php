<?php

use yii\bootstrap5\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\TemplateName $model */
/** @var yii\widgets\ActiveForm $form */
?>

<?= $form->field($model, 'name_template')->textInput() ?>

<br>

<?php foreach ($model->templateFields as $templateField): ?>
    <li>{<?= $templateField->id ?>} (<a href="#" onclick="insertIntoField('{<?= $templateField->id ?>}');return false;">insert</a>) - <?= $templateField->title ?></li>
<?php endforeach; ?>

<script>
    function insertIntoField(iVal){
        place = document.getElementById("templatename-name_template");
        place.value += " " + iVal;
        return false;
    }
</script>