<?php

use app\modules\ids_template\models\TemplateName;
use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\TemplateName $model */

/** @var \app\modules\ids_template\models\IdsPrintTemplateSearch $printTemaplateSearchModel */
/** @var \yii\data\ActiveDataProvider $printTemaplateDataProvider */

/** @var \app\modules\ids_template\models\TemplateFieldSearch $fieldSearchModel */
/** @var \yii\data\ActiveDataProvider $fieldDataProvider */

/** @var \app\modules\ids_template\models\IdsFlowSearch $flowSearchModel */
/** @var \yii\data\ActiveDataProvider $flowDataProvider */

/** @var \app\modules\ids_template\models\IdsTemplateLinks $linksSearchModel */
/** @var \yii\data\ActiveDataProvider $linksDataProvider */

/** @var \app\modules\ids_import\models\IdsImportNameSearch $importSearchModel */
/** @var \yii\data\$importDataProvider $linksDataProvider */

/** @var \app\modules\ids_template\models\IdsTemplateUserAccessSearch $userAccessSearchModel */
/** @var \yii\data\ActiveDataProvider $userAccessDataProvider */

/** @var \app\modules\ids_template\models\IdsTemplateStatusNameSearch $statusNameSearchModel */
/** @var \yii\data\ActiveDataProvider $statusNameDataProvider */

$this->title = $model->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Template Names'),
    'url' => ['/ids_template']];

$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="temaplate-name-view">

    <h1>
        <?php if(file_exists(Yii::getAlias("@app/web/tplicons/{$model->id}.png"))): ?>
            <img src='/tplicons/<?= $model->id ?>.png' style='max-height: 50px;'>
        <?php endif; ?>
        <?= Html::encode($this->title) ?>
    </h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update template'),
            ['update', 'id' => $model->id],
            ['class' => 'btn btn-primary']) ?>

        <?php /*= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) */?>

        <?= Html::a(Yii::t('app', 'Create new item'),
            ['/ids/create', 'template_id' => $model->id],
            ['class' => 'btn btn-success']) ?>

        <?= Html::a(Yii::t('app', 'View items'),
            ['/ids/', 'template_id' => $model->id],
            ['class' => 'btn btn-warning']) ?>

        <?= Html::a(Yii::t('app', 'View Kanban Table'),
            ['/ids_kanban/', 'template_id' => $model->id, 'view' => 'table'],
            ['class' => 'btn btn-warning']) ?>

        <?= Html::a(Yii::t('app', 'View Kanban Tree'),
            ['/ids_kanban/', 'template_id' => $model->id],
            ['class' => 'btn btn-warning']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'hint',
        ],
    ]) ?>

    <?= \yii\bootstrap5\Tabs::widget(['items' => [
        [
            'label' => 'Details',
            'active' => false,
            'content' => "<br>".$this->render("view_detail", ['model' => $model])
        ],

        [
            'label' => "User Access ({$userAccessDataProvider->totalCount})",
            'active' => aGet('tab') == 'user_access' ? true : false,
            'content' => "<br>".$this->render("@app/modules/ids_template/views/user-access/index.php", [
                    'searchModel' => $userAccessSearchModel,
                    'dataProvider' => $userAccessDataProvider,
                ])
        ],

        [
            'label' => "Fields ({$fieldDataProvider->totalCount})",
            'content' => "<br>".$this->render("../field/index", [
                    'searchModel' => $fieldSearchModel,
                    'dataProvider' => $fieldDataProvider,
                ])
        ],

        [
            'label' => "Print template ({$printTemaplateDataProvider->totalCount})",
            'active' => aGet('tab') == 'print-template' ? true : null,
            'content' => "<br>".$this->render("../print-template/index", [
                    'searchModel' => $printTemaplateSearchModel,
                    'dataProvider' => $printTemaplateDataProvider,
                ])
        ],

        [
            'label' => "Flow ({$flowDataProvider->totalCount})",
            'active' => aGet('tab') == 'flow' ? true : null,
            'content' => "<br>".$this->render("@modules/ids_flow/views/flow/index", [
                    'searchModel' => $flowSearchModel,
                    'dataProvider' => $flowDataProvider,
                ])
        ],

        [
            'label' => "Links ({$linksDataProvider->totalCount})",
            'active' => aGet('tab') == 'links' ? true : null,
            'content' => "<br>".$this->render("../links/index", [
                    'searchModel' => $linksSearchModel,
                    'dataProvider' => $linksDataProvider,
                ])
        ],

        [
            'label' => "Import ({$importDataProvider->totalCount})",
            'active' => aGet('tab') == 'import' ? true : null,
            'content' => "<br>".$this->render("@app/modules/ids_import/views/default/index.php", [
                    'searchModel' => $importSearchModel,
                    'dataProvider' => $importDataProvider,
                ])
        ],

        [
            'label' => "Extra Module",
            'active' => aGet('tab') == 'user_access' ? true : null,
            'content' => "<br>".DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'extra_module_action_view',
                    ],
                ])
        ],

        [
            'label' => "CRUD Index",
            'active' => aGet('tab') == 'user_access' ? true : null,
            'content' => "<br>".DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'show_field_name_on_crud_index:boolean',
                    ],
                ])
        ],

        [
            'label' => "Status name ({$statusNameDataProvider->totalCount})",
            'active' => aGet('tab') == 'status_name' ? true : null,
            'content' => "<br>".$this->render("@app/modules/ids_template/views/status-name/index.php", [
                    'searchModel' => $statusNameSearchModel,
                    'dataProvider' => $statusNameDataProvider,
                ])
        ],
    ]]) ?>

</div>
