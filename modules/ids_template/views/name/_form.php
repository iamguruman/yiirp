<?php

use yii\bootstrap5\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\TemplateName $model */
/** @var yii\widgets\ActiveForm $form */
?>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <br>

    <?= $form->field($model, 'hint')->textarea(['rows' => 3]) ?>

    <br>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <br>

    <?= Tabs::widget(['items' => [
        [
            'label' => "Name template",
            'content' => "<br>". $this->render("_form_name_template", ['model' => $model, 'form' => $form])
        ],

        [
            'label' => "Icon",
            'content' => "<br>". $this->render("_form_icon", ['model' => $model, 'form' => $form])
        ],

        [
            'label' => "Module",
            'content' => "<br>". $this->render("_form_module", ['model' => $model, 'form' => $form])
        ],

        [
            'label' => "CRUD Index",
            'content' => "<br>".
                $form->field($model, 'show_field_name_on_crud_index')->checkbox()
        ],
    ]])?>

    <?php ActiveForm::end(); ?>
