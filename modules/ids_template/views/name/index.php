<?php

use app\modules\ids_template\models\TemplateName;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\TemplateNameSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app', 'Template Names');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="temaplate-name-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Template Name'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',

            [
                'label' => 'Icon',
                'format' => 'raw',
                'value' => function(TemplateName $model){
                    $icon = Yii::getAlias("@app/web/tplicons/{$model->id}.png");
                    if(file_exists($icon)){
                        return "<img src='/tplicons/{$model->id}.png' style='max-height: 50px;'>";
                    }
                }
            ],

            'name',

            [
                'label' => 'List',
                'format' => 'raw',
                'value' => function(TemplateName $model){
                    return Html::a("Open",
                        ['/ids', 'template_id' => $model->id],
                        ['data-pjax' => 0,
                            'class' => 'btn btn-primary']);
                }
            ],

            [
                'label' => 'Kanban',
                'format' => 'raw',
                'value' => function(TemplateName $model){
                    return Html::a("Open",
                        ['/ids_kanban', 'template_id' => $model->id],
                        ['data-pjax' => 0,
                            'class' => 'btn btn-primary']);
                }
            ],

            [
                'label' => 'Settings',
                'format' => 'raw',
                'value' => function(TemplateName $model){
                    $ret = [];

                    if($model->owner_user_id == aUserMyId()){
                        $ret [] = Html::a("Settings",
                            ['/ids_template/name/view', 'id' => $model->id],
                            ['data-pjax' => 0, 'class' => 'btn btn-primary']);
                    } else {
                        $ret [] = "no access";
                    }

                    return implode("", $ret);
                }
            ],
            /*[
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, TemplateName $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],*/
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
