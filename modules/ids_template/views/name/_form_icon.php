<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\TemplateName $model */
/** @var yii\widgets\ActiveForm $form */
?>

<?= Html::a("Search icon by Name at icons8.com",
    ['#'],
    ['class' => 'btn btn-primary',
        'onClick' => 'findIcon8(); return false;']) ?>


<?= Html::a("Search icon by Name at Flaticon",
    ['#'],
    ['class' => 'btn btn-primary',
        'onClick' => 'findFlaticon(); return false;']) ?>

<br>
<br>

<?= $form->field($model, 'iconFile')->fileInput(['multiple' => false]) ?>

<br>

<?php $icon = Yii::getAlias("@app/web/tplicons/{$model->id}.png"); if(file_exists($icon)): ?>
    Icon:
    <img src='/tplicons/<?= $model->id ?>.png' style='max-height: 50px;'>

    <br>
    <br>

    <?= $form->field($model, 'removeIcon')->checkbox() ?>
<?php endif; ?>

<br>


<script>
    function findIcon8(){
        let url = "https://icons8.com/icons/set/" + document.getElementById('templatename-name').value;
        window.open(url, '_blank').focus();
    }
    function findFlaticon(){
        let url = "https://www.flaticon.com/ru/search?word=" + document.getElementById('templatename-name').value;
        window.open(url, '_blank').focus();
    }
</script>