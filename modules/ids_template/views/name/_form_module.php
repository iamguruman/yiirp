<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\TemplateName $model */
/** @var yii\widgets\ActiveForm $form */
?>

<?= $form->field($model, 'extra_module_action_view')->textInput() ?>

