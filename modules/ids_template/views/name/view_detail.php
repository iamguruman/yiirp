<?php

use app\modules\ids_template\models\TemplateName;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\TemplateName $model */

?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',

        [
            'attribute' => 'owner_user_id',
            'value' => function(TemplateName $model){

                if($model->ownerUser){
                    return $model->ownerUser->username;
                }

            }
        ],

        [
            'attribute' => 'name_template',
            'value' => function(TemplateName $model){
                $ret = $model->name_template;

                foreach ($model->templateFields as $templateField){
                    $ret = str_replace("{".$templateField->id."}", "{".$templateField->title."}", $ret);
                }

                return $ret;

            }
        ],

        [
            'label' => 'Icon',
            'format' => 'raw',
            'value' => function(TemplateName $model){
                $icon = Yii::getAlias("@app/web/tplicons/{$model->id}.png");
                if(file_exists($icon)){
                    return "<img src='/tplicons/{$model->id}.png' style='max-height: 50px;'>";
                }
            }
        ],
    ],
]) ?>
