<?php

use app\modules\ids_template\models\IdsTemplateUserAccess;
use app\modules\ids_template\models\TemplateName;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\IdsTemplateStatusName $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ids-template-status-name-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ids_template_name_id')->widget(Select2::className(), [
        'data' => TemplateName::find()
            ->select(['name', TemplateName::tableName().'.id'])
            ->indexBy(TemplateName::tableName().'.id')
            ->joinWith('usersAccess')
            ->andWhere([IdsTemplateUserAccess::tableName().'.sys_user_id' => aUserMyId()])
            ->column(),
        'options' => [
            'placeholder' => 'Select a template ...',
            'onchange' => "changeTemplate();"
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <br>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <br>

    <?= $form->field($model, 'sort')->textInput() ?>

    <br>

    <?= $form->field($model, 'style')->textarea(['rows' => 2]) ?>

    <br>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <br>

    <?= $form->field($model, 'show_counter')->checkbox() ?>

    <br>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
