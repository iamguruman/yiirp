<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\IdsTemplateStatusName $model */

$this->title = Yii::t('app', 'Update Ids Template Status Name: {name}', [
    'name' => $model->name,
]);

//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ids Template Status Names'), 'url' => ['index']];

if($model->idsTemplateName){
    $this->params['breadcrumbs'][] = ['label' => $model->idsTemplateName->name,
        'url' => ['/ids_template/name/view', 'id' => $model->ids_template_name_id, 'tab' => 'status_name']];
}

$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];

$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ids-template-status-name-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
