<?php

use app\modules\ids_template\models\IdsTemplateStatusName;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\IdsTemplateStatusNameSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

if(aIfModuleControllerAction("ids_template", "status-template", "index")){
    $this->title = Yii::t('app', 'Ids Template Status Names');
    $this->params['breadcrumbs'][] = $this->title;
}
?>
<div class="ids-template-status-name-index">

    <?= aHtmlHeader($this->title, "ids_template", "status-template", "index") ?>

    <p>
        <?php if(aIfModuleControllerAction("ids_template", "status-template", "index")): ?>
            <?= Html::a(Yii::t('app', 'Create Ids Template Status Name'), ['create'], ['class' => 'btn btn-success']) ?>
        <?php endif; ?>

        <?php if(aIfModuleControllerAction("ids_template", "name", "view")): ?>
            <?= Html::a(Yii::t('app', 'Create Ids Template Status Name'),
                ['/ids_template/status-name/create', 'template_id' => aGet('id')],
                ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'created_by',
            //'created_at',
            //'updated_by',
            //'updated_at',
            //'markdel_by',
            //'markdel_at',

            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function(IdsTemplateStatusName $model){
                    return "<div style='{$model->style}'>{$model->name}</div>";
                }
            ],

            aIfModuleControllerAction("ids_template", "name", "view" )
                ? ['visible' => false] : [
                'attribute' => 'idsTemplateName.name',
            ],

            'description:ntext',

            //'sort',

            [
                'label' => '',
                'format' => 'raw',
                'value' => function(IdsTemplateStatusName $model){
                    return Html::a("++",
                        ['/ids_template/status-name/create',
                            'template_id' => $model->ids_template_name_id,
                            'sort' => $model->sort + 1],
                        ['data-pjax' => 0]);
                }
            ],

            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, IdsTemplateStatusName $model, $key, $index, $column) {
                    return Url::toRoute(["/ids_template/status-name/".$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
