<?php

use app\modules\ids_template\models\IdsTemplateStatusName;
use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\IdsTemplateStatusName $model */

$this->title = $model->name;

if($model->idsTemplateName){
    $this->params['breadcrumbs'][] = ['label' => $model->idsTemplateName->name,
        'url' => ['/ids_template/name/view', 'id' => $model->ids_template_name_id, 'tab' => 'status_name']];
}

//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ids Template Status Names'), 'url' => ['index']];

$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ids-template-status-name-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',

            [
                'attribute' => 'style',
                'format' => 'raw',
                'value' => function(IdsTemplateStatusName $model){
                    return "<div style='{$model->style}'>{$model->style}</div>";
                }
            ],

            'idsTemplateName.name',
            'description:ntext',
            'sort',
        ],
    ]) ?>

</div>
