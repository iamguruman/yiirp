<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\ids_template\models\IdsTemplateStatusName $model */

$this->title = Yii::t('app', 'Create Ids Template Status Name');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ids Template Status Names'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ids-template-status-name-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
