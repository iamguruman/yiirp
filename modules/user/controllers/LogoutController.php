<?php

namespace app\modules\user\controllers;

use app\models\User;
use yii\web\Controller;
use Yii;

class LogoutController extends Controller
{

    public function actionIndex(){

        if (!Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
        }

        return $this->goHome();

    }

}