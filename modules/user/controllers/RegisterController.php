<?php

namespace app\modules\user\controllers;

use app\models\LoginForm;
use app\modules\user\models\RegisterForm;
use app\modules\user\models\User;
use yii\filters\VerbFilter;
use yii\web\Controller;
use Yii;

class RegisterController extends Controller
{

    public function actionIndex(){

        $model = new RegisterForm();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {

                $count = User::find()->andWhere(['or', ['username' => $model->username], ['email' => $model->email]])->count();

                if($count == 0){

                    if($model->password1 == $model->password2){

                        $userModel = new User();
                        $userModel->password_hash = md5($model->password1);
                        $userModel->email = $model->email;
                        $userModel->username = $model->username;

                        if ($userModel->save()) {
                            return $this->redirect(['/user/login']);
                        }

                    } else {

                        ddd('passes is not equal');

                    }

                } else {

                    ddd('username or email is already registred');
                }

            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);

    }

}