<?php

namespace app\modules\user\controllers;

use app\models\LoginForm;
use yii\filters\VerbFilter;
use yii\web\Controller;
use Yii;

class LoginController extends Controller
{

    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'login' => ['POST'],
                    ],
                ],
            ]
        );
    }
    public function actionIndex(){

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new \app\modules\user\models\LoginForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->login()) {
                return $this->goBack();
            } else {

            }
        }

        $model->password = '';
        return $this->render('index', [
            'model' => $model,
        ]);

    }

}