<?php

namespace app\modules\user\controllers;

use app\models\User;
use yii\web\Controller;
use Yii;

class DefaultController extends Controller
{

    public function actionIndex(){

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            return $this->redirect("/user/login");
        }

    }

}