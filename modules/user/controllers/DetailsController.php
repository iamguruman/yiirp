<?php

namespace app\modules\user\controllers;

use app\models\User;
use yii\web\Controller;
use Yii;

class DetailsController extends Controller
{

    public function actionIndex(){

        if (!Yii::$app->user->isGuest) {

            $model = Yii::$app->user->identity;

            return  $this->render("view", ['model' => $model]);

        } else {
            return $this->redirect("/user/login");
        }

    }

}