<?php

namespace app\modules\user\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read \app\models\User|null $user
 *
 */
class RegisterForm extends Model
{
    public $username;
    public $password1;
    public $password2;

    public $email;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password1', 'password2', 'email'], 'required'],
            // rememberMe must be a boolean value
        ];
    }
}
