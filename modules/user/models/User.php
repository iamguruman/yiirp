<?php

namespace app\modules\user\models;

use app\modules\users\models\UserKey;
use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "sys_user".
 *
 * @property int $id
 * @property int|null $created_by
 * @property string|null $created_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @property int|null $markdel_by
 * @property string|null $markdel_at
 * @property string $username
 * @property string $password_hash
 * @property string|null $email
 * @property string|null $cell_phone
 * @property string|null $firstname
 * @property string|null $lastname
 * @property string|null $middlename
 * @property int|null $blocked_by
 * @property string|null $blocked_at
 * @property string|null $blocked_reason
 *
 * @property-read string|null $default_sort
 *
 * @property integer|null $dialog_width_px
 * @property integer|null $dialog_height_px
 *
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sys_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['hide_markdel'], 'integer', 'max' => 1],
            [['dialog_height_px'], 'integer'],
            [['dialog_width_px'], 'integer'],

            [['default_sort'], 'string'],
            [['created_by', 'updated_by', 'markdel_by', 'blocked_by'], 'integer'],
            [['created_at', 'updated_at', 'markdel_at', 'blocked_at'], 'safe'],
            [['username', 'password_hash'], 'required'],
            [['username'], 'string', 'max' => 50],
            [['password_hash', 'email', 'cell_phone', 'firstname', 'lastname', 'middlename', 'blocked_reason'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'default_sort' => Yii::t('app', 'Default Sort'),
            'id' => Yii::t('app', 'ID'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'markdel_by' => Yii::t('app', 'Markdel By'),
            'markdel_at' => Yii::t('app', 'Markdel At'),
            'username' => Yii::t('app', 'Username'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'email' => Yii::t('app', 'Email'),
            'cell_phone' => Yii::t('app', 'Cell Phone'),
            'firstname' => Yii::t('app', 'Firstname'),
            'lastname' => Yii::t('app', 'Lastname'),
            'middlename' => Yii::t('app', 'Middlename'),
            'blocked_by' => Yii::t('app', 'Blocked By'),
            'blocked_at' => Yii::t('app', 'Blocked At'),
            'blocked_reason' => Yii::t('app', 'Blocked Reason'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    public static function checkLogin($username, $password)
    {
        return self::find()
            ->where(['username' => $username, 'password_hash' => md5($password)])
            ->one();
    }

    public static function findIdentity($id)
    {
        // TODO: Implement findIdentity() method.
        return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Implement findIdentityByAccessToken() method.
        $userId = UserKey::findOne(['key' => $token])->attributes['user_id'];
        return User::findOne($userId);
    }

    public function getId(): int
    {
        // TODO: Implement getId() method.
        return $this->id;
    }

    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
        return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
        return $this->authKey === $authKey;
    }

    public function getFullname(){

        if($this->lastname){

        }
    }
}
