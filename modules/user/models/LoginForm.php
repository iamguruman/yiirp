<?php

namespace app\modules\user\models;

use app\modules\user\models\User;
use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read \app\models\User|null $user
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if($user = User::find()
            ->andWhere(['username' => $this->username])
            ->andWhere(['password_hash' => md5($this->password)])
            ->one()
        ){
            return Yii::$app->user->login($user, $this->rememberMe ? 3600*24*30 : 0);

        }

        return false;
    }

    public function validatePassword($attribute, $params)
    {
        if ($this->_user === false) {
            $this->_user = \app\modules\user\models\User::checkLogin($this->username, $this->password);
            //die(var_dump($this->_user));
        }

        //die(var_dump($this->_user));
        return $this->_user;
    }

}
