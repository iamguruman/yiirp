<?php

namespace app\modules\pbx_webhook_type;

use app\components\ModuleAccess;
use yii\helpers\ArrayHelper;

/**
 * pbx_webhook_type module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\pbx_webhook_type\controllers';

    const moduleId = "pbx_webhook_type";
    
    const moduleUrl = "/".self::moduleId;

    const moduleTitle = "Типы событий МСН ВАТС";

    // удялет из меню со списком всех модулей
    //const removeFromModuleMenu = true;

    // удляет из индивидуального меню для модуля
    //const removeFromThisModuleMenu = true;

    public static function getBreadcrumbs($url = true){
        return [
            'label' => self::moduleTitle,
            'url' => $url ? [self::moduleUrl.'/default/index'] : null,
        ];
    }

    public static function moduleMenu(){
        $array = [
            'label' => self::moduleTitle,
            'items' => [
                [
                    'label' => "Список типов вебхук",
                    'url' => self::moduleUrl,
                ],
                [
                    'label' => '&nbsp;&nbsp;&nbsp;добавить',
                    'url' => self::moduleUrl.'/default/create',
                ],
            ]
        ];

        ArrayHelper::multisort($array, 'label');

        return $array;
    }

    /**
     * {@inheritdoc}
     */
    public function init()
    {

        //ModuleAccess::check($this->id);

        parent::init();

        // custom initialization code goes here
    }
}
