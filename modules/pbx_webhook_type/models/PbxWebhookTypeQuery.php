<?php

namespace app\modules\pbx_webhook_type\models;

/**
 * This is the ActiveQuery class for [[PbxWebhookType]].
 *
 * @see PbxWebhookType
 */
class PbxWebhookTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    public function init()
    {
        parent::init();

        if(aIfHideMarkdel()){
            $this->andWhere(['m_mcn_webhook_type.markdel_by' => null]);
        }
    }

    /**
     * {@inheritdoc}
     * @return PbxWebhookType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PbxWebhookType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
