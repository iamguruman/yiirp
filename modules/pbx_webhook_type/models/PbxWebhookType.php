<?php

namespace app\modules\pbx_webhook_type\models;

use app\modules\pbx_webhook\models\PbxWebhook;
use app\modules\user\models\User;
use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "m_mcn_webhook_type".
 *
 * @property int $id
 * @property string $created_at Добавлено когда
 *
 * @property int $created_by Добавлено кем
 * @property User $createdBy
 *
 * @property string $updated_at Изменено когда
 *
 * @property int $updated_by Изменено кем
 * @property User $updatedBy
 *
 * @property string $markdel_at Удалено когда
 *
 * @property int $markdel_by Удалено кем
 * @property User $markdelBy
 *
 * @property string $name Наименование
 *
 * @property-read string $urlIconToFiles - ссылка-иконка на вложения
 *
 * @property string $shortname
 * @property-read  PbxWebhook[] $webhooks
 *
 */
class PbxWebhookType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pbx_webhook_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['shortname'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 255],

            [['created_at', 'updated_at', 'markdel_at'], 'safe'],

            [['created_by', 'updated_by', 'markdel_by'], 'integer'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['markdel_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['markdel_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'shortname' => 'Короткое наименование',
            'id' => 'ID',

            'urlTo' => 'MCN Тип Webhook-а',
            'urlToBlank' => 'MCN Тип Webhook-а',

            'created_at' => 'Добавлено когда',

            'created_by' => 'Добавлено кем',
            'createdBy.lastNameWithInitials' => 'Добавлено кем',

            'updated_at' => 'Изменено когда',

            'updated_by' => 'Изменено кем',
            'updatedBy.lastNameWithInitials' => 'Изменено кем',

            'markdel_at' => 'Удалено когда',

            'markdel_by' => 'Удалено кем',
            'markdelBy.lastNameWithInitials' => 'Удалено кем',

            'name' => 'Наименование',
        ];
    }

    /**
     * ссылка на просмотр объекта
     * @return array
     */
    public function getUrlView($return = 'string'){
        
        $arr = [];
        
        $arr [] = ['/pbx_webhook_type/default/view', 'id' => $this->id];
        
        if($return == 'array'){
            return $arr;
        } elseif ($return == 'string'){
            return "/pbx_webhook_type/default/view?id={$this->id}";
        } else {
            return $arr;
        }
    }

    /**
     * ссылка к списку объектов
     * @return array
     */
    public function getUrlIndex(){
        return ['/pbx_webhook_type/default/index'];
    }

    public function getUrlTo($target = null){
        return Html::a(str_replace("&nbsp;", " ", $this->getTitle()),
            $this->getUrlView(),
            ['target' => $target, 'data-pjax' => 0]);
    }

    /**
     * получить заголовок объекта
     * @return string
     */
    public function getTitle(){

        $ret = [];
        
        if($this->name)
            $ret [] = $this->name;
        
        if(count($ret) == 0){
            $ret [] = "Без названия";
        }
        
        $ret [] = $this->getUrlIconToFiles();
        
        return implode(' ', $ret);
    
    }
    
    public function getUrlIconToFiles(){

        return;
    }

    public function getUrlToBlank(){
        return $this->getUrlTo('_blank');
    }

    public function getBreadcrumbs(){
        return [
            'label' => $this->getTitle(),
            'url' => $this->getUrlView()
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarkdelBy()
    {
        return $this->hasOne(User::className(), ['id' => 'markdel_by']);
    }

    /**
     * {@inheritdoc}
     * @return PbxWebhookTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PbxWebhookTypeQuery(get_called_class());
    }

    public function getWebhooks(){
        return $this->hasMany(PbxWebhook::className(), ['webhook_type_id' => 'id']);
    }
}
