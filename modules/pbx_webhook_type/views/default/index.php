<?php

use app\modules\pbx_webhook_type\models\PbxWebhookType;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\pbx_webhook_type\models\PbxWebhookTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$module = app\modules\pbx_webhook_type\Module::moduleId;
$controller = "default";
$action = "index";

if(aIfModuleControllerAction($module, $controller, $action)){

    $this->title = \app\modules\pbx_webhook_type\Module::moduleTitle;

    $this->params['breadcrumbs'][] = $this->title;
}

?>
<div class="pbx_webhook_type-index">

    <?= aHtmlHeader($this->title, $module, $controller, $action) ?>

    <p>
        <?= aIfModuleControllerAction($module, $controller, $action) ?
            Html::a('Добавить', ["/{$module}/{$controller}/create"], ['class' => 'btn btn-success'])
            : null  ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute' => 'id',
                'headerOptions' => ['style' => 'width:100px;'],
                'format' => 'raw', 'value' => function($model) { return aGridVIewColumnId($model); }],
            
            //'created_at',
            //'markdel_at',
            //'markdel_by',
            //'shortname',
            'name',

            [
                'label' => 'Количество хуков получено',
                'value' => function(PbxWebhookType $model){
                    return count($model->webhooks);
                }
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {files}', 'buttons' => [
                'view' => function($url, PbxWebhookType $model, $key){
                    return Html::a("v",
                        ['/pbx_webhook_type/default/view', 'id' => $model->id],
                        ['data-pjax' => 0]);
                },
            ]],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
