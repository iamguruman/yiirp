<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\pbx_webhook_type\models\PbxWebhookType */

$this->title = 'Добавить';

$this->params['breadcrumbs'][] = app\modules\pbx_webhook_type\Module::getBreadcrumbs();

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="pbx_webhook_type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
