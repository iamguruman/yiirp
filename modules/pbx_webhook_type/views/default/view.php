<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\pbx_webhook_type\models\PbxWebhookType */

/* @var $hookSearchModel \app\modules\pbx_webhook\models\PbxWebhookSearch */
/* @var $hookDataProvider \yii\data\ActiveDataProvider */

if(aIfModuleControllerAction("pbx_webhook_type", "default", "view")){
    $this->title = $model->getTitle();

    $this->params['breadcrumbs'][] = app\modules\pbx_webhook_type\Module::getBreadcrumbs();

    $this->params['breadcrumbs'][] = $this->title;

    \yii\web\YiiAsset::register($this);
}
?>
<div class="mmcn_webhook_type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'shortname',
            'name',
        ],
    ]) ?>

    <?= \yii\bootstrap5\Tabs::widget(['items' => [

        [
            'label' => 'ID',
            'active' => false,
            'content' => DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'created_at',
                    'createdBy.lastNameWithInitials',
                    'updated_at',
                    'updatedBy.lastNameWithInitials',
                    'markdel_at',
                    'markdelBy.lastNameWithInitials',
                ],
            ])
        ],

        !empty($hookDataProvider) ? [
            'label' => "Webhooks ({$hookDataProvider->totalCount})",
            'active' => aGet('tab') == 'webhooks' ? true : null,
            'content' => "<br>".$this->render("@app/modules/pbx_webhook/views/default/index.php", [
                    'searchModel' => $hookSearchModel,
                    'dataProvider' => $hookDataProvider,
                ]),
        ] : ['visible' => false],


    ]]) ?>

</div>
