<?php

namespace app\modules\ids_import\controllers;

use app\components\XlsImportHelper;
use app\modules\ids\models\Ids;
use app\modules\ids_import\models\IdsImportField;
use app\modules\ids_import\models\IdsImportFile;
use app\modules\ids_import\models\IdsImportFileSearch;
use app\modules\ids_import\models\IdsImportName;
use app\modules\ids_template\models\TemplateField;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * FileController implements the CRUD actions for IdsImportFile model.
 */
class FileController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    public function actionImport($id){

        $model = $this->findModel($id);

        XlsImportHelper::runImport($model);

        exit();

        $filePath = Yii::getAlias("@app")."/web/_ids_import/{$model->md5}.{$model->ext}";
        if(file_exists($filePath)){

            $datas = self::csv_to_array($filePath, ';');

            foreach ($datas as $data){

                $newIds = new Ids();
                $newIds->created_at = aDateNow();
                //$newIds->save();

                foreach ($data as $key => $value){

                    $key = str_replace("", "", $key);
                    $idsImportField = IdsImportField::find()
                        ->andWhere(['ids_import__name_id' => $model->ids_import__name_id])
                        ->andWhere(['field_name_from_file' => $key])
                        ->one();
                    dd($idsImportField);
                    dd($key);


                }
                ddd('x');

                ddd($data);
            }

            /*$xml = simplexml_load_string($fileContent, "SimpleXMLElement", LIBXML_NOCDATA);
            $json = json_encode($xml);
            $array = json_decode($json,TRUE);
            ddd($array['bik']);*/

            //foreach (){}

        }

    }

    public function actionImportPrintRArray($id){

        $model = $this->findModel($id);

        $filePath = Yii::getAlias("@app")."/web/_ids_import/{$model->md5}.{$model->ext}";
        if(file_exists($filePath)){

            $data = self::csv_to_array($filePath, ';');
            ddd($data);

        }

    }



    private static function csv_to_array($filename = '', $delimiter = ',')
    {
        if(!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $header = NULL;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                if(!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }

    /**
     * Lists all IdsImportFile models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new IdsImportFileSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single IdsImportFile model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new IdsImportFile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $idsImportName = IdsImportName::findOne(aGet('import_name_id'));

        $idsImportFileModel = new IdsImportFile();
        $idsImportFileModel->created_at = aDateNow();
        $idsImportFileModel->ids_import__name_id = $idsImportName->id;

        if ($this->request->isPost) {
            if ($idsImportFileModel->load($this->request->post())) {

                $uploadedFile = UploadedFile::getInstanceByName('IdsImportFile[file]');
                if($uploadedFile->hasError) {
                    throw new \Exception("File upload error, error code : ".$uploadedFile->error);
                }

                if ($idsImportFileModel->save()) {

                    $idsImportFileModel->file = UploadedFile::getInstances($idsImportFileModel, 'file');
                    $uploadedFile= $idsImportFileModel->file[0];
                    $md5 = md5_file($uploadedFile->tempName);
                    $fileName = $md5 . '.' . $uploadedFile->extension;
                    $directory = Yii::getAlias('@app/web/_ids_import') . DIRECTORY_SEPARATOR;
                    $filePath = $directory . $fileName;
                    if(!file_exists($filePath)){
                        $uploadedFile->saveAs($filePath);

                    }

                    $idsImportFileModel->md5 = $md5;
                    $idsImportFileModel->ext = $uploadedFile->extension;
                    $idsImportFileModel->save();

                    return $this->redirect(['view', 'id' => $idsImportFileModel->id]);
                }
            }
        } else {
            $idsImportFileModel->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $idsImportFileModel,
        ]);
    }

    /**
     * Updates an existing IdsImportFile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing IdsImportFile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the IdsImportFile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return IdsImportFile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IdsImportFile::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
