<?php

namespace app\modules\ids_import\controllers;

use app\modules\ids_import\models\IdsImportFileSearch;
use app\modules\ids_template\models\TemplateName;
use app\modules\ids_import\models\IdsImportFieldSearch;
use app\modules\ids_import\models\IdsImportName;
use app\modules\ids_import\models\IdsImportNameSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for IdsImportName model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all IdsImportName models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new IdsImportNameSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single IdsImportName model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $model = $this->findModel($id);

        $fieldSearchModel = new IdsImportFieldSearch();
        $fieldDataProvider = $fieldSearchModel->search($this->request->queryParams, [
            'ids_import__name_id' => $model->id
        ]);

        $fileSearchModel = new IdsImportFileSearch();
        $fileDataProvider = $fileSearchModel->search($this->request->queryParams, [
            'ids_import__name_id' => $model->id
        ]);
        $fileDataProvider->setSort(['defaultOrder' => ['created_at' => SORT_DESC]]);

        return $this->render('view', [
            'fileSearchModel' => $fileSearchModel,
            'fileDataProvider' => $fileDataProvider,

            'fieldSearchModel' => $fieldSearchModel,
            'fieldDataProvider' => $fieldDataProvider,

            'model' => $model,
        ]);
    }

    public function actionImport($id)
    {
        $model = $this->findModel($id);
        var_dump($_FILES);
    }

    /**
     * Creates a new IdsImportName model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new IdsImportName();

        $model->created_at = aDateNow();

        if($templateModel = TemplateName::findOne(aGet('template_id'))){
            $model->ids_template_name_id = $templateModel->id;
        }

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing IdsImportName model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing IdsImportName model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the IdsImportName model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return IdsImportName the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IdsImportName::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
