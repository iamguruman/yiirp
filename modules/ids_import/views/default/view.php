<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\modules\ids_import\models\IdsImportName $model */

/** @var \app\modules\ids_import\models\IdsImportFieldSearch $fieldSearchModel */
/** @var \yii\data\ActiveDataProvider $fieldDataProvider */

/** @var \app\modules\ids_import\models\IdsImportFile $fileSearchModel */
/** @var \yii\data\ActiveDataProvider $fileDataProvider */

$this->title = $model->getTitle();

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Template Names'),
    'url' => ['/ids_template']];

if($idsTemplateName = $model->idsTemplateName){

    $this->params['breadcrumbs'][] = ['label' => $model->idsTemplateName->name,
        'url' => ['/ids_template/name/view', 'id' => $model->ids_template_name_id, 'tab' => 'import']];

}

$this->params['breadcrumbs'][] = ['label' =>
    Yii::t('app', "All Imports to Template"),
    'url' =>
        $model->idsTemplateName ?
            ['/ids_template/name/view', 'id' => $model->ids_template_name_id, 'tab' => 'import']
            :
            ['index']
];

$this->params['breadcrumbs'][] = ['label' =>
    Yii::t('app', "Import {$model->name}"),
    'url' =>
        $model->idsTemplateName ?
            ['/ids_import/default/view', 'id' => $model->id, 'tab' => 'import']
            :
            ['index']
];

$this->params['breadcrumbs'][] = $this->title;

\yii\web\YiiAsset::register($this);
?>
<div class="import-xml-to-ids-name-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'),
            ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

        <?= Html::a(Yii::t('app', '💾 Upload import file ...'),
            ['/ids_import/file/create', 'import_name_id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'idsTemplateName.name',
            'name',
            'comment:ntext',
        ],
    ]) ?>

    <?= \yii\bootstrap5\Tabs::widget(['items' => [
        [
            'label' => "Fields ({$fieldDataProvider->totalCount})",
            'content' => "<br>".$this->render("../field/index", [
                'searchModel' => $fieldSearchModel,
                'dataProvider' => $fieldDataProvider
            ])
        ],

        [
            'label' => "File ({$fileDataProvider->totalCount})",
            'active' => aGet('tab') == 'file' ? true : null,
            'content' => "<br>".$this->render("../file/index", [
                'searchModel' => $fileSearchModel,
                'dataProvider' => $fileDataProvider
            ])
        ],
    ]]) ?>

</div>
