<?php

use app\modules\ids_template\models\TemplateName;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\ids_import\models\IdsImportName $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="import-xml-to-ids-name-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ids_template_name_id')->widget(Select2::className(), [
        'data' => TemplateName::find()->select(['name', 'id'])->indexBy('id')->column(),
        'options' => ['placeholder' => 'Select a template ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <br>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <br>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <br>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
