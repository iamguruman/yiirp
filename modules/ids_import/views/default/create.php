<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\ids_import\models\IdsImportName $model */

$this->title = Yii::t('app', 'Create import to Ids');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Import data to Ids'), 'url' => ['index']];

if($model->idsTemplateName){
    $this->params['breadcrumbs'][] = ['label' => $model->idsTemplateName->name,
        'url' => ['/ids_template/name/view', 'id' => $model->ids_template_name_id]];
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="import-xml-to-ids-name-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
