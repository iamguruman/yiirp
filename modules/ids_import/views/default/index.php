<?php

use app\modules\ids_import\models\IdsImportName;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this */
/** @var app\modules\ids_import\models\IdsImportNameSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

if(aIfModuleControllerAction("ids_import", "default", "index")){
    $this->title = Yii::t('app', 'Import data To Ids');
    $this->params['breadcrumbs'][] = $this->title;
}
?>
<div class="import-xml-to-ids-name-index">

    <?php if(aIfModuleControllerAction("ids_import", "default", "index")): ?>
        <h1><?= Html::encode($this->title) ?></h1>
    <?php endif; ?>

    <p>

        <?= Html::a(Yii::t('app', 'Create Import'),
            ['/ids_import/default/create',
                'template_id' =>
                    aIfModuleControllerAction("ids_template", "name", "view") ?
                        aGet('id')
                    : null,
            ],
            ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'markdel_at',
            //'markdel_by',

            'idsTemplateName.name',
            'name',

            //'comment:ntext',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, IdsImportName $model, $key, $index, $column) {
                    return Url::toRoute(['/ids_import/default/'.$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
