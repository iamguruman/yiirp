<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\ids_import\models\IdsImportName $model */

$this->title = $model->title;

if($model->idsTemplateName){
    $this->params['breadcrumbs'][] = ['label' => $model->idsTemplateName->name,
        'url' => ['/ids_template/name/view', 'id' => $model->ids_template_name_id]];
}

$this->params['breadcrumbs'][] = ['label' =>
    Yii::t('app', 'Import data to Ids'),
    'url' =>
        $model->idsTemplateName ?
            ['/ids_template/name/view', 'id' => $model->ids_template_name_id, 'tab' => 'import']
            :
            ['index']
];

$this->params['breadcrumbs'][] = ['label' => $model->getTitle(), 'url' => ['view', 'id' => $model->id]];

$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="import-xml-to-ids-name-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
