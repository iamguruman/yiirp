<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\modules\ids_import\models\IdsImportField $model */

$this->title = $model->id;

if($model->idsTemplateField && $model->idsTemplateField->templateName){
    $this->params['breadcrumbs'][] = ['label' => $model->idsTemplateField->templateName->name,
        'url' => ['/ids_template/name/view', 'id' => $model->idsTemplateField->templateName->id, 'tab' => 'import']];
}

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Import Xml To Ids Fields'),
    'url' => ['index']];

if($model->idsImportName){
    $this->params['breadcrumbs'][] = ['label' => $model->idsImportName->name,
        'url' => ['/ids_import/default/view', 'id' => $model->idsImportName->id]];
}




$this->params['breadcrumbs'][] = $this->title;

\yii\web\YiiAsset::register($this);
?>
<div class="import-xml-to-ids-field-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'idsTemplateField.templateName.name:raw:Template',
            'idsTemplateField.title:raw:Template`s field title',
            'field_name_from_file',
            'comment:ntext',
        ],
    ]) ?>

</div>
