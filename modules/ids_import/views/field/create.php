<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\ids_import\models\IdsImportField $model */

$this->title = Yii::t('app', 'Create Import Xml To Ids Field');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Import Xml To Ids Fields'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="import-xml-to-ids-field-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
