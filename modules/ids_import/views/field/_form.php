<?php

use app\modules\ids_template\models\TemplateField;
use app\modules\ids_import\models\IdsImportName;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\ids_import\models\IdsImportField $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="import-xml-to-ids-field-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ids_import__name_id')->widget(Select2::className(), [
        'data' => IdsImportName::find()->select(['name', 'id'])->indexBy('id')->column(),
        'options' => ['placeholder' => 'Select import name ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <br>

    <?= $form->field($model, 'ids_template_field_id')->widget(Select2::className(), [
        'data' => TemplateField::find()
            ->andWhere(['template_name_id' => $model->idsImportName->ids_template_name_id])
            ->select(['title', 'id'])
            ->indexBy('id')
            ->orderBy(['sort' => SORT_ASC])
            ->column(),
        'options' => ['placeholder' => 'Select template field ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <br>

    <?= $form->field($model, 'field_name_from_file')->textInput(['maxlength' => true]) ?>

    <br>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <br>

    <?= $form->field($model, 'is_equal')->checkbox() ?>

    <br>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
