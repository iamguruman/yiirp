<?php

use app\modules\ids_import\models\IdsImportField;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this */
/** @var app\modules\ids_import\models\IdsImportFieldSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

if(aIfModuleControllerAction("ids_import", "field", "index")) {
    $this->title = Yii::t('app', 'Import Xml To Ids Fields');
    $this->params['breadcrumbs'][] = $this->title;
}
?>
<div class="import-xml-to-ids-field-index">

    <?php if(aIfModuleControllerAction("ids_import", "default", "index")): ?>
        <h1><?= Html::encode($this->title) ?></h1>
    <?php endif; ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create field import'),
            ['/ids_import/field/create',
                'import_name_id' =>
                    aIfModuleControllerAction("ids_import", "default", "view") ?
                        aGet('id')
                    : null,

                'returnto' =>
                    aIfModuleControllerAction("ids_import", "default", "view") ?
                        $_SERVER['REQUEST_URI']
                    : null,
                ],
            ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'comment:ntext',
            'idsTemplateField.title',
            'field_name_from_file',
            'is_equal:boolean',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, IdsImportField $model, $key, $index, $column) {
                    return Url::toRoute(['/ids_import/field/'.$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
