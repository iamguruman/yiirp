<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\ids_import\models\IdsImportFile $model */

$this->title = Yii::t('app', 'Create Ids Import File');

if($model->idsImportName){
    $this->params['breadcrumbs'][] = ['label' => $model->idsImportName->name,
        'url' => ['/ids_import/default/view', 'id' => $model->idsImportName->id, 'tab' => 'file']];
}

/*$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ids Import Files'),
    'url' => ['index']];*/

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ids-import-file-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
