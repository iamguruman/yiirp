<?php

use app\modules\ids_import\models\IdsImportName;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\ids_import\models\IdsImportFile $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ids-import-file-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ids_import__name_id')->widget(Select2::className(), [
        'data' => IdsImportName::find()->select(['name', 'id'])->indexBy('id')->column(),
        'options' => ['placeholder' => 'Select import name ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <br>

   <?= $form->field($model, 'file') ->fileInput(['multiple' => false])  ?>

    <br>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <br>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
