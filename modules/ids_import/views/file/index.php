<?php

use app\modules\ids_import\models\IdsImportFile;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this */
/** @var app\modules\ids_import\models\IdsImportFileSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

if(aIfModuleControllerAction("
ids_import", "file", "index")){
    $this->title = Yii::t('app', 'Ids Import Files');
    $this->params['breadcrumbs'][] = $this->title;
}
?>
<div class="ids-import-file-index">

    <?php if(aIfModuleControllerAction("ids_import", "file", "index")): ?>
        <h1><?= Html::encode($this->title) ?></h1>
    <?php endif; ?>

    <p>
        <?= Html::a(Yii::t('app', '💾 Upload import file ...'),
            ['/ids_import/file/create',
                'import_name_id' =>
                    aIfModuleControllerAction("ids_import", "default", "view") ?
                      aGet('id')
                    : null,
            ],
            ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'created_at',
            'idsImportName.title',
            'comment:ntext',
            [
                'label' => '',
                'format' => 'raw',
                'value' => function(IdsImportFile $model){
                    return Html::a(
                            'download',
                            "/_ids_import/{$model->md5}.{$model->ext}",
                        ['data-pjax' => 0]
                    );
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, IdsImportFile $model, $key, $index, $column) {
                    return Url::toRoute(["/ids_import/file/".$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
