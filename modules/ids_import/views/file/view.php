<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\modules\ids_import\models\IdsImportFile $model */

$this->title = $model->id;

//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ids Import Files'), 'url' => ['index']];

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Template Names'),
    'url' => ['/ids_template']];

if($idsTemplateName = $model->idsImportName->idsTemplateName){
    $this->params['breadcrumbs'][] = ['label' => $idsTemplateName->name,
        'url' => ['/ids_template/name/view', 'id' => $idsTemplateName->id, 'tab' => 'import']];
}

if($model->idsImportName){
    $this->params['breadcrumbs'][] = ['label' => $model->idsImportName->name,
        'url' => ['/ids_import/default/view', 'id' => $model->idsImportName->id]];
}

$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ids-import-file-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

        <?= Html::a(Yii::t('app', 'Download file'),
            "/_ids_import/{$model->md5}.{$model->ext}",
            ['class' => 'btn btn-primary', 'target' => '_blank']) ?>

        <?= Html::a(Yii::t('app', 'Import data'),
            ['import', 'id' => $model->id],
            ['class' => 'btn btn-primary']) ?>

        <?= Html::a(Yii::t('app', 'Import Print Array'),
            ['import-print-r-array', 'id' => $model->id],
            ['class' => 'btn btn-primary', 'target' => '_blank']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'idsImportName.title',
            'comment:ntext',
        ],
    ]) ?>

</div>
