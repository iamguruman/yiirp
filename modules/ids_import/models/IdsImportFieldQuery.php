<?php

namespace app\modules\ids_import\models;

/**
 * This is the ActiveQuery class for [[IdsImportField]].
 *
 * @see IdsImportField
 */
class IdsImportFieldQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return IdsImportField[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return IdsImportField|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
