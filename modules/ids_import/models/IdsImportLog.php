<?php

namespace app\modules\ids_import\models;

use Yii;

/**
 * This is the model class for table "ids_import__log".
 *
 * @property int $id
 * @property int|null $import_id
 * @property int|null $file_id
 * @property string|null $entry_json
 * @property string|null $created_at
 */
class IdsImportLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ids_import__log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['import_id', 'file_id'], 'integer'],
            [['entry_json'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'import_id' => 'Import ID',
            'file_id' => 'File ID',
            'entry_json' => 'Entry Json',
            'created_at' => 'Created At',
        ];
    }
}
