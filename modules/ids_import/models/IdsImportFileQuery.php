<?php

namespace app\modules\ids_import\models;

/**
 * This is the ActiveQuery class for [[IdsImportFile]].
 *
 * @see IdsImportFile
 */
class IdsImportFileQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return IdsImportFile[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return IdsImportFile|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
