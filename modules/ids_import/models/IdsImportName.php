<?php

namespace app\modules\ids_import\models;

use app\modules\ids_template\models\TemplateName;
use Yii;

/**
 * This is the model class for table "ids_import__name".
 *
 * @property int $id
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property string|null $markdel_at
 * @property int|null $markdel_by
 *
 * @property-read string $title this is template name + this name
 *
 * @property string|null $name
 * @property string|null $comment
 *
 * @property int $ids_template_name_id
 * @property TemplateName $idsTemplateName
 * @property IdsImportFile $fileLast
 *
 */
class IdsImportName extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ids_import__name';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'markdel_at'], 'safe'],
            [['created_by', 'updated_by', 'markdel_by', 'ids_template_name_id'], 'integer'],
            [['comment'], 'string'],
            [['ids_template_name_id'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'markdel_at' => Yii::t('app', 'Markdel At'),
            'markdel_by' => Yii::t('app', 'Markdel By'),
            'name' => Yii::t('app', 'Name'),
            'comment' => Yii::t('app', 'Comment'),
            'ids_template_name_id' => Yii::t('app', 'Ids Template Name ID'),
        ];
    }

    public function getIdsTemplateName(){
        return $this->hasOne(TemplateName::className(), ['id' => 'ids_template_name_id']);
    }

    public function getTitle(){
        $ret = [];

        if($this->idsTemplateName){

            if($this->name){
                $ret [] = $this->idsTemplateName->name.":";
            } else {
                $ret [] = $this->idsTemplateName->name;
            }

        }

        if($this->name){
            $ret [] = $this->name;
        }

        return implode(" ", $ret);
    }


    public function getFileLast()
    {
        return $this->hasOne(IdsImportFile::class, ['ids_import__name_id' => 'id'])->orderBy('ids_import__file.id DESC');
    }

    /**
     * {@inheritdoc}
     * @return IdsImportQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IdsImportQuery(get_called_class());
    }
}
