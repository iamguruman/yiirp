<?php

namespace app\modules\ids_import\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\ids_import\models\IdsImportName;

/**
 * IdsImportNameSearch represents the model behind the search form of `app\modules\ids_import\models\IdsImportName`.
 */
class IdsImportNameSearch extends IdsImportName
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by', 'markdel_by', 'ids_template_name_id'], 'integer'],
            [['created_at', 'updated_at', 'markdel_at', 'name', 'comment'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $params2 = [])
    {
        $query = IdsImportName::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'markdel_at' => $this->markdel_at,
            'markdel_by' => $this->markdel_by,
            'ids_template_name_id' => $this->ids_template_name_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        if(!empty($params2['ids_template_name_id'])){
            $query->andWhere(['ids_template_name_id' => $params2['ids_template_name_id']]);
        }

        return $dataProvider;
    }
}
