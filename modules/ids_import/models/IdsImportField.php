<?php

namespace app\modules\ids_import\models;

use app\modules\ids_template\models\TemplateField;
use Yii;

/**
 * This is the model class for table "ids_import__field".
 *
 * @property int $id
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property string|null $markdel_at
 * @property int|null $markdel_by
 * @property string|null $comment
 *
 * @property int $ids_template_field_id
 * @property-read TemplateField $idsTemplateField
 *
 * @property string $field_name_from_file
 *
 * @property string $is_equal
 *
 * @property string $ids_import__name_id
 * @property-read IdsImportName $idsImportName
 */
class IdsImportField extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ids_import__field';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_equal'], 'integer', 'max' => 1],

            [['ids_import__name_id', 'field_name_from_file'], 'required'],
            [['ids_import__name_id'], 'integer'],
            [['ids_template_field_id'], 'integer'],
            [['id', 'created_by', 'updated_by', 'markdel_by'], 'integer'],
            [['created_at', 'updated_at', 'markdel_at'], 'safe'],
            [['comment'], 'string'],
            [['field_name_from_file'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Is equal' => Yii::t('app', 'Is equal'),
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'markdel_at' => Yii::t('app', 'Markdel At'),
            'markdel_by' => Yii::t('app', 'Markdel By'),
            'comment' => Yii::t('app', 'Comment'),
            'ids_import__name_id' => Yii::t('app', 'Import xml to Ids'),
            'ids_template_field_id' => Yii::t('app', 'Template field'),
            'field_name_from_file' => Yii::t('app', 'Field name from file'),
        ];
    }

    public function getIdsImportName(){
        return $this->hasOne(IdsImportName::className(), ['id' => 'ids_import__name_id']);
    }

    public function getIdsTemplateField(){
        return $this->hasOne(TemplateField::className(), ['id' => 'ids_template_field_id']);
    }

    /**
     * {@inheritdoc}
     * @return IdsImportFieldQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IdsImportFieldQuery(get_called_class());
    }
}
