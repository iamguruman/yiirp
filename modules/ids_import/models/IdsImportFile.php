<?php

namespace app\modules\ids_import\models;

use Yii;

/**
 * This is the model class for table "ids_import__file".
 *
 * @property int $id
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property string|null $markdel_at
 * @property int|null $markdel_by
 * @property string|null $comment
 *
 * @property int $ids_import__name_id
 * @property-read IdsImportName $idsImportName
 *
 * @property string $md5
 * @property string $ext
 */
class IdsImportFile extends \yii\db\ActiveRecord
{

    public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ids_import__file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'markdel_at'], 'safe'],
            [['created_by', 'updated_by', 'markdel_by', 'ids_import__name_id'], 'integer'],
            [['comment'], 'string'],
            [['ids_import__name_id'], 'required'],
            [['file'], 'safe'],
            [['md5'], 'string', 'max' => 255],
            [['ext'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ext' => Yii::t('app', 'Extension'),
            'md5' => Yii::t('app', 'Md5'),
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'markdel_at' => Yii::t('app', 'Markdel At'),
            'markdel_by' => Yii::t('app', 'Markdel By'),
            'comment' => Yii::t('app', 'Comment'),
            'ids_import__name_id' => Yii::t('app', 'Ids Import  Name ID'),
        ];
    }

    public function getIdsImportName(){
        return $this->hasOne(IdsImportName::className(), ['id' => 'ids_import__name_id']);
    }

    /**
     * @return string|null
     */
    public function getPath()
    {
        if(!$this->md5 || !$this->ext) {
            return null;
        }

        return \Yii::getAlias('@app/web/_ids_import') . DIRECTORY_SEPARATOR.$this->md5.'.'.$this->ext;
    }

    /**
     * {@inheritdoc}
     * @return IdsImportFileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IdsImportFileQuery(get_called_class());
    }
}
