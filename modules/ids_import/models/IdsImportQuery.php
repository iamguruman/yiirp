<?php

namespace app\modules\ids_import\models;

/**
 * This is the ActiveQuery class for [[IdsImportName]].
 *
 * @see IdsImportName
 */
class IdsImportQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return IdsImportName[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return IdsImportName|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
