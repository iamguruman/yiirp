<?php

namespace app\modules\ids_field\controllers;

use app\modules\ids_template\models\TemplateField;
use yii\web\Controller;

class SortController extends Controller
{

    public function actionIndex($direction, $template_field_id){

        $model = TemplateField::findOne($template_field_id);

        $sortNow = $model->sort;

        if($direction == 'up'){

            $sortPrevious = TemplateField::find()
                ->select('sort')
                ->andWhere(['and', ['template_name_id' => $model->template_name_id], ['<', 'sort', $sortNow]])
                ->orderBy(['sort' => SORT_DESC])
                ->scalar();

            $model->sort = $sortPrevious - 1;
        }

        if($direction == 'down'){

            $sortNext = TemplateField::find()
                ->select('sort')
                ->andWhere(['and', ['template_name_id' => $model->template_name_id], ['>', 'sort', $sortNow]])
                ->orderBy(['sort' => SORT_ASC])
                ->scalar();

            $model->sort = $sortNext + 1;
        }

        $model->save();

        $i = 10;
        foreach (TemplateField::find()
         ->andWhere(['template_name_id' => $model->template_name_id])
         ->orderBy(['sort' => SORT_ASC])->all() as $field){
            $field->sort = $i;
            $field->save();

            $i += 10;
        }

        aReturnto();

    }

}