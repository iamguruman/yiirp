<?php

namespace app\modules\ids_field\controllers;

use app\modules\ids_field\models\IdsTemplateFieldReplace;
use app\modules\ids_field\models\IdsTemplateFieldReplaceSearch;
use app\modules\ids_template\models\TemplateField;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * FieldReplaceController implements the CRUD actions for IdsTemplateFieldReplace model.
 */
class FieldReplaceController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                        'do-replace' => ['POST'],
                    ],
                ],
            ]
        );
    }

    public function actionDoReplace($id){

        $model = $this->findModel($id);

        $sql = "select id, value from ids_value_{$model->idsTemplateField->value_field} where template_field_id={$model->idsTemplateField->id}";

        dd($sql);

        $results = Yii::$app->db->createCommand($sql)->queryAll();

        $sqlA = [];
        foreach ($results as $result){

            if(str_contains($result['value'], $model->text_search)){

                $sqlA [] = "update ids_value_{$model->idsTemplateField->value_field} set value = '".addslashes(str_replace($model->text_search, $model->text_replace, $result['value']))."' where id={$result['id']} limit 1";

            }

        }


        $sql = implode(";\n", $sqlA);

        //ddd($sql);

        Yii::$app->db->createCommand($sql)->execute();

        return $this->redirect(["view", 'id' => $model->id]);

    }

    /**
     * Lists all IdsTemplateFieldReplace models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new IdsTemplateFieldReplaceSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single IdsTemplateFieldReplace model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new IdsTemplateFieldReplace model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new IdsTemplateFieldReplace();

        $model->created_at = aDateNow();
        $model->created_by = aUserMyId();

        if($templateField = TemplateField::findOne(aGet('template_field_id'))){
            $model->ids_template_field_id = $templateField->id;
        }

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing IdsTemplateFieldReplace model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing IdsTemplateFieldReplace model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the IdsTemplateFieldReplace model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return IdsTemplateFieldReplace the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IdsTemplateFieldReplace::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
