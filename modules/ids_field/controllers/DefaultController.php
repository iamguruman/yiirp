<?php

namespace app\modules\ids_field\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{

    public function actionIndex(){
        return $this->render("index");
    }
}