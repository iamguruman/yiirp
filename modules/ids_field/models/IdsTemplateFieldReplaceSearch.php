<?php

namespace app\modules\ids_field\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * IdsTemplateFieldReplaceSearch represents the model behind the search form of `app\modules\ids_template\models\IdsTemplateFieldReplace`.
 */
class IdsTemplateFieldReplaceSearch extends IdsTemplateFieldReplace
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by', 'markdel_by', 'ids_template_field_id'], 'integer'],
            [['created_at', 'updated_at', 'markdel_at', 'text_search', 'text_replace'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $params2 = [])
    {
        $query = IdsTemplateFieldReplace::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'markdel_by' => $this->markdel_by,
            'markdel_at' => $this->markdel_at,
            'ids_template_field_id' => $this->ids_template_field_id,
        ]);

        $query->andFilterWhere(['like', 'text_search', $this->text_search])
            ->andFilterWhere(['like', 'text_replace', $this->text_replace]);

        if(!empty($params2['ids_template_field_id'])){
            $query->andWhere(['ids_template_field_id' => $params2['ids_template_field_id']]);
        }

        return $dataProvider;
    }
}
