<?php

namespace app\modules\ids_field\models;

/**
 * This is the ActiveQuery class for [[IdsTemplateFieldReplace]].
 *
 * @see IdsTemplateFieldReplace
 */
class IdsTemplateFieldReplaceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return IdsTemplateFieldReplace[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return IdsTemplateFieldReplace|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
