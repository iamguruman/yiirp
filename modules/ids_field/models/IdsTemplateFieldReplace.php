<?php

namespace app\modules\ids_field\models;

use app\modules\ids_template\models\TemplateField;
use Yii;

/**
 * This is the model class for table "ids_template_field_replace".
 *
 * @property int $id
 * @property int|null $created_by
 * @property string|null $created_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @property int|null $markdel_by
 * @property string|null $markdel_at
 *
 * @property int $ids_template_field_id
 * @property-read TemplateField $idsTemplateField
 *
 * @property string|null $text_search
 * @property string|null $text_replace
 */
class IdsTemplateFieldReplace extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ids_template_field_replace';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_by', 'updated_by', 'markdel_by', 'ids_template_field_id'], 'integer'],
            [['created_at', 'updated_at', 'markdel_at'], 'safe'],
            [['ids_template_field_id'], 'required'],
            [['text_search', 'text_replace'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'markdel_by' => Yii::t('app', 'Markdel By'),
            'markdel_at' => Yii::t('app', 'Markdel At'),
            'ids_template_field_id' => Yii::t('app', 'Ids Template Field ID'),
            'text_search' => Yii::t('app', 'Text Search'),
            'text_replace' => Yii::t('app', 'Text Replace'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return IdsTemplateFieldReplaceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IdsTemplateFieldReplaceQuery(get_called_class());
    }

    public function getIdsTemplateField(){
        return $this->hasOne(TemplateField::className(), ['id' => 'ids_template_field_id']);
    }
}
