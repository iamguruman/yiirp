<?php

use app\modules\ids_field\models\IdsTemplateFieldReplace;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var \app\modules\ids_field\models\IdsTemplateFieldReplaceSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

if(aIfModuleControllerAction("ids_field", "field-replace", "index")){
    $this->title = Yii::t('app', 'Ids Template Field Replaces');
    $this->params['breadcrumbs'][] = $this->title;
}
?>
<div class="ids-template-field-replace-index">

    <?php if(aIfModuleControllerAction("ids_field", "field-replace", "index")): ?>
        <h1><?= Html::encode($this->title) ?></h1>
    <?php endif; ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Ids Template Field Replace'),
            ['/ids_field/field-replace/create',
                'template_field_id' =>
                    aIfModuleControllerAction("ids_template", "field", "view") ?
                        aGet('id')
                        : null,
            ],
            ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',

            aIfModuleControllerAction("ids_template", "field", "view") ? ['visible' => false] : [
                'attribute' => 'idsTemplateField.title',
            ],

            'text_search',
            'text_replace',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, IdsTemplateFieldReplace $model, $key, $index, $column) {
                    return Url::toRoute(["/ids_field/field-replace/".$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
