<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var \app\modules\ids_field\models\IdsTemplateFieldReplace $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ids-template-field-replace-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ids_template_field_id')->textInput() ?>

    <br>

    <?= $form->field($model, 'text_search')->textInput(['maxlength' => true]) ?>

    <br>

    <?= $form->field($model, 'text_replace')->textInput(['maxlength' => true]) ?>

    <br>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
