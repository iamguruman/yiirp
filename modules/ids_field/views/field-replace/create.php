<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var \app\modules\ids_field\models\IdsTemplateFieldReplace $model */

$this->title = Yii::t('app', 'Create Ids Template Field Replace');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ids Template Field Replaces'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ids-template-field-replace-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
