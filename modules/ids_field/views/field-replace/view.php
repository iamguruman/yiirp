<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var \app\modules\ids_field\models\IdsTemplateFieldReplace $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ids Template Field Replaces'), 'url' => ['index']];

if($model->idsTemplateField){
    $this->params['breadcrumbs'][] = ['label' => $model->idsTemplateField->title,
        'url' => ['/ids_template/field/view',
            'id' => $model->idsTemplateField->id,
            'tab' => 'field-replace']];
}

$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ids-template-field-replace-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>

        <?= Html::a(Yii::t('app', 'Do replace'), ['do-replace', 'id' => $model->id], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to do this replace?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_by',
            'created_at',
            'updated_by',
            'updated_at',
            'markdel_by',
            'markdel_at',
            'ids_template_field_id',
            'text_search',
            'text_replace',
        ],
    ]) ?>

</div>
