<?php

namespace app\modules\db_dump\controllers;

use app\models\User;
use yii\web\Controller;
use Yii;

class DefaultController extends Controller
{

    public function actionIndex(){

        $db = Yii::$app->db;

        $user = $db->username;
        $pass = $db->password;

        $fileName = Yii::getAlias("@app").'/web/dump.sql.gz';

        exec("mysqldump --user=root --password=pP3450719!! --host=127.0.0.1 yiirp | gzip > {$fileName}");

        return $this->render("index");

    }

}