<?php

$fileName = Yii::getAlias("@app").'/web/dump.sql.gz';

echo \yii\helpers\Html::a($fileName, ['/dump.sql.gz']);

echo " was last modified: " . date ("F  d Y H:i:s.", filemtime($fileName));
echo " and filesize is " . round(filesize($fileName) / pow(1024, 2), 2);
echo "mb";


?>

