<?php

namespace app\modules\pbx_webhook\models;

/**
 * This is the ActiveQuery class for [[PbxWebhook]].
 *
 * @see PbxWebhook
 */
class PbxWebhookQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    public function init()
    {
        parent::init();

        if(aIfHideMarkdel()){
            $this->andWhere(['m_mcn_weebhook.markdel_by' => null]);
        }
    }

    /**
     * {@inheritdoc}
     * @return PbxWebhook[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PbxWebhook|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
