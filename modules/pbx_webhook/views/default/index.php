<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\pbx_webhook\models\PbxWebhookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$module = app\modules\pbx_webhook\Module::moduleId;
$controller = "default";
$action = "index";

if(aIfModuleControllerAction($module, $controller, $action)){

    $this->title = \app\modules\pbx_webhook\Module::moduleTitle;

    $this->params['breadcrumbs'][] = $this->title;
}

?>
<div class="pbx_webhook-index">

    <?= aHtmlHeader($this->title, $module, $controller, $action) ?>

    <p>
        <?php /*= aIfModuleControllerAction($module, $controller, $action) ?
            Html::a('Добавить', ["/{$module}/{$controller}/create"], ['class' => 'btn btn-success'])
            : null */ ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute' => 'id',
                'headerOptions' => ['style' => 'width:100px;'],
                'format' => 'raw', 'value' => function($model) { return aGridVIewColumnId($model); }],
            
            //'created_at',
            //'markdel_at',
            //'markdel_by',
            'created_at',
            'webhookType.urlTo:raw',
            'internalNumber',

            [
                'attribute' => 'json',
                'format' => 'ntext',
                'value' => function($model){
                    return aJsonPrettyPrint($model->json);
                }
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}', 'buttons' => [
                'view' => function($url, \app\modules\pbx_webhook\models\PbxWebhook $model, $key){
                    return Html::a("v", ['/pbx_webhook/default/view', 'id' => $model->id], ['data-pjax' => 0]);
                },


            ]],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
