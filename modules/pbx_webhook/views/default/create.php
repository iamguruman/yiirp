<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\pbx_webhook\models\PbxWebhook */

$this->title = 'Добавить';

$this->params['breadcrumbs'][] = app\modules\pbx_webhook\Module::getBreadcrumbs();

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="pbx_webhook-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
