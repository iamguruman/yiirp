<?php

use app\modules\pbx_webhook\models\PbxWebhook;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\pbx_webhook\models\PbxWebhook */

if(aIfModuleControllerAction("pbx_webhook", "default", "view")){
    $this->title = $model->getTitle();

    $this->params['breadcrumbs'][] = app\modules\pbx_webhook\Module::getBreadcrumbs();

    $this->params['breadcrumbs'][] = $this->title;

    \yii\web\YiiAsset::register($this);
}
?>
<div class="pbx_webhook-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',

            [
                'attribute' => 'json',
                'format' => 'ntext',
                'value' => function(PbxWebhook $model){
                    return aJsonPrettyPrint($model->json);
                }
            ],
        ],
    ]) ?>

    <?= \yii\bootstrap5\Tabs::widget(['items' => [

        [
            'label' => 'ID',
            'active' => false,
            'content' => DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'created_at',
                    'createdBy.lastNameWithInitials',
                    'updated_at',
                    'updatedBy.lastNameWithInitials',
                    'markdel_at',
                    'markdelBy.lastNameWithInitials',
                ],
            ])
        ],

    ]]) ?>

</div>
