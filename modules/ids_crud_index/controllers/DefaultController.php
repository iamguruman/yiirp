<?php

namespace app\modules\ids_crud_index\controllers;

use app\modules\ids\models\Ids;
use app\modules\ids_template\models\TemplateField;
use app\modules\ids_template\models\TemplateName;
use Yii;
use yii\base\DynamicModel;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex($template_id){
        $template = TemplateName::findOne($template_id);

        $query = (new Query())
            ->addSelect(['ID' => 'ids.id'])
            ->addSelect(['status' => 'ids.ids_template_status_name_id'])
            ->from('ids')
            ->where(['ids.template_name_id' => $template_id]);

        if($template->show_field_name_on_crud_index){
            $query->addSelect(['name' => 'ids.name']);
        }

        /** @var TemplateField $templateField */
        foreach ($template->templateFieldsForTableList as $templateField) {
            $titleTranslitered = $templateField->titleTranslitered;
            $query->addSelect([$templateField->title => $titleTranslitered .'.value']);
            $query->leftJoin([$titleTranslitered => 'ids_value_'.$templateField->value_field],
                $titleTranslitered .'.ids_id = ids.id AND '. $titleTranslitered .'.template_field_id = '.$templateField->id);
            $dynamicModelConfig[$titleTranslitered] = $_GET['DynamicModel'][$titleTranslitered] ?? null;
            if($dynamicModelConfig[$titleTranslitered]) {
                //todo разный поиск
                //todo положить поиск в отдельный метод
                $terms = explode('+', $dynamicModelConfig[$titleTranslitered]);
                array_walk($terms, 'trim');
                foreach($terms as $term) {
                    $query->andWhere(['like', $titleTranslitered . '.value', $term]);
                }
            }
        }

        $dynamicModelConfig['ID'] = $_GET['DynamicModel']['ID'] ?? null;
        if($dynamicModelConfig['ID']) {
            $query->andWhere(['ids.id' => $dynamicModelConfig['ID']]);
        }

        if(isset($_GET['status']) && !isset($_GET['DynamicModel']['status'])) {
            $_GET['DynamicModel']['status'] = $_GET['status'];
        }

        $dynamicModelConfig['status'] = $_GET['DynamicModel']['status'] ?? null;
        if($dynamicModelConfig['status']) {
            $query->andWhere(['ids.ids_template_status_name_id' => $dynamicModelConfig['status']]);
        }

        if($template->show_field_name_on_crud_index){
            $dynamicModelConfig['name'] = $_GET['DynamicModel']['name'] ?? null;
            if($dynamicModelConfig['name']) {
                $query->andWhere(['like', 'ids.name', $dynamicModelConfig['name']]);
            }
        }

        //quick sketch
        $searchModel = new DynamicModel($dynamicModelConfig);

        //todo add actual search rules ?
        $searchModel->addRule(array_keys($dynamicModelConfig), 'safe');
        $searchModel->validate();
        $searchableAttributes = array_merge(['ID', 'name'], array_keys($dynamicModelConfig));
        $searchableMap = array_combine($searchableAttributes, array_map(function($attribute) {
            if(in_array($attribute, ['ID', 'name'])) {
                return $attribute;
            }
            else {
                return [
                    'asc' => [$attribute.'.value' => SORT_ASC],
                    'desc' => [$attribute.'.value' => SORT_DESC],
                ];
            }
        }, $searchableAttributes));
        $dp = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => $searchableMap,
            ]
        ]);

//        var_dump($query->createCommand()->queryAll());
//        exit();
        $columnsConfigArray = $this->getColumnsConfigArray($template_id);
        return $this->render('index', [
            'columns' => $columnsConfigArray,
            'searchModel' => $searchModel,
            'dataProvider' => $dp,
        ]);

    }


    public function actionIndexOld($template_id){

        $template = TemplateName::findOne($template_id);

        $sql = [];

        $sql [] = "select ";
        //select

        $sql [] = "`ids`.`id` as `ID`, ";

        /** @var TemplateField $field */
        $count = count($template->templateFieldsForTableList);
        $i = 0;
        foreach ($template->templateFieldsForTableList as $field){
            $i++;
            $sql [] = "`{$field->titleTranslitered}`.`value` as `{$field->title}` ";

            if($count <> $i){
                $sql [] = ", ";
            }
        }


        // from
        $sql [] = "from `ids` ";

        //join-s

        /** @var TemplateField $field */
        foreach ($template->templateFieldsForTableList as $field){
            $sql [] = "left join `ids_value_{$field->value_field}` ";
            $sql [] = " as `{$field->titleTranslitered}` ";
            $sql [] = "     on `{$field->titleTranslitered}`.`ids_id` = `ids`.`id` ";
            $sql [] = "     and `{$field->titleTranslitered}`.`template_field_id` = {$field->id}";
        }

        //where-s
        /*foreach ($template->templateFieldsForTableList as $field){
            if($_GET["search[{$field->titleTranslitered}]"]){

                $sql [] = "`{$field->titleTranslitered}`.`value` = ";

                if('' == 'int'){
                    $sql [] = $_GET["search[]"];
                }

            }
        }*/

        //limit
        $sql [] = "limit 1,10";

        $sql = implode("\n", $sql);
        //ddd($sql);

        $result = Yii::$app->db->createCommand($sql)->execute();

        return $this->render("index", ['mysql_result' => $result]);

    }

    /**
     * @param $template_id
     * @return array
     */
    public function getColumnsConfigArray($template_id): array
    {

        $templateName = TemplateName::findOne($template_id);

        $columns = [];
        $columns [] = ['class' => 'yii\grid\SerialColumn'];

        !$template_id ? $columns [] = [
            'label' => 'Icon',
            'format' => 'raw',
            'value' => function (Ids $model) {
                $icon = Yii::getAlias("@app/web/tplicons/{$model->template_name_id}.png");
                if (file_exists($icon)) {

                    $ret = [];

                    $title = '';
                    if ($model->templateName && $model->templateName->name) {
                        $title = $model->templateName->name;
                    }

                    $ret [] = Html::a(
                        "<img src='/tplicons/{$model->template_name_id}.png' title='{$title}' style='max-height: 30px;'>",
                        ['/ids', 'template_id' => $model->template_name_id],
                        ['data-pjax' => 0]);

                    if ($model->markdelBy) {
                        $ret [] = "<span style='color: red;' title='Deleted by {$model->markdelBy->username} at {$model->markdelAtFormatted}'>d</span>";
                    }

                    return implode("&nbsp;", $ret);
                }
            }
        ] : ['visible' => false];


        $columns [] = ['attribute' => 'ID'];

        if($templateName && $templateName->show_field_name_on_crud_index){
            $columns [] = ['attribute' => 'name'];
        }

        //global $templateFieldTable, $templateFieldId;
        //if ($templateName = TemplateName::findOne($template_id)) {
        if ($templateName) {
            /** @var TemplateField $templateField */
            foreach ($templateName->templateFields as $templateField) {
                if ($templateField->show_as_column_at_template_list) {

                    $templateFieldId = $templateField->id;
                    $templateFieldTable = $templateField->value_field;

                    $columns [] = [
                        'attribute' => $templateField->titleTranslitered,
                        'label' => "{$templateField->title} ($templateField->id)",
                        'format' => 'raw',
                        'value' => function ($row) use ($templateField) {
                            //ddd($grid);

                            return '<pre>'.print_r($row[$templateField->title], true).'</pre>';

                            /*return TemplateField::getFieldValueStatic(
                                $model->id,
                                $GLOBALS['templateFieldId'],
                                $GLOBALS['templateFieldTable']
                            );*/
                        }
                    ];
                }
            }
        }

        $columns [] = ['attribute' => 'status'];

        $columns [] = [
            'label' => '',
            'format' => 'raw',
            'value' => function ($row) {

                $ret [] = Html::a("<img src='/media/icon_eye.png' height='20'>", ['/ids/view', 'id' => $row['ID']], ['data-pjax' => 0]);
                //todo
                $ret [] = Html::a("<img src='/media/icon_popup_window.png' height='20'>", '#', ['data-pjax' => 0, 'class' => 'ids-popup-open-link', 'data-ids-id' => $row['ID']]);

                return implode("&nbsp;", $ret);
            }
        ];
        return $columns;
    }

}